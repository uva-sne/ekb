1. Energy Knowledge Base program includes
	1) data collectors: power collector and performance collector
	2) the ClusterManagerAPI for performance collector
	3) meta data collector: data is saved in triple store
	4) service: the access APIs of EKB
2. Applications:
	1) create a data profile using EKB and visualization of the data profile
	2) study power estimation models based on the data profile 