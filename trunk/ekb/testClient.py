import time
import json
import requests
import simplejson
import concurrent.futures
#--------------------------------------------------
t1 = (2013, 05, 21 , 11, 30, 0, 1, 48, 0)
t2 = (2013, 05, 22 , 0, 0, 0, 1, 48, 0)
startseconds_test = time.mktime(t1) 
endseconds_test = time.mktime(t2)

startseconds = "1368223200"
endseconds = "1368309600"
#--------------------------------------------------         
def tester(querys): 
    #print "query " , querys 
    query_args = {'query' : querys}  
    start = time.time()
    results = requests.get('http://145.100.132.31:8001/queryreq?', params = query_args)
    end = time.time()
    responseTime = end - start
    return "start is %s and responseTime is %s" %(start, responseTime), results.text
    
#def main(n):
 #   print "tstd1 ", n,time.time()
   # n = int(n)
Metrics = "ActiveEnergy"
f = open('t.txt', 'wb')
querystr = """select ?timevalue where{ 
                edl:outlet11 rdf:type edl:Outlet.
                edl:outlet11 edl:informOf ?powerread.
				?powerread rdf:type edl:%s.
				?powerread edl:energyValue ?powervalue.
				?powerread edl:sampleAt ?timeread.
				?timeread  edl:relativeSecond ?timevalue.
						FILTER  (?timevalue > '%s'^^xsd:long && ?timevalue < '%s'^^xsd:long). }""" %(Metrics , startseconds_test, endseconds_test)
querystr = """select ?outlet ?powerread where{ 
                edl:outlet11 rdf:type edl:Outlet.
                edl:outlet11 edl:informOf ?powerread.
                ?powerread rdf:type edl:%s
                }
                """% Metrics

querystr2 = """select ?outlet where {?outlet rdf:type edl:Outlet}"""						
print querystr 						  
						
urls = [querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr, querystr]
with concurrent.futures.ThreadPoolExecutor(max_workers= 1) as pool:
    results = pool.map(tester, urls)
    for result in results:
        f.write('\n')
        f.write(str(result))
f.close()  
    

