'''
Created on Mar 13, 2013

@author: haozhu
'''
#!/usr/bin/env python
import pythoncm #bright cluster manager api
import sys, os



class BrightCMDException(Exception):
    pass

class BrightClusterClient(object):
    '''
    classdocs
    '''
    
    def __init__(self, BrightURL = 'https://localhost:8081', pfx = '/home/hzhu/.cm/cmgui/admin.pfx', BrightPassword = 'Re4d0nly' ):
        '''
        Constructor
        '''
        self.manager = pythoncm.ClusterManager()
        self.cluster = self.manager.addCluster(BrightURL, pfx , '', BrightPassword);
        if not self.cluster.connect():
            raise BrightCMDException('Connection failed!')

     
    def disconnect(self):
        self.cluster.disconnect()
           # raise BrightCMDException('Disconnection failed!')

              
    def getAllDevices(self):
        devices = self.cluster.getAll('Device')
        #print "Return all the devices in this cluster"
        return devices
    
    def getDeviceType(self, device):
        deviceType = device.childType 
        print "Return type of device - EthernetSwitch, MasterNode, PhysicalNode or others: %s" % deviceType
        return deviceType
    
    def getDeviceInfo(self, device): 
        deviceName = device.hostname
        deviceStatus = device.status()
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            if 'UP' in str(deviceStatus):
                nodeOS = device.sysinfo().osName
            else: 
                nodeOS = None    
            nodeInfo = {'deviceNamekey':deviceName, 'deviceStatuskey':deviceStatus, 'deviceOSkey':nodeOS}
            print "Return the info of node: %s" % nodeInfo
            return nodeInfo
        elif device.childType == 'EthernetSwitch':
            switchIP = device.ip
            switchPortsNum = device.ports
            switchInfo = {'deviceNamekey':deviceName, 'deviceStatuskey':deviceStatus,'switchIPkey':switchIP,'switchPortsNumkey':switchPortsNum} 
            print "Return the info of switch: %s" % switchInfo
            return switchInfo
        else: 
            raise BrightCMDException ('It is not a normal device: no info')
 
    def getNodeSysinfo(self, device):
        return device.sysinfo()


    def getProcessorInfo(self,device):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            CPUmodel = device.sysinfo().processors[0].modelName
            coreNum = device.sysinfo().processors[0].cores
            CPUfrequency = device.sysinfo().processors[0].speed 
            deviceCPU = {'CPUmodelkey': CPUmodel, 'coreNumkey': coreNum, 'CPUfrequencykey': CPUfrequency}
            print "Return the CPU info of node: %s" % deviceCPU
            return deviceCPU
        else: 
            raise BrightCMDException('It is not a normal node: no CPU info')
    
    def getMemInfo(self,device):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            memTotal= device.sysinfo().memoryTotal
            deviceMem = {'memTotal': memTotal}
            print "Return the memory info of node: %s" % deviceMem
            return deviceMem
        else: 
            raise BrightCMDException('It is not a normal node: no CPU info')

    def getDiskInfo(self,device):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            diskCount = device.sysinfo().diskCount
            diskTotal = device.sysinfo().diskTotalSpace
            diskSpace = []
            for it in range(diskCount):
                singleSpace = []
                diskModel = device.sysinfo().disks[it].model
                diskSpaceName = device.sysinfo().disks[it].name
                diskSpaceSize = device.sysinfo().disks[it].size
                singleSpace =  [diskModel, diskSpaceName, diskSpaceSize]
                diskSpace.append(singleSpace)
            deviceDisk = {'diskTotal': diskTotal, 'diskCount': diskCount, 'diskSpace': diskSpace}
            print "Return the Disk info of node: %s" % deviceDisk
            return deviceDisk
        else: 
            raise BrightCMDException('It is not a normal node: no Disk info') 

    def getAllInterfaces(self,device):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            deviceInterfaces = device.interfaces 
            print "Return all the interfaces of node: %s" % deviceInterfaces
            return deviceInterfaces
        else: 
            raise BrightCMDException('It is not a normal node: no interfaces in it')
    
    def getInterfaceType(self, device, interface):
        interfaceType = interface.network.name 
        print "Return the type of interface: %s" % interfaceType
        return interfaceType
     
    def getInterfaceInfo(self, device, interface):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            interfaceName = interface.name
            interfaceIP = interface.ip
            if hasattr(interface, 'speed'):
                interfaceBandwidth = interface.speed 
            else:
                interfaceBandwidth = None    
            interfaceInfo = {'interfaceNamekey':interfaceName, 'interfaceIPkey':interfaceIP, 'interfaceBandwidthkey':interfaceBandwidth}    
            print "Return interface info of node: %s" % interfaceInfo
            return interfaceInfo
        else: 
            raise BrightCMDException('It is not a normal node: no interface info')
        
    def getInterface2Port(self,device):
        if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
            if device.ethernetSwitch.ethernetSwitch != None:
                node2Switch = device.ethernetSwitch.ethernetSwitch
                node2SwitchName = device.ethernetSwitch.ethernetSwitch.hostname
                node2PortNum = device.ethernetSwitch.port
                node2Port = {'node2Switch':node2Switch, 'node2Switchkey':node2SwitchName, 'interface2PortNumkey':node2PortNum}
            else: 
                node2Port = None 
            return node2Port 
        else: 
            raise BrightCMDException('It is not a normal node: no port info')        
    

    
                        
