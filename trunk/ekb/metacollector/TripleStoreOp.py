'''
Created on Mar 19, 2013

@author: haozhu
'''

#!/usr/bin/env python
from franz.openrdf.sail.allegrographserver import AllegroGraphServer
from franz.openrdf.repository.repository import Repository
from franz.miniclient import repository
from franz.openrdf.query.query import QueryLanguage
from franz.openrdf.model import URI
from franz.openrdf.vocabulary.rdf import RDF
from franz.openrdf.vocabulary.rdfs import RDFS
from franz.openrdf.vocabulary.owl import OWL 
from franz.openrdf.vocabulary.xmlschema import XMLSchema
from franz.openrdf.query.dataset import Dataset
from franz.openrdf.rio.rdfformat import RDFFormat
from franz.openrdf.rio.rdfwriter import  NTriplesWriter
from franz.openrdf.rio.rdfxmlwriter import RDFXMLWriter

import BrightClusterClient
import sys, os, datetime, time, urllib
import json

class TripleStoreException(Exception):
    pass

 

class TripleStore(object):
    def __init__(self, Conn, brighturl, pfxfile, brightpw ):
        self.storeConn = Conn
        self.EDLBase = 'http://www.semanticweb.org/ontologies/2012/3/edl.owl#'
        self.NMLBase = 'http://schemas.ogf.org/nml/2012/10/base#'
        self.INDLBase = 'http://www.science.uva.nl/research/sne/indl#'
        self.OWLBase = 'http://www.w3.org/2002/07/owl#' 
        self.RDFSBase = 'http://www.w3.org/2000/01/rdf-schema#'
                # set prefix:namespace
        self.storeConn.setNamespace('nml', self.NMLBase)
        self.storeConn.setNamespace('indl', self.INDLBase)
        self.storeConn.setNamespace('edl', self.EDLBase)  
        # Connect to the client of Bright Cluster Manager
        #self.brightConn = BrightClusterClient.BrightClusterClient(BrightURL = 'https://146.50.10.100:8081', pfx = '/home/karel/.cm/cmgui/admin.pfx', BrightPassword = 'Re4d0nly');
        self.brightConn = BrightClusterClient.BrightClusterClient(BrightURL = brighturl, pfx = pfxfile, BrightPassword = brightpw);
        self.CPUModelSet = set()
        self.PowerMeterSet = set()
        self.MetricSet = set()
        self.UnitSet = set()
    
    def instantiateDevice(self, deviceRaw):
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        deviceNameStr = str(deviceInfoRaw['deviceNamekey'])
        deviceIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = deviceNameStr)
        deviceNameLitr = self.storeConn.createLiteral(deviceNameStr, datatype=XMLSchema.STRING)
        return deviceIndvdURI

    def instantiateSwithPort(self, deviceRaw, portNum):
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        deviceNameStr = str(deviceInfoRaw['deviceNamekey'])
        switchPortNameStr = deviceNameStr + 'port' + portNum
        switchPortIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = switchPortNameStr)
        return switchPortIndvdURI

    def instantiateNodePort(self, deviceRaw, interfaceRaw):
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        deviceNameStr = str(deviceInfoRaw['deviceNamekey'])
        interfaceInfoRaw = self.brightConn.getInterfaceInfo(deviceRaw, interfaceRaw)
        nodePortNameStr = deviceNameStr + 'port' +str(interfaceInfoRaw['interfaceNamekey'])
        nodePortIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = nodePortNameStr)
        return nodePortIndvdURI

    def instantiateNodeCPU(self, deviceRaw):        
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        nodeNameStr = str(deviceInfoRaw['deviceNamekey'])
        processorInfoRaw = self.brightConn.getProcessorInfo(deviceRaw)
            # add the processor individual:* is individual; Type of * is ProcessingComponent; 
            # * has cpuspeed, cpuarch, cores; * is part of node individual; node individual hasComponent *;
        CPUIndvdURI = self.storeConn.createURI(namespace = self.INDLBase, localname = nodeNameStr+'CPU')
        return CPUIndvdURI

    def instantiateNodeMem(self, deviceRaw):        
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        nodeNameStr = str(deviceInfoRaw['deviceNamekey'])
        memIndvdURI = self.storeConn.createURI(namespace = self.INDLBase, localname = nodeNameStr+'Memory')
        return memIndvdURI

    def instantiateNodeDisk(self, deviceRaw, diskSpaceName):        
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        nodeNameStr = str(deviceInfoRaw['deviceNamekey'])    
        diskSpaceNameStr = diskSpaceName
        diskIndvdURI = self.storeConn.createURI(namespace = self.INDLBase, localname = nodeNameStr+diskSpaceNameStr)
        return diskIndvdURI



    def addTripleOfDevice(self, deviceRaw):
        # create common URIs of concepts in selected schemas
        indvdURI = self.storeConn.createURI(namespace=self.OWLBase, localname='NamedIndividual')
            #class
        NodeURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Node')
            #Object Properties
        nameURI = self.storeConn.createURI(namespace=self.NMLBase, localname='name')
       
        deviceTypeRaw = self.brightConn.getDeviceType(deviceRaw) 
        print "^^^^^^^^^^"
        print deviceTypeRaw
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)

        # add the device individual: * is individual; Type of * is Node; * has name. 
        deviceNameStr = str(deviceInfoRaw['deviceNamekey'])
        deviceIndvdURI = self.instantiateDevice(deviceRaw)
        deviceNameLitr = self.storeConn.createLiteral(deviceNameStr, datatype=XMLSchema.STRING)
        
        self.storeConn.addTriples([(deviceIndvdURI, RDF.TYPE, indvdURI),
                              (deviceIndvdURI, RDF.TYPE, NodeURI),
                              (deviceIndvdURI, nameURI, deviceNameLitr)
                              ])        
        
        
    def addTripleOfSwitch(self, deviceRaw):    
        
        # create common URIs of concepts in selected schemas
        indvdURI = self.storeConn.createURI(namespace=self.OWLBase, localname='NamedIndividual')
            #class
        PortURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Port')

        
            #Object Properties
        nameURI = self.storeConn.createURI(namespace=self.NMLBase, localname='name')
        inBoundURI = self.storeConn.createURI(namespace=self.NMLBase, localname='hasInboundPort')
        outBoundURI = self.storeConn.createURI(namespace=self.NMLBase, localname='hasOutboundPort')
   
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        deviceNameStr = str(deviceInfoRaw['deviceNamekey'])

        switchIndvdURI = self.instantiateDevice(deviceRaw)

        # add the port individual: switch has outbound; switch has inbound;
        # port is individual;  port has name; Type of port is Port
        #deviceInfoRaw['switchIPkey']
        for switchPortNum in range(deviceInfoRaw['switchPortsNumkey']):
            #switchPortNameStr = deviceNameStr + 'port' +str(switchPortNum+1)
            #portIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = portNameStr)
            switchPortIndvdURI = self.instantiateSwithPort(deviceRaw, str(switchPortNum+1))
            switchPortLitr = self.storeConn.createLiteral(str(switchPortNum+1), datatype=XMLSchema.STRING)
            
            self.storeConn.addTriples([(switchIndvdURI, inBoundURI, switchPortIndvdURI),
                                 (switchIndvdURI, outBoundURI, switchPortIndvdURI),
                                 (switchPortIndvdURI, RDF.TYPE, indvdURI),
                                 (switchPortIndvdURI, RDF.TYPE, PortURI),
                                 (switchPortIndvdURI, nameURI, switchPortLitr)
                                 ])

        
    def addTripleOfNode(self, deviceRaw):
        # create common URIs of concepts in selected schemas
        indvdURI = self.storeConn.createURI(namespace=self.OWLBase, localname='NamedIndividual')
            #class
        NodeURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Node')
        PortURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Port')
        LinkURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Link')
        ProcessingComponentURI = self.storeConn.createURI(namespace=self.INDLBase, localname='ProcessingComponent')
        MemoryComponentURI = self.storeConn.createURI(namespace=self.INDLBase, localname='MemoryComponent')
        StorageComponentURI = self.storeConn.createURI(namespace=self.INDLBase, localname='StorageComponent')
        
            #Object Properties
        nameURI = self.storeConn.createURI(namespace=self.NMLBase, localname='name')
        inBoundURI = self.storeConn.createURI(namespace=self.NMLBase, localname='hasInboundPort')
        outBoundURI =self. storeConn.createURI(namespace=self.NMLBase, localname='hasOutboundPort')
        isSinkURI = self.storeConn.createURI(namespace=self.NMLBase, localname='isSink')
        isSourceURI = self.storeConn.createURI(namespace=self.NMLBase, localname='isSource')
        hasSinkURI = self.storeConn.createURI(namespace=self.NMLBase, localname='hasSink')
        hasSourceURI = self.storeConn.createURI(namespace=self.NMLBase, localname='hasSource')
        hasComponetURI = self.storeConn.createURI(namespace=self.INDLBase, localname='hasComponent')
        partOfURI = self.storeConn.createURI(namespace=self.INDLBase, localname='partOf')
        
            #Data Properties
        cpuspeedURI =  self.storeConn.createURI(namespace=self.INDLBase, localname='cpuspeed')
        coresURI = self.storeConn.createURI(namespace=self.INDLBase, localname='cores')
        sizeURI = self.storeConn.createURI(namespace=self.INDLBase, localname='size')
        modelURI = self.storeConn.createURI(namespace=self.INDLBase, localname='model')
        
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
        # add the device individual: * is individual; Type of * is Node; * has name. 
        nodeNameStr = str(deviceInfoRaw['deviceNamekey'])
        nodeIndvdURI = self.instantiateDevice(deviceRaw)

        deviceTypeRaw = self.brightConn.getDeviceType(deviceRaw)

        
 
        sysinfoRaw = self.brightConn.getNodeSysinfo(deviceRaw)
        if sysinfoRaw != None :
            processorInfoRaw = self.brightConn.getProcessorInfo(deviceRaw)
            # add the processor individual:* is individual; Type of * is ProcessingComponent; 
            # * has cpuspeed, cpuarch, cores; * is part of node individual; node individual hasComponent *;
            CPUIndvdURI = self.instantiateNodeCPU(deviceRaw)
            CPUnameLitr = self.storeConn.createLiteral('CPU', datatype=XMLSchema.STRING)
            
 
            CPUspeedLitr = self.storeConn.createLiteral(processorInfoRaw['CPUfrequencykey'], datatype=XMLSchema.FLOAT)
            coresLitr = self.storeConn.createLiteral(str(processorInfoRaw['coreNumkey']), datatype=XMLSchema.INT)
            CPUmodelLitr = self.storeConn.createLiteral(str(processorInfoRaw['CPUmodelkey']), datatype=XMLSchema.STRING)
                    
            self.storeConn.addTriples([(CPUIndvdURI, RDF.TYPE, indvdURI),
                                    (CPUIndvdURI, RDF.TYPE, ProcessingComponentURI),
                                    (CPUIndvdURI, cpuspeedURI, CPUspeedLitr),
                                    (CPUIndvdURI, coresURI, coresLitr),
                                    (CPUIndvdURI, modelURI, CPUmodelLitr),
                                    (CPUIndvdURI, partOfURI, nodeIndvdURI),
                                    (nodeIndvdURI, hasComponetURI, CPUIndvdURI),
                                    (CPUIndvdURI, nameURI, CPUnameLitr)
                                              ])
            # add memory instance
            memInfoRaw = self.brightConn.getMemInfo(deviceRaw)  
            memIndvdURI = self.instantiateNodeMem(deviceRaw)
            memNameLitr = self.storeConn.createLiteral('Memory', datatype=XMLSchema.STRING)
    
            memTotalLitr = self.storeConn.createLiteral(str(memInfoRaw['memTotal']), datatype=XMLSchema.LONG)
                    
            self.storeConn.addTriples([(memIndvdURI, RDF.TYPE, indvdURI),
                                    (memIndvdURI, RDF.TYPE, MemoryComponentURI),
                                    (memIndvdURI, sizeURI, memTotalLitr),
                                    (memIndvdURI, partOfURI, nodeIndvdURI),
                                    (nodeIndvdURI, hasComponetURI, memIndvdURI),
                                    (memIndvdURI, nameURI, memNameLitr)
                                              ])
            # add disk instance
            diskInfoRaw = self.brightConn.getDiskInfo(deviceRaw)
            diskCount = diskInfoRaw['diskCount']
            diskSpace = diskInfoRaw['diskSpace']
            for row in diskSpace:
                diskModelStr =  row[0]
                diskSpaceNameStr = row[1]
                diskSpaceSizeStr = row[2]
                diskIndvdURI = self.instantiateNodeDisk(deviceRaw, diskSpaceNameStr)
                diskNameLitr = self.storeConn.createLiteral(diskSpaceNameStr, datatype=XMLSchema.STRING)

                diskSizeLitr = self.storeConn.createLiteral(str(diskSpaceSizeStr), datatype=XMLSchema.LONG)
                diskModelLitr = self.storeConn.createLiteral(diskModelStr, datatype=XMLSchema.STRING)
                    
                self.storeConn.addTriples([(diskIndvdURI, RDF.TYPE, indvdURI),
                                        (diskIndvdURI, RDF.TYPE, StorageComponentURI),
                                        (diskIndvdURI, sizeURI, diskSizeLitr),
                                        (diskIndvdURI, modelURI, diskModelLitr),
                                        (diskIndvdURI, partOfURI, nodeIndvdURI),
                                        (nodeIndvdURI, hasComponetURI, diskIndvdURI),
                                        (diskIndvdURI, nameURI, diskNameLitr)
                                                  ])


        allInterfacesRaw = self.brightConn.getAllInterfaces(deviceRaw) 
        interfaceCount = 0
        for interfaceRaw in allInterfacesRaw: 
            interfaceCount = interfaceCount + 1
            # add the port individual: switch has outbound; switch has inbound;
            # port is individual;  port has name; Type of port is Port
            interfaceInfoRaw = self.brightConn.getInterfaceInfo(deviceRaw, interfaceRaw)
            nodePortNameStr = nodeNameStr + 'port' + str(interfaceInfoRaw['interfaceNamekey'])
            nodePortIndvdURI = self.instantiateNodePort(deviceRaw, interfaceRaw)
            nodePortLitr = self.storeConn.createLiteral(str(interfaceInfoRaw['interfaceNamekey']) , datatype=XMLSchema.STRING)
        
            self.storeConn.addTriples([(nodeIndvdURI, inBoundURI, nodePortIndvdURI),
                                 (nodeIndvdURI, outBoundURI, nodePortIndvdURI),
                                 (nodePortIndvdURI, RDF.TYPE, indvdURI),
                                 (nodePortIndvdURI, RDF.TYPE, PortURI),
                                 (nodePortIndvdURI, nameURI, nodePortLitr)
                                 ])
        
            if self.brightConn.getInterfaceType(deviceRaw, interfaceRaw) == 'internalnet':
            # this is a internal network link    
            # add the link individual: * is individual; Type of * is Link; * has name; 
            # link hasSink port; link hasSource port; port isSink link; port isSource link.
                interface2PortInfoRaw = self.brightConn.getInterface2Port(deviceRaw)

                if interface2PortInfoRaw != None:
                    # this is a switch link
                    switchRaw = interface2PortInfoRaw['node2Switch']
                    print "**************"
                    print switchRaw, interface2PortInfoRaw['node2Switchkey']
                    switchPortNameStr = interface2PortInfoRaw['node2Switchkey'] + 'port' + str(interface2PortInfoRaw['interface2PortNumkey'])
                    switchPortIndvdURI = self.instantiateSwithPort(switchRaw, str(interface2PortInfoRaw['interface2PortNumkey']))
                    linkNameStr = nodePortNameStr + '-LinkTo-' + switchPortNameStr
                    linkIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = linkNameStr)
                    linkLitr = self.storeConn.createLiteral('Link', datatype=XMLSchema.STRING)

                    
                    self.storeConn.addTriples([(linkIndvdURI, RDF.TYPE, indvdURI),
                                          (linkIndvdURI, RDF.TYPE, LinkURI),
                                          (linkIndvdURI, nameURI, linkLitr),
                                          (linkIndvdURI, hasSinkURI, nodePortIndvdURI),
                                          (linkIndvdURI, hasSourceURI, switchPortIndvdURI),
                                          (linkIndvdURI, hasSinkURI, switchPortIndvdURI),
                                          (linkIndvdURI, hasSourceURI, nodePortIndvdURI),
                                          (nodePortIndvdURI, isSinkURI, linkIndvdURI),
                                          (nodePortIndvdURI, isSourceURI, linkIndvdURI),
                                          (switchPortIndvdURI, isSinkURI, linkIndvdURI),
                                          (switchPortIndvdURI, isSourceURI, linkIndvdURI)
                                          ])
                    
    def instantiateOutlet(self, outletIndvd, outlet):
        outletNameStr = str(outlet)
        outletIndvdURI = self.storeConn.createURI(namespace = self.EDLBase, localname = outletIndvd)
        outletNameLitr = self.storeConn.createLiteral(outletNameStr, datatype=XMLSchema.STRING)
        print "show the name of Outlet %s" % outletNameLitr
        return outletIndvdURI, outletNameLitr
        
    def addTripleOfPDU(self,deviceRaw):
        
        # create common URIs of concepts in selected schemas
        indvdURI = self.storeConn.createURI(namespace=self.OWLBase, localname='NamedIndividual')
            #class
        NodeURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Node')
        PortURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Port')
        LinkURI = self.storeConn.createURI(namespace=self.NMLBase, localname='Link')
        OutletURI = self.storeConn.createURI(namespace=self.EDLBase, localname='Outlet')
        PowerMeterURI = self.storeConn.createURI(namespace=self.EDLBase, localname='PowerMeter')
        
            #Object Properties
        nameURI = self.storeConn.createURI(namespace=self.NMLBase, localname='name')
        attachToURI = self.storeConn.createURI(namespace=self.EDLBase, localname='attachTo')
        hasOutletURI = self.storeConn.createURI(namespace=self.EDLBase, localname='hasOutlet')
        #informOfURI = self.storeConn.createURI(namespace=self.EDLBase, localname='informOf')
        
            #Data Properties
     
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
            # add the device individual: * is individual; Type of * is Node; * has name. 
        deviceNameStr = deviceInfoRaw['deviceNamekey']
        deviceIndvdURI = self.storeConn.createURI(namespace = self.NMLBase, localname = deviceNameStr)
        
        absdir, _ = os.path.split(os.path.abspath(__file__))
        confpath  = os.path.join(absdir, 'pdu.conf')
        conf      = load_conf(confpath)

        for pdu in conf['pdus']:

            powermeterNameStr = str(pdu['name']) #'uva1'
            powermeterIndvdURI = self.storeConn.createURI(namespace = self.EDLBase, localname = powermeterNameStr+str(pdu['model']))
            powermeterNameLitr = self.storeConn.createLiteral(powermeterNameStr, datatype=XMLSchema.STRING)   
            for map in pdu['maps']:
                if deviceNameStr == map[0]: 
                    outletIndvdURI, outletNameLitr = self.instantiateOutlet(powermeterNameStr+'outlet'+str(map[1]), map[1])
            
                    if powermeterNameStr in self.PowerMeterSet:
                        self.storeConn.addTriples([(outletIndvdURI, RDF.TYPE, indvdURI),
                                                    (outletIndvdURI, RDF.TYPE, OutletURI),
                                                    (outletIndvdURI, nameURI, outletNameLitr),
                                                    (powermeterIndvdURI, hasOutletURI, outletIndvdURI),
                                                    (deviceIndvdURI, attachToURI, outletIndvdURI)
                                                    ])
                    else:
                        self.PowerMeterSet.add(powermeterNameStr)
                        self.storeConn.addTriples([(outletIndvdURI, RDF.TYPE, indvdURI),
                                                  (outletIndvdURI, RDF.TYPE, OutletURI),
                                                  (outletIndvdURI, nameURI, outletNameLitr),
                                                  (powermeterIndvdURI, RDF.TYPE, indvdURI),
                                                  (powermeterIndvdURI, RDF.TYPE, PowerMeterURI),
                                                  (powermeterIndvdURI, nameURI, powermeterNameLitr),
                                                  (powermeterIndvdURI, hasOutletURI, outletIndvdURI),
                                                  (deviceIndvdURI, attachToURI, outletIndvdURI)
                                                  ])

# increase the description of metric: metric, metric name in tsdb, comment about metric and unit
# flag depicts the type of metric
    def addTripleOfMetric(self, deviceIndvdURI, metricNameStr,tsdbmetricNameStr, metricComment,unitofmetricNameStr, flag):
        # create common URIs of concepts in selected schemas
        indvdURI = self.storeConn.createURI(namespace=self.OWLBase, localname='NamedIndividual')
        commentURI = self.storeConn.createURI(namespace=self.RDFSBase, localname='comment')
            #class
        PerformanceMetricURI = self.storeConn.createURI(namespace=self.EDLBase, localname='PerfMetric')
        CalculatedGMetricURI = self.storeConn.createURI(namespace=self.EDLBase, localname='CalculatedGMetric')
        ObservedGMetricURI = self.storeConn.createURI(namespace=self.EDLBase, localname='ObservedGMetric')
        RatioURI = self.storeConn.createURI(namespace=self.EDLBase, localname='Ratio')
        EfficiencyURI = self.storeConn.createURI(namespace=self.EDLBase, localname='Efficiency')
        QuantityURI = self.storeConn.createURI(namespace=self.EDLBase, localname='Quantity')        
        UnitURI = self.storeConn.createURI(namespace=self.EDLBase, localname='Unit')
            #Object Properties
        nameURI = self.storeConn.createURI(namespace=self.NMLBase, localname='name')
        hasUnitURI = self.storeConn.createURI(namespace=self.EDLBase, localname='hasUnit')
        measuredByMetricURI = self.storeConn.createURI(namespace=self.EDLBase, localname='measuredByMetric') 
            #Data Properties
        

        metricIndvdURI = self.storeConn.createURI(namespace = self.EDLBase, localname = metricNameStr)
        metricNameLitr = self.storeConn.createLiteral(tsdbmetricNameStr, datatype=XMLSchema.STRING)
        metricCommentLitr = self.storeConn.createLiteral(metricComment, datatype=XMLSchema.STRING)


        unitofmetricIndvdURI = self.storeConn.createURI(namespace = self.EDLBase, localname = unitofmetricNameStr)
        unitofmetricNameLitr = self.storeConn.createLiteral(unitofmetricNameStr, datatype=XMLSchema.STRING)

        if metricNameStr in self.MetricSet:
            self.storeConn.addTriples([(deviceIndvdURI, measuredByMetricURI, metricIndvdURI),
                                      (metricIndvdURI, hasUnitURI, unitofmetricIndvdURI)
                                      ])   
        else:
            self.MetricSet.add(metricNameStr)
            self.storeConn.addTriples([(metricIndvdURI, commentURI, metricCommentLitr)])
            if flag == 'ObservedGMetric':
                tmpMetricURI = ObservedGMetricURI
            elif flag == 'CalculatedGMetric':
                tmpMetricURI = CalculatedGMetricURI
            else:
                tmpMetricURI = PerformanceMetricURI    
            if  unitofmetricNameStr in self.UnitSet:
                self.storeConn.addTriples([(metricIndvdURI, RDF.TYPE, indvdURI),
                                          (metricIndvdURI, RDF.TYPE, tmpMetricURI),
                                          (metricIndvdURI, nameURI, metricNameLitr),
                                          (deviceIndvdURI, measuredByMetricURI, metricIndvdURI),
                                          (metricIndvdURI, hasUnitURI, unitofmetricIndvdURI)
                                          ])                  
            else:            
                self.storeConn.addTriples([(metricIndvdURI, RDF.TYPE, indvdURI),
                                          (metricIndvdURI, RDF.TYPE, tmpMetricURI),
                                          (metricIndvdURI, nameURI, metricNameLitr),
                                          (unitofmetricIndvdURI, RDF.TYPE, indvdURI),
                                          (unitofmetricIndvdURI, RDF.TYPE, UnitURI),
                                          (unitofmetricIndvdURI, nameURI, unitofmetricNameLitr),
                                          (deviceIndvdURI, measuredByMetricURI, metricIndvdURI),
                                          (metricIndvdURI, hasUnitURI, unitofmetricIndvdURI)
                                          ])      
        
   

    def addTripleOfMetrics(self, deviceRaw):
        

        nodeIndvdURI = self.instantiateDevice(deviceRaw)

        self.addTripleOfMetric(nodeIndvdURI, 'Power', 'pdu.power', 'Power Consumption in Watt', 'Watt', 'ObservedGMetric')     
        self.addTripleOfMetric(nodeIndvdURI, 'ActiveEnergy', 'pdu.energy', 'Energy Consumption in kWh', 'kWh', 'ObservedGMetric')
        self.addTripleOfMetric(nodeIndvdURI, 'PowerFactor', 'pdu.pfactor', 'Power Factor. ApparentPower = Power * PowerFactor','None', 'ObservedGMetric')
        self.addTripleOfMetric(nodeIndvdURI, 'Temperature', 'temperature', 'Temperature of a node ', 'Celcius Degree', 'PerfMetric')

        deviceTypeRaw = self.brightConn.getDeviceType(deviceRaw)
        deviceInfoRaw = self.brightConn.getDeviceInfo(deviceRaw)
       
        nodeNameStr = deviceInfoRaw['deviceNamekey']

        if deviceTypeRaw ==  'PhysicalNode' or deviceTypeRaw ==  'MasterNode':
            sysinfoRaw = self.brightConn.getNodeSysinfo(deviceRaw)
            if sysinfoRaw != None :

                CPUIndvdURI = self.instantiateNodeCPU(deviceRaw)
                self.addTripleOfMetric(CPUIndvdURI, 'CPUUtilization', 'CPU.load', 'component= CPU, loadstat= System/User, The utilization of CPU','None', 'PerfMetric')
                self.addTripleOfMetric(CPUIndvdURI, 'FanSpeed', 'CPU.fanspeed', 'component=Fan1,Fan2.. The speed of CPU fan','RPM', 'PerfMetric')

                memIndvdURI = self.instantiateNodeMem(deviceRaw)
                self.addTripleOfMetric(memIndvdURI, 'MemoryUsed', 'Memory.load', 'component=Memory, The size of Memory used','bytes', 'PerfMetric')

                diskInfoRaw = self.brightConn.getDiskInfo(deviceRaw)
                diskSpace = diskInfoRaw['diskSpace']
                for row in diskSpace:
                    diskSpaceNameStr = row[1]
                    diskIndvdURI = self.instantiateNodeDisk(deviceRaw, diskSpaceNameStr)
                    self.addTripleOfMetric(diskIndvdURI, 'DiskUtilization', 'Storage.datatrans', 'component=Disk, The size of Disk used in space like /dev/sda','bytes', 'PerfMetric')
                    self.addTripleOfMetric(diskIndvdURI, 'ResponseTime', 'Storage.iotime', 'component=sda, sdb.. The Response Time of Disk','ms', 'PerfMetric')
            
            allInterfacesRaw = self.brightConn.getAllInterfaces(deviceRaw) 
            for interfaceRaw in allInterfacesRaw: 
                # add the port individual: switch has outbound; switch has inbound;
                # port is individual;  port has name; Type of port is Port
                interfaceInfoRaw = self.brightConn.getInterfaceInfo(deviceRaw, interfaceRaw)
                #nodePortNameStr = nodeNameStr + 'port' +str(interfaceInfoRaw['interfaceNamekey'])
                nodePortIndvdURI = self.instantiateNodePort(deviceRaw, interfaceRaw)
                self.addTripleOfMetric(nodePortIndvdURI, 'IncomingTrafficSize', 'Net.datatrans', 'component= portX, direction=in, The size of incoming traffic in this inferface','bytes', 'PerfMetric')           
                self.addTripleOfMetric(nodePortIndvdURI, 'OutgoingTrafficSize', 'Net.datatrans', 'component= portX, direction= out, The size of outgoing traffic in this inferface','bytes', 'PerfMetric')

                if self.brightConn.getInterfaceType(deviceRaw, interfaceRaw) == 'internalnet':
                # this is a internal network link    
                # add the link individual: * is individual; Type of * is Link; * has name; 
                # link hasSink port; link hasSource port; port isSink link; port isSource link.
                    interface2PortInfoRaw = self.brightConn.getInterface2Port(deviceRaw)
                    
                    if interface2PortInfoRaw != None:
                        # this is a switch link
                        #switchPortNameStr = interface2PortInfoRaw['node2Switchkey'] + 'port' + str(interface2PortInfoRaw['interface2PortNumkey'])
                        interface2PortInfoRaw = self.brightConn.getInterface2Port(deviceRaw)
                        switchRaw = interface2PortInfoRaw['node2Switch']
                        switchPortIndvdURI = self.instantiateSwithPort(switchRaw, str(interface2PortInfoRaw['interface2PortNumkey']))
                        self.addTripleOfMetric(switchPortIndvdURI, 'IncomingTrafficSize', 'net.bytes', 'component= portX, direction=in, The size of incoming traffic in this inferface','bytes', 'PerfMetric')           
                        self.addTripleOfMetric(switchPortIndvdURI, 'OutgoingTrafficSize', 'net.bytes', 'component= portX, direction= out, The size of outgoing traffic in this inferface','bytes', 'PerfMetric')


                 
                    
    def addTripleFromRawdata(self):
        try:         
            allDevicesRaw = self.brightConn.getAllDevices()
            try: 
                for deviceRaw in allDevicesRaw:
                    deviceTypeRaw = self.brightConn.getDeviceType(deviceRaw)
                    if 'PowerDistributionUnit' not in deviceTypeRaw:
                        self.addTripleOfDevice(deviceRaw)
                        #deviceInfoRaw['deviceStatuskey']
                        deviceTypeRaw = self.brightConn.getDeviceType(deviceRaw)
                        
                        if 'Switch' in deviceTypeRaw:
                            self.addTripleOfSwitch(deviceRaw)
                        elif 'Node' in deviceTypeRaw:
                            self.addTripleOfNode(deviceRaw)                  
                        else:
                            raise TripleStoreException('Unknown device type during inserting triples')   
                            return 'error'
                        
                        self.addTripleOfPDU(deviceRaw)
                        self.addTripleOfMetrics(deviceRaw)

                return 'done'
            finally:  
                self.storeConn.close()    
                self.brightConn.disconnect()
          
        except TripleStoreException, e:
            print >>sys.stderr, e 
            self.brightConn.disconnect()
            return 'error'
         

def initStore(algCatalog,AlgRepo):
    store = algCatalog.getRepository(AlgRepo, Repository.RENEW)
    store.initialize()
    return store       

def load_conf(path):
    return json.loads( open(path).read() )  
    
def main():
    absdir, _ = os.path.split(os.path.abspath(__file__))
    confpath  = os.path.join(absdir, 'triplestore.conf')
    conf      = load_conf(confpath)
    AlgHost = str(conf['triplestore']['host'])
    AlgPort = conf['triplestore']['port']
    AlgUser = str(conf['triplestore']['user'])
    AlgPassword = str(conf['triplestore']['passwd'])
    AlgRepo = str(conf['triplestore']['reponame'])

    brighturl = str(conf['manager']['host'])
    pfxfile = str(conf['manager']['pfx'])
    brightpw = str(conf['manager']['passwd'])

    # AlgHost = os.environ.get('AGRAPH_HOST', '192.168.122.173')
    # AlgPort = int(os.environ.get('AGRAPH_PORT', '10035'))
    # #AlgCatalog = 'python-catalog'

    # AlgUser = 'green'
    # AlgPassword = 'clouds'
    
    # AlgRepo = 'EnergyRepo'
    # NMLOWLFile = './nml-base.owl'
    # INDLOWLFile = './indl.owl'
    # EDLOWLFile = './edl-sim.owl'
   
    try: 
        algServer = AllegroGraphServer(AlgHost, AlgPort, AlgUser, AlgPassword)
        algCatalog = algServer.openCatalog() # default root catalog
        #while True:    
        tripleStore = initStore(algCatalog, AlgRepo)
        storeConn = tripleStore.getConnection()
        try:            
            if storeConn.isEmpty():
                for owlfile in conf['triplestore']['ontologies']:
                    print owlfile
                    storeConn.addFile(str(owlfile), format = RDFFormat.RDFXML)

            myTripleStore = TripleStore(storeConn, brighturl, pfxfile, brightpw)
            result = myTripleStore.addTripleFromRawdata()
            if result == 'error':
                raise TripleStoreException('error exists when inserting triples')
            else:
                print "Triples registration finished once"
        finally:
            storeConn.close()
            tripleStore.shutDown()
            #time.sleep(60)    
    except TripleStoreException, e:
        print >> sys.stderr, e   
    
    
if __name__ == '__main__':
    sys.exit(main())

    
    