#!/usr/bin/env python
from datetime import datetime
from itertools import izip_longest
import argparse
import json
import sys
import os
import time
from PerfWrapper import Bright 

def utctime(ts):
	utc = datetime.utcfromtimestamp(ts)
	return float(utc.strftime('%s.%f'))

def utcnow():
	#return utctime(time.time())
	return time.time()

def load_conf(path):
	return json.loads( open(path).read() )



def main(argv):
	absdir, _ = os.path.split(os.path.abspath(__file__))
	confpath  = os.path.join(absdir, 'PerfCollector.conf')
	conf      = load_conf(confpath)
	
	interval = conf['interval']

	pfmetrics = conf['BrightClusterManager']['pfmetrics']
	manager = conf['BrightClusterManager']['perfsource']
	#print manager
	#pdus = [Racktivity(name, host, outlets) for name, host, outlets in conf['pdus']]
	managername, managerurl, cerffile, managerpass = manager
	managercon = Bright(str(managerurl), str(cerffile), str(managerpass))

	while True:
		elapsed = time.time()
		t0 = int(utcnow()*1000)
		pfcollections = [managercon.getMonitoringData(t0, pfmetrics)]
		for c in pfcollections:
			for result in c:
				print result
		sys.stdout.flush()
		elapsed = time.time() - elapsed
		sleep = interval - elapsed
		time.sleep(sleep if sleep > 0 else 0)
	return 0

	managercon.disconnect()

if __name__ == '__main__':
	sys.exit(main(sys.argv))
