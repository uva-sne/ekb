#!/usr/bin/env python
from datetime import datetime
import argparse
import netsnmp
import json
import os
import sys
import time
from PDUWrapper import Racktivity

class PDUCollectorException(Exception):
	pass


def utctime(ts):
	utc = datetime.utcfromtimestamp(ts)
	return float(utc.strftime('%s.%f'))

def utcnow():
	#return float(datetime.utcnow().strftime('%s.%f'))
	return time.time()

def load_conf(path):
	return json.loads( open(path).read() )

def parse_args(args):
	p = argparse.ArgumentParser(prog='PDUCollector', description='Racktivity Collector')
	p.add_argument('--config', help='Path to config file.', default='./PDUCollector.conf')
	return p.parse_args(args)


def main(argv):
#	args = parse_args(argv[1:])
#	conf = load_conf(args.config)
	absdir, _ = os.path.split(os.path.abspath(__file__))
	confpath  = os.path.join(absdir, 'PDUCollector.conf')
	try:
		conf   = load_conf(confpath)
	except:
		raise PDUCollectorException('Error!! The configuration file is not in valid format!!')	
	else:
	
		interval = conf['interval']
		
		Rack_metrics  = conf['Racktivity']['metrics']
		Rack_pdus = [Racktivity(name, host, outlets) for name, host, outlets in conf['Racktivity']['pdus']]


		while True:
			elapsed = time.time()

			t0 = int(utcnow() * 1000.0)
			collections = [pdu.collect(t0, Rack_metrics) for pdu in Rack_pdus]
			for c in collections:
				for result in c:
					print result
			
			#  should add output of other PDUs here
			sys.stdout.flush()

			elapsed = time.time() - elapsed
			sleep   = interval - elapsed
			time.sleep(sleep if sleep > 0 else 0)
		return 0

if __name__ == '__main__':
	sys.exit(main(sys.argv))
