'''
Created on Mar 13, 2013

@author: haozhu
'''
#!/usr/bin/env python
import pythoncm #bright cluster manager api
import sys, os


class BrightCMDException(Exception):
    pass

class Bright(object):
    '''
    classdocs
    '''
    
    def __init__(self, BrightURL = 'https://localhost:8081', pfx = '/home/hzhu/.cm/cmgui/admin.pfx',\
     BrightPassword = 'Re4d0nly' ):
        '''
        Constructor
        '''
        self.manager = pythoncm.ClusterManager()
        self.cluster = self.manager.addCluster(BrightURL, pfx , '', BrightPassword);
        if not self.cluster.connect():
            raise BrightCMDException('Connection failed!')

     
    def disconnect(self):
        if not self.cluster.disconnect():
            raise BrightCMDException('Disconnection failed!')

              
    def getAllDevices(self):
        devices = self.cluster.getAll('Device')
        #print "Return all the devices in this cluster"
        return devices
       
    
    def getMonitoringData(self,ts,metrics):
        devices = self.getAllDevices()
        for device in devices:
            if device.childType == 'PhysicalNode' or device.childType == 'MasterNode':
                if device.sysinfo() != None: 
                    deviceName = device.hostname
                    dataResults = device.latestMonitoringData()
                    for dataResult in dataResults:
                        dmetric = dataResult[0]
                        comlable = dataResult[1]
                        loadstat = 'None'
                        direction = 'None'
                        matchlable = False

                        if not dataResult.count('no data'):
                            for tsdbMetric, component in metrics:
                                timestamp = dataResult[2]*1000
                                val = dataResult[3]
                                scale = 0.001
                                if tsdbMetric == 'load' and component == 'CPU':
                                    if dmetric == 'CPUUser' :
                                        loadstat = 'User' 
                                        matchlable = True
                                        break
                                    elif dmetric == 'CPUSystem': 
                                        loadstat = 'System'
                                        matchlable = True
                                        break
                                    elif dmetric == 'CPUIdle': 
                                        loadstat = 'Idle'
                                        matchlable = True
                                        break   
                                    elif dmetric ==  'CPUWait':
                                        loadstat = "Wait"
                                        matchlable = True
                                        break       

                                elif tsdbMetric == 'fanspeed' and component == 'CPU':
                                    scale = 1
                                    if 'Fan' or 'FAN' in dmetric:
                                        component = dmetric
                                        matchlable = True       
                                        break
                                     
  
                                elif tsdbMetric == 'load' and component == 'Memory':
                                    if dmetric == "MemoryUsed":
                                        matchlable = True
                                        valUsed = dataResult[3]
                                        valTotal = device.sysinfo().memoryTotal
                                        val = float(valUsed/valTotal)
                                        break
  

                                elif tsdbMetric == 'pagefault' and component == 'Memory':
                                    scale = 1 
                                    if dmetric == 'PageFaults':
                                        matchlable = True
                                        loadstat = 'All'
                                        break
                                    elif dmetric == 'MajorPageFaults':
                                        matchlable = True
                                        loadstat = 'Major'
                                        break    


                                elif tsdbMetric == 'iospeed' and component == 'Storage':
                                    if dmetric == "SectorsRead":
                                        direction = 'out'
                                        matchlable = True
                                        component = comlable
                                        break
                                    elif dmetric == "SectorsWritten":
                                        direction = 'in'
                                        matchlable = True
                                        component = comlable
                                        break

 
                                elif tsdbMetric == 'iotime' and component== 'Storage':
                                    if dmetric == 'IOTime':
                                        component = comlable
                                        matchlable = True
                                        break
 
                                elif tsdbMetric == 'iospeed' and component == 'Interface':
                                    scale = 1
                                    if dmetric == 'BytesRecv':
                                        direction = 'in'
                                        component = comlable
                                        matchlable = True
                                        break
                                    elif dmetric == 'BytesSent':
                                        direction = 'out'
                                        component = comlable
                                        matchlable = True
                                        break
                                elif tsdbMetric == 'temperature': 
                                    scale =0.1
                                    if dmetric == 'System_Temp':
                                        loadstat = 'System'
                                        matchlable = True
                                        component = 'None'
                                        break 

                                elif component == 'Switch':
                                    pass

                                else:
                                    
                                    raise BrightCMDException('Wrong metric infomation input: %s, %s' %(tsdbMetric, component))
                                
                            if matchlable:
                                #v = int(1/scale*val) * scale            
                                yield '%s %d %g node.name=%s component=%s loadstat=%s direction=%s' %\
                                (tsdbMetric, timestamp, val, deviceName, component, loadstat, direction)
                else:
                    print 'there is no node data available now'              

            elif device.childType == 'EthernetSwitch':
                    deviceName = device.hostname
                    dataResults = device.latestMonitoringData()
                    if dataResults:
                        for dataResult in dataResults:
                            if dataResult[0] == 'NetworkUtilization' and not dataResult.count('no data'):
                                tsdbMetric = 'load'
                                loadstat = 'None'
                                direction = 'None'
                                component = 'Switch'
                                timestamp = dataResult[2]*1000
                                scale = 0.001
                                val = dataResult[3]
                                #v = int(1/scale*dataResult[3]) * scale
                                yield '%s %d %g node.name=%s component=%s loadstat=%s direction=%s' %\
                                (tsdbMetric, timestamp, val, deviceName, component, loadstat, direction)
                    else:
                        print 'there is no switch data available now'                 






    
                        
