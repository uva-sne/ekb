#!/usr/bin/env python
#author Karel, Hao
from itertools import izip_longest
import netsnmp
import sys
from datetime import datetime


TYPES = {
	'int' : int,
	'float' : float,
	'string' : str
}

fromtimestamp = datetime.fromtimestamp


class PDUWrapperException(Exception):
	pass


def values(v):
	return v

def string(v):
	return tuple(s.rstrip('\x00') for s in v)

def scale(s):
	return lambda x: tuple(s * y for y in x)

def ipaddress(v):
	return tuple('%d.%d.%d.%d' % w for w in v)

def timestamp(v):
	return tuple(fromtimestamp(w) for w in v)

def temperature(v):
	return tuple(0.1 * w - 273.15 for w in v)

def temperature_ts(v):
	return tuple( (0.1 * v[i] - 273.15, fromtimestamp(v[i + 1])) for i in xrange(0, len(v), 2) )

def scale_ts(s):
	return lambda v: tuple((s * v[i], fromtimestamp(v[i + 1])) for i in xrange(0, len(v), 2))


def decoder(oid_end, count, fmt, mapper=None):
	mapper     = mapper or values
	return oid_end, count, fmt, mapper

class Racktivity:
	def __init__(self, name, host, outlets):
		"""
		name: pdu logical name
		host: address:port to snmp interface
		outlets: list of outlets to be collected, or [] for all outlets
		"""
		self.masterMIBbase = '.1.3.6.1.4.1.34097.9.77.1.1.'
		self.powerMIBbase= '.1.3.6.1.4.1.34097.9.80.1.1.'
		self.name = name
		self.session = netsnmp.Session(Version=2, Community='public', DestHost=host,
			Timeout=100000, Retries=0)
		self.outlets  = outlets

		self.powermodule = {

			'pCurrentTime'		  :   		[3, 'TimeTicks', 'string', 's', 0],
			'pVoltage'    		  :         [4, 'Uhundredth', 'float',	'V', 0.01],
			'pFrequency'  		  :         [5, 'Uthousandth', 'float', 'Hz', 0.01],
			'pCurrent'    		  :         [6, 'Uthousandth', 'float', 'A', 0.001],
			'pPower'      		  :         [7, 'Unsigned32', 'int', 'W', 1],
			'pStatePortCur'		  :         [8, 'CURPORTSTATE', 'string', '', 0],
			'pActiveEnergy' 	  :         [9, 'Uthousandth', 'float', 'kWh', 0.001],
			'pApparentEnergy'	  :         [10, 'Unsigned32', 'int', 'W', 1],
			'pTemperature'    	  :         [11, 'Utenth', 'float', 'K', 0.1],
			'pApparentPower'   	  :         [15, 'Unsigned32', 'int', 'W', 1],
			'pPowerFactor'        :         [16, 'Unsigned32', 'float', '', 0.01],
			'pTotalCurrent'       :         [17, 'Uthousandth', 'float', 'A', 0.001],
			'pTotalRealPower'     :         [18, 'Unsigned32', 'int', 'W', 1],
			'pTotalApparentPower' :         [19, 'Unsigned32', 'int', 'W', 1],
			'pTotalActiveEnergy'  :         [20, 'Uthousandth', 'float', 'kWh', 0.001],
			'pTotalApparentEnergy':         [21, 'Uthousandth', 'float', 'kWh', 0.001],
			'pTotalPowerFactor'   :         [22, 'Unsigned32', 'float', '', 0.01 ],
			'pPortName'           :         [10034, 'DisplayString', 'string', 0],
			'pPortState'          :         [10035, 'Unsigned32', 'int', 1],
			'pModuleName'         :         [10001, 'DisplayString', 'string', 0],
			'pFirmwareVersion'    :         [10002, 'Version', 'string', 0],
			'pHardwareVersion'    :         [10003, 'Version', 'string', 0],
			'pFirmwareID'         :         [10004, 'DisplayString', 'string', 0],
			'pHardwareID'         :         [10005, 'DisplayString', 'string', 0]
		}

		self.mastermodule = {
			'mCurrentTime'        :   		[3, 'TimeTicks', 'string', 's',0],
			'mTemperature'        :         [11, 'Utenth', 'float', 'K',0.1],
			'mCurrentIP'          :         [14, 'IpAddress', 'string', '',0],
			'mTotalCurrent'       :         [17, 'Uthousandth', 'float', 'A', 0.001],
			'mTotalRealPower'     :         [18, 'Unsigned32', 'int', 'W', 1],
			'mTotalActiveEnergy'  :         [20, 'Uthousandth', 'float', 'kWh', 0.001],
			'mModuleName'         :         [10001, 'DisplayString', 'string', '', 0],
			'mFirmwareVersion'    :         [10002, 'Version', 'string', '', 0],
			'mHardwareVersion'    :         [10003, 'Version', 'string', '', 0],
			'mFirmwareID'         :         [10004, 'DisplayString', 'string', '',0],
			'mHardwareID'         :         [10005, 'DisplayString', 'string', '',0]
		}

		



	def getconf(self, metrics):	
		oids = []
		types = []	
		scales = []
		for edlmetric, pdumetric in metrics:
			oid_end, pdutype, valuetype, unit, scale = self.powermodule.get(pdumetric, None)
			if pdumetric[0] == 'p': 
				raw_oid = self.powerMIBbase + str(oid_end)
			elif pdumetric[0] == 'm': 
				raw_oid = self.masterMIBbase + str(oid_end)
			else:
				raise PDUWrapperException('Error! Metric input is not in master and power module!!')
			oid = netsnmp.Varbind(raw_oid)
			oids.append(oid)
			types.append(valuetype)
			scales.append(scale)
		return oids, types, scales	

	def getids(self, metrics):
		ids = [] 
		for edlmetric, pdumetric in metrics:
			ident = netsnmp.Varbind(pdumetric)
			ids.append(ident)
		return ids	


	def gettime(self, timeid):
		oid_end, _, _, _, _= self.powermodule.get(timeid, None)
		rawoid = self.powerMIBbase + str(oid_end) 
		oid = netsnmp.Varbind(rawoid)
		results = self.session.walk(netsnmp.VarList(oid))
		#print results
		if results:
			return int(results[0])*1000	

	def getoutletname(self, outletid):
		oid_end, _, _, _, _= self.powermodule.get(outletid, None)
		rawoid = self.powerMIBbase + str(oid_end) 
		oid = netsnmp.Varbind(rawoid)
		idlist = self.session.walk(netsnmp.VarList(oid))
		return idlist


	def singlecollect(self, ts, metric):
		oid, type, scale = self.getconf(metric)	
		results = zip( *(self.session.walk(netsnmp.VarList(oident)) for oident in oid) )
		outlets = self.outlets
		for outlet, val in enumerate(results):
			if (not outlets) or (outlet in outlets):
				edlmetric, pdumetric = metric
				if scale:
					value = float(val) * scale
					ty = type
					value = TYPES[ty](value)
					snmptime = self.gettime('pCurrentTime')
					if snmptime:
						yield '%s %d %g pdu.name=%s pdu.outlet=%d' %\
						(edlmetric, snmptime, value, self.name, outlet)
				else:
					raise PDUWrapperException('Error!! Check metric input, it is not a valid metric!!') 

	def collect(self, ts, metrics):

		oids, types, scales = self.getconf(metrics)	
		results = zip( *(self.session.walk(netsnmp.VarList(oident)) for oident in oids) )
		outlets = self.outlets
		for outlet, vals in enumerate(results):
			if (not outlets) or (outlet in outlets):
				for j, val in enumerate(vals):	
					edlmetric, pdumetric = metrics[j]
					if scales[j]:
						value = float(val) * scales[j]
						ty = types[j]
						value = TYPES[ty](value)
						snmptime = self.gettime('pCurrentTime')
						if snmptime:
							yield '%s %d %g pdu.name=%s pdu.outlet=%d' %\
							(edlmetric, snmptime, value, self.name, outlet)
					else:
						raise PDUWrapperException('Error!! Check metric input, it is not a valid metric!!') 
						

	


if __name__ == '__main__':
	sys.exit(main(sys.argv))
