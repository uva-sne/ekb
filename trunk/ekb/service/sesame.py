#!/usr/bin/env python
"""Python SESAME2 API implementation

SESAME Documentation:
http://www.openrdf.org/doc/sesame2/system/ch08.html
http://www.franz.com/agraph/support/documentation/current/http-protocol.html
"""
#(buggy) import human_curl as requests
import requests
import ujson as json
import rdflib
from rdflib.term import URIRef, Literal, XSDToPython



class SesameException(Exception):
	"""Sesame Exception"""
	pass



class Sesame(object):
	"""Sesame API"""
	def __init__(self, address, auth):
		"""Constructor
		address: HTTP address to repository.
		auth: (username, password)
		"""
		self.__addr = address
		self.__auth = auth
	def _raw_query(self, accept, **params):
		"""Do raw SPARQL request.
		accept: Mimetype of desired output format.
		params: Keyword argument list of request parameters.
		returns: A string with content in format given by accept.
		Throws SesameException if the status code != 200 OK.
		"""
		headers = {'Accept' : accept}
		r = requests.post(self.__addr, data=params, headers=headers, auth=self.__auth)
		if r.status_code != 200:
			raise SesameException(r.content)
		return r.content
	def __parse_row(self, row, variables, datatypes):
		"""Go through all elements in the row and convert them to URIs and Literals."""
		for v in variables:
			col = row.get(v, None)
			if col is None:
				yield None
			else:
				ctype = col['type']
				cval  = col['value']
				if ctype == 'uri':
					yield URIRef(cval)
				elif ctype == 'literal':
					yield Literal(cval)
				elif ctype == 'literal-typed':
					cdtype = URIRef(col['datatype'])
					conv   = datatypes.get(cdtype, None)
					val    = conv(cval) if conv else cval
					yield Literal(val, datatype=cdtype)
				elif ctype == 'bnode':
					yield None	
				else:
					raise SesameException('invalid type: \'%s\'' % ctype)
	def query(self, query, datatypes=XSDToPython, **params):
		accept    = 'application/sparql-results+json'
		content   = self._raw_query(accept, query=query, **params)
		content   = json.loads(content)
		variables = content['head']['vars']
		bindings  = content['results']['bindings']
		rows      = (tuple(self.__parse_row(row, variables, datatypes)) for row in bindings)
		rows      = list(rows)
		return rows, variables



if __name__ == '__main__':
	addr = 'http://localhost:10035/repositories/EnergyRepo'
	s = Sesame(addr, ('green', 'clouds'))
	q = 'select ?s ?p ?o where { ?s ?p ?o }'
	#print s._raw_query('application/sparql-results+json', query=q)
	rows, vars = s.query(q)
	print ' '.join(vars)
	for row in rows:
		print ' '.join(x.n3() for x in row)
