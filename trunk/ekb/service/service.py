#!/usr/bin/env python
import os, time
import bson
import ujson as json
import requests
from datetime import datetime
from flask import Flask, Response, request, abort
from sesame import Sesame
from tsdb import TSDB, TSDBException


def __read_config():
	"""Read the config file given by the environment var GSONARCONF."""
	path = os.getenv('GSONARCONF', None)
	if not path:
		raise Exception('GSONARCONF environment variable not given! Cannot start service.')
	with open(path) as f:
		conf = json.loads(f.read())
		tsdb_host = conf['tsdb']['host']
		tsdb_port = conf['tsdb']['port']
		sesame_repo = conf['sesame']['repo']
		sesame_user = conf['sesame']['user']
		sesame_pass = conf['sesame']['pass']
		return (tsdb_host, tsdb_port), sesame_repo, (sesame_user, sesame_pass)

TSDB_ADDR, SESAME_REPO, SESAME_AUTH = \
	__read_config()

#TSDB_HOST = 'localhost:8080'
#Repos_Addr = 'http://localhost:10035/repositories/EnergyRepo'
#Repos_Acont = 'green'
#Repos_Pass = 'clouds'

SPARQL_PREFIX = """
	PREFIX     owl:<http://www.w3.org/2002/07/owl#>
	PREFIX     rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX     edl:<http://www.semanticweb.org/ontologies/2012/3/edl.owl#>
	PREFIX    rdfs:<http://www.w3.org/2000/01/rdf-schema#>
	PREFIX     xsd:<http://www.w3.org/2001/XMLSchema#>
	PREFIX owl2xml:<http://www.w3.org/2006/12/owl2-xml#>
	PREFIX     cdl:<http://cinegrid.uvalight.nl/owl/cdl/2.0#>
	PREFIX  qosawf:<http://cinegrid.uvalight.nl/owl/qosawf.owl#>
	PREFIX    indl:<http://www.science.uva.nl/research/sne/indl#>
	PREFIX     nml:<http://schemas.ogf.org/nml/2012/10/base#>
	"""

def utcnow():
	#return float(datetime.utcnow().strftime('%s.%f'))
	return float(time.time())

def __sparqlquery(sparql):
	sesameCon = Sesame(SESAME_REPO, SESAME_AUTH)
	return sesameCon.query(sparql)

def suffix_get(rows):
	for row in rows:
		for x in row:
			tmp = str(x).split('#')
			yield tmp[len(tmp)-1]	

def get_outlet(node):
	queryString = SPARQL_PREFIX +\
		""" SELECT ?pduname ?outletname WHERE {
			?node rdf:type nml:Node.
			?node nml:name ?nodename.
			FILTER regex(?nodename, '%s')
			?node edl:attachTo ?outlet.
			?outlet nml:name ?outletname.
			?pdu edl:hasOutlet ?outlet.
			?pdu nml:name ?pduname
		}""" % node
	rows, variables = __sparqlquery(queryString)
	return rows

def get_metric(node):
	queryString = SPARQL_PREFIX +\
		""" SELECT ?metric ?metricname ?metriccomment WHERE {
			?node rdf:type nml:Node. 
			?node nml:name ?nodename.
			FILTER regex(?nodename, '%s')
			?node edl:hasMetric ?metric.
			?metric nml:name ?metricname
		}""" % node
	rows, variables = __sparqlquery(queryString)
	return rows

app = Flask('GreenSonar Service')

@app.route('/')
def hello():
	return 'hello world'

# GET /topology
# Returns a graph of all nodes, ports, and links between them.
@app.route('/topology')
def topology_get():
	queryString = SPARQL_PREFIX + \
		""" SELECT ?node1nm ?port1nm ?node2nm ?port2nm WHERE {
			?node1 rdf:type nml:Node.
			?node1 nml:hasOutboundPort ?port1.
			?port1 nml:isSource ?link.
			?link rdf:type nml:Link.
			?link nml:hasSink ?port2.
			?node2 nml:hasInboundPort ?port2.
			?node1 nml:name ?node1nm.
			?node2 nml:name ?node2nm.
			?port1 nml:name ?port1nm.
			?port2 nml:name ?port2nm.
			FILTER(?node1 != ?node2)
		}"""
	rows, variables = __sparqlquery(queryString)
	content = [dict(node_in=n1, port_in=p1, node_out=n2, port_out=p2) for n1,p1,n2,p2 in rows]
	content = json.dumps(content)
	return Response(content, mimetype='application/json')



@app.route('/topology/metrics')
def topology_metrics_get():
	queryString = SPARQL_PREFIX +\
		""" SELECT ?metric ?metricname ?metriccomment WHERE {
			?metric rdf:type ?metrictype.
			?metric nml:name ?metricname.
			?metric rdfs:comment ?metriccomment.
			{?metrictype rdfs:subClassOf edl:Metric}
			UNION {
				?metrictype rdfs:subClassOf edl:GreenMetric
				}
		}"""
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	#suffix = suffix_get(rows)
	#suffix = list(suffix_get(rows))
	content = [dict(uri=u, name=n, desc=d) for u, n, d in rows]
	content = json.dumps(content)
	return Response(content, mimetype='application/json')

@app.route('/topology/nodes')
def topology_nodes_get():
	queryString = SPARQL_PREFIX + \
		""" SELECT ?nodename WHERE {
			?node rdf:type nml:Node.
			?node nml:name ?nodename 
		}"""
	rows, variables = __sparqlquery(queryString)
	rows = [r for r, in rows]
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')



@app.route('/topology/nodes/attribute/<capability>')
def topology_capbility_get(capability):
	pass



@app.route('/topology/nodes/attribute/<energysource>')
def topology_capbility_get(energysource):
	pass



@app.route('/topology/nodes/attribute/<powerstate>')
def topology_capbility_get(powerstate):
	pass



#@app.route('/topology/nodes/<node>/attribute/outlet')
#def topology_nodes_outlet_get(node):
#	queryString = SPARQL_PREFIX + \
#		""" SELECT ?pduname ?outletname WHERE {
#			?node rdf:type nml:Node.
#			?node nml:name ?nodename.
#			FILTER regex(?nodename, '%s')
#			?node edl:attachTo ?outlet.
#			?outlet nml:name ?outletname.
#			?pdu edl:hasOutlet ?outlet.
#			?pdu nml:name ?pduname
#		}""" % node
#	rows, variables = __sparqlquery(queryString)
#	content = json.dumps(rows)
#	return Response(content, mimetype='application/json')



@app.route('/topology/nodes/<node>/attribute/capability')
def topology_nodes_capbility_get(node):							 
	queryString = SPARQL_PREFIX + \
		""" SELECT ?capability WHERE {
			?node rdf:type nml:Node.
			?node nml:name ?nodename
			FILTER regex(?nodename, '%s')  
			?node indl:hasCapability ?capability
		}""" % node
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')



@app.route('/topology/nodes/<node>/attribute/port')
def topology_nodes_port_get(node):
	queryString = SPARQL_PREFIX + \
		""" SELECT ?port ?portname WHERE {
			?node rdf:type nml:Node. 
			?node nml:name ?nodename. 
			FILTER regex(?nodename, '%s')
			?node nml:hasOutboundPort ?port.
			?port nml:name ?portname
		}""" % node
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')



@app.route('/topology/nodes/<node>/attribute/metrics')
def topology_nodes_metrics_get(node):	
	queryString = SPARQL_PREFIX + \
		""" SELECT ?metric ?metricname WHERE {
			?node rdf:type nml:Node. 
			?node nml:name ?nodename.
			FILTER regex(?nodename, '%s')
			?node edl:measuredByMetric ?metric.
			?metric nml:name ?metricname
		}""" % node
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')



@app.route('/topology/nodes/<node>/attribute/component')
def topology_nodes_metrics_get(node):
	queryString = SPARQL_PREFIX + \
		""" SELECT ?components ?name ?proname ?pro WHERE {
			?node rdf:type nml:Node. 
			?node nml:name ?nodename.
			FILTER regex(?nodename, '%s')
			?node indl:hasComponent ?components.
			?components ?proname ?pro.
			?proname rdf:type owl:DatatypeProperty. 
			?components nml:name ?name
		}""" % node
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')	



@app.route('/topology/nodes/<node>/port/<port>')
def topology_nodes_connport_get(node, port):
	queryString = SPARQL_PREFIX + \
		""" SELECT ?nodename2 ?port12 WHERE {
			?node1 rdf:type nml:Node.
			?node1 nml:name ?nodename. 
			FILTER regex(?nodename, '%s')
			?node1 nml:hasOutboundPort ?port1. 
			?port1 nml:name ?portname. 
			FILTER regex(?portname, '%s')
			?port1 nml:isSource ?link1.
			?link1 rdf:type nml:Link.
			?link1 nml:hasSink ?port12.
			?switch nml:hasInboundPort ?port12.
			?switch nml:name ?nodename2
		}""" % (node, port)
	# ?node2 nml:name ?nodename2. 
	# ?node2 rdf:type nml:Node.
	# ?node2 nml:hasInboundPort ?port2.
	# ?port2 nml:isSink ?link2.
	# ?link2 rdf:type nml:Link.
	# ?link2 nml:hasSource ?port21.
	# ?switch nml:hasOutboundPort ?port21
	# }"""	% (node,port)
	rows, variables = __sparqlquery(queryString)
#	print ' '.join(variables)
	content = json.dumps(rows)
	return Response(content, mimetype='application/json')



@app.route('/topology/pdu/mapping')
def topology_pdu_mapping_get():
	queryString = SPARQL_PREFIX + \
		""" SELECT ?nodename ?pduname ?outletname WHERE {
			?node rdf:type nml:Node.
			?node nml:name ?nodename.
			?node edl:attachTo ?outlet.
			?outlet nml:name ?outletname.
			?pdu edl:hasOutlet ?outlet.
			?pdu nml:name ?pduname
		}"""
	rows, variables = __sparqlquery(queryString)
	output = dict( (n, dict(pdu=p, outlet=o)) for n, p, o in rows )
	content = json.dumps(output)
	#content = json.dumps(rows)
	return Response(content, mimetype='application/json')




def __tsquery(t0, t1, metrics, aggregators):
	ts = TSDB(TSDB_ADDR)
	return ts.query(t0, t1, metrics, aggregators)

def __get_values(query):
	results = query['results'][0]
	return results['name'], results['values']

def __build_response(t0, t1, respo_time, content, fmt='json'):
	# get rid of all the extraneous stuff and
	# just return the data.
	if fmt == 'json':
		content = json.loads(content)
		queries = content['queries']
		data = dict( __get_values(q) for q in queries )
		r = json.dumps({
			'data': data,
			'meta': {
				'start': int(t0*1000),
				'end'  : int(t1*1000),
				'respo_time' : respo_time,
			},
		})
		return Response(r, status=200, mimetype='application/json')
	else: #raw format
		return Response(content, status=200, mimetype='application/json')

def __mkaggregate(aggregate):
	if aggregate:
		aggregate = [a.split(':') for a in aggregate.split(',')]
		# invalid format
		if min(len(a) for a in aggregate) < 2:
			abort(400)
		return [(a[0], int(a[1])) for a in aggregate]
	else:
		return []


# GET /topology/pdu/data/<pdu>/<outlet>?metrics=&start=&end=&aggregrate=
@app.route('/topology/pdu/data/<pdu>/<int:outlet>')
def topology_pdu_data_get(pdu, outlet):
	metrics = request.args.get('metrics', None)
	start   = int(request.args.get('start', 0))
	end     = int(request.args.get('end', 0))

	if (not metrics) or (start == 0 and end == 0):
		abort(400)

	t1 = end   if   end > 0 else utcnow() + end
	t0 = start if start > 0 else t1 + start


	tags = {
		'pdu.name'   : pdu,
		'pdu.outlet' : outlet,
	}

	aggregate = request.args.get('aggregate', None)
	aggregate = __mkaggregate(aggregate)
	metrics   = metrics.split(',')
	metrics   = [(metric, tags, []) for metric in metrics]

	try:
		respo_time, content = __tsquery(t0, t1, metrics, aggregate)
		return __build_response(t0, t1, respo_time, content)
	except TSDBException, e:
		return Response(e, status=400, mimetype='application/json')



#GET /topology/nodes/<node>/data/<component>?metrics=&start=&end=
@app.route('/topology/nodes/<node>/data/<component>')
def topology_nodes_data_get(node, component):
	metrics = request.args.get('metrics', None)
	start   = int(request.args.get('start', 0))
	end     = int(request.args.get('end', 0))

	if (not metrics) or (start == 0 and end == 0):
		abort(400)

	t1 = end   if   end > 0 else utcnow() + end
	t0 = start if start > 0 else t1 + start


	tags = {
		'node.name' : node,
		'component' : component,
#		'loadstat'  : loadstat,
#		'direction' : direction,
	}

	loadstat  = request.args.get('loadstat', None)
	direction = request.args.get('direction', None)
	if loadstat:
		tags['loadstat'] = loadstat
	if direction:
		tags['direction'] = direction

	aggregate = request.args.get('aggregate', None)
	aggregate = __mkaggregate(aggregate)
	metrics   = metrics.split(',')
	metrics   = [(metric, tags, []) for metric in metrics]

	try:
		respo_time, content = __tsquery(t0, t1, metrics, aggregate)
		return __build_response(t0, t1, respo_time, content)
	except TSDBException, e:
		return Response(e, status=400, mimetype='application/json')




if __name__ == '__main__':
	app.run(port=8888, debug=True)
	#app.run(host='0.0.0.0',port=8888)
