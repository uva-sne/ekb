"""
Wrapper for KairosDB API.

See also:
https://code.google.com/p/kairosdb/wiki/QueryMetrics

Default aggregators:
min  - returns the smallest value
max  - returns the largest value
sum  - returns the sum of all values
avg  - returns the average value
dev  - returns the standard deviation
rate - returns the rate of change between pair of data points.
sort - sorts the data points by timestamp. All other aggregators
       sort the values before performing their respective operation.
"""
import requests
import ujson as json
import time
DATAPOINTS_API = 'api/v1/datapoints'

VALID_AGGREGATORS = ['min', 'max', 'sum', 'avg', 'dev', 'rate', 'sort']


def is_valid_aggregate(a):
	return a in VALID_AGGREGATORS

def group_by_tag(tags):
	return [{
		'name' : 'tag',
		'tags' : tags,
	}]

def group_by_time(timespan, count):
	return [{
		'name'        : 'time',
		'group_count' : count,
		'range_size'  : {
			'value'	: timespan,
			'unit'  : 'seconds',
		}
	}]

def group_by_value(size):
	return [{
		'name' : 'value',
		'range_size' : size,
	}]


def _mk_aggregate(name, sampling):
	d = {'name' : name}
	if name != 'rate':
		d['sampling'] = {'value':sampling, 'unit':'seconds'}
	return d


class TSDBException(Exception):
	pass

class TSDB(object):
	def __init__(self, addr):
		host, port = addr
		self.__queryurl  = 'http://%s:%d/%s/query' % (host, port, DATAPOINTS_API)
	def post_data(self, start, end, metrics, aggregators=None, cache_time=0):
		"""
		Constructs a JSON request message for KairosDB.
		start: interval absolute start time in seconds.
		end: interval absolute end time in seconds.
		metrics: dict of metrics and associated tags to retrieve.
		aggregators: list of aggregators and timespans in seconds.
		cache_time: how long to cache the results of this query.
		"""
		aggregators = aggregators or []
#		aggregators = [
#			{'name'     : name,
#			 'sampling' : {'value':value, 'unit':'seconds'} }
#			for name, value in aggregators ]
		aggregators = [_mk_aggregate(name, value) for name, value in aggregators]
		metrics = [
			{'name' : name,
			 'tags' : tags,
			 'aggregators' : aggregators,
			 'group_by' : (group or []) }
			 for name, tags, group in metrics ]
		msg = {
			'start_absolute' : int(start * 1000),
			'end_absolute'   : int(end   * 1000),
			'cache_time'     : int(cache_time),
			'metrics'        : metrics }
		return json.dumps(msg)
	def query(self, start, end, metrics, aggregators=None, cache_time=0):
		post_data = self.post_data(start, end, metrics, aggregators, cache_time)
		start_tstamp = time.time()
		response  = requests.post(self.__queryurl, post_data)
		end_tstamp = time.time()
		respo_time = end_tstamp - start_tstamp
		content   = response.content
		status    = response.status_code
		if status == 200:
			return respo_time,content
		else:
			raise TSDBException(content)



def TSDBCollector(object):
	def __init__(self, addr):
		host, port = addr
		self.__url = 'http://%s:%d/%s' % (host, port, DATAPOINTS_API)
		self.__collect = []
	def insert_many(self, metric, datapoints, **tags):
		points = {
			'name' : metric,
			'datapoints' : datapoints,
			'tags' : tags,
		}
		self.__collect.append(points)
	def insert(self, metric, timestamp, value, **tags):
		point = {
			'name' : metric,
			'timestamp' : timestamp,
			'value' : value,
			'tags' : tags,
		}
		self.__collect.append(point)
	def flush(self, limit=0):
		if limit > 0:
			points = self.__collect[:limit]
		else:
			points = self.__collect
		msg = json.dumps(points)
		response = requests.post(self.__url, msg)
		status = response.status_code
		if status == 204:
			self.__collect = []
			return True
		else:
			raise TSDBException('Failure response: %d' % status)



if __name__ == '__main__':
	from datetime import datetime
	def utcnow():
		return float(datetime.utcnow().strftime('%s.%f'))
	metrics = [
		('pdu.power',  {'pdu.name' : 'uva1', 'pdu.outlet' : '0' }, []),
#		('pdu.power',  {'pdu.name' : 'uva1', 'pdu.outlet' : '1' }, []),
#		('math', {}, group_by_tag(['function'])),
	]
	aggregators = [('avg', 600)]
	end   = utcnow()
	start = end - 3600
	print 'start=%d, end=%d' % (start, end)
	tsdb = TSDB(('localhost', 8080))
	respo_time, content = tsdb.query(start, end, metrics, aggregators)
	content = json.loads(content)
	print respo_time
	print content['queries']
