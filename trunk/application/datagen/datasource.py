import time, os, datetime
import json
import requests
#import simplejson
import sys
import numpy as np


#--------------------------------------------------




class DataException(Exception):
    """Data Collection Exception"""
    pass

class EKBInterface(object):
    def __init__(self, __address, __nodename, starttime, endtime):

        self.address = __address
        self.nodename = __nodename
        self.tstart = starttime
        self.tend = endtime

    def createlist(self,data):
        array=np.array(data)
        return array 

    def createlist_state(self,data1,data2):
        array1=np.array(data1)
        array2=np.array(data2) 
        return array1, array2

    def getpowerdata(self, pdu, pduport, aggr=0 ): 
        url_power = self.address + '/topology/pdu/data/%s/%s?' %(pdu, pduport)
        if aggr:
                params_power = "metrics=pdu.power&start=%s&end=%s&aggregate=avg:%s" \
                %(int(self.tstart), int(self.tend), aggr)
        else:
            params_power = "metrics=pdu.power&start=%s&end=%s" %(int(self.tstart), int(self.tend))      
        results = requests.get(url_power, params = params_power)
        if results.status_code !=200:
                print "response status:%s " % results.status_code
                raise PLOTException(results.status_code)
        content = json.loads(results.text)
        metadata = content['meta']
        data = content['data']['pdu.power']
        
        fdata = open('powerdata.txt', "w", 0)
        fdata.write(results.text)
        fdata.close()
        return self.createlist(data) 
         
       
    def getCPUdata(self): 
        url_power = self.address + '/topology/nodes/%s/data/CPU?' %self.nodename
        params_power = "metrics=CPU.load&start=%s&end=%s" %(int(self.tstart), int(self.tend))      
        results = requests.get(url_power, params = params_power)
        if results.status_code !=200:
                print "response status:%s " % results.status_code
                raise PLOTException(results.status_code)
        content = json.loads(results.text)
        metadata = content['meta']
        data = content['data']['CPU.load']
        
        fdata = open('cpudata.txt', "w", 0)
        fdata.write(results.text)
        fdata.close()
        return self.createlist(data)

    def getCPUdata_state(self): 
        url_power = self.address + '/topology/nodes/%s/data/CPU?' %self.nodename
        params_in = "metrics=CPU.load&start=%s&end=%s&loadstat=System" %(int(self.tstart), int(self.tend))
        params_out = "metrics=CPU.load&start=%s&end=%s&loadstat=User" %(int(self.tstart), int(self.tend))
        results_sys = requests.get(url_power, params = params_in)
        results_user = requests.get(url_power, params = params_out)
        if results_sys.status_code !=200:
            print "response status:%s " % results_sys.status_code
            raise PLOTException(results_sys.status_code)
        if results_user.status_code !=200:
            print "response status:%s " % results_user.status_code
            raise PLOTException(results_user.status_code)
        content_sys = json.loads(results_sys.text)
        metadata_sys = content_sys['meta']
        data_sys = content_sys['data']['CPU.load']
        content_user = json.loads(results_user.text)
        metadata_user = content_user['meta']
        data_user = content_user['data']['CPU.load']
        
        fdata_in = open('cpudata_in.txt', "w", 0)
        fdata_in.write(results_sys.text)
        fdata_in.close()
        fdata_out = open('cpudata_out.txt', "w", 0)
        fdata_out.write(results_user.text)
        fdata_out.close()
        return self.createlist_state(data_sys, data_user)   

    def getMemdata(self): 
        url_power = self.address + '/topology/nodes/%s/data/Memory?' %self.nodename
        params_power = "metrics=Memory.load&start=%s&end=%s" %(int(self.tstart), int(self.tend))      
        results = requests.get(url_power, params = params_power)
        if results.status_code !=200:
                print "response status:%s " % results.status_code
                raise PLOTException(results.status_code)
        content = json.loads(results.text)
        metadata = content['meta']
        data = content['data']['Memory.load']
        
        fdata = open('memdata.txt', "w", 0)
        fdata.write(results.text)
        fdata.close()
        return self.createlist(data)       
        
     

    def getDiskdata_state(self, diskname='sda'): 
        url_power = self.address + '/topology/nodes/%s/data/%s?' %(self.nodename, diskname)
        params_in = "metrics=Storage.datatrans&start=%s&end=%s&direction=in" %(int(self.tstart), int(self.tend))
        params_out = "metrics=Storage.datatrans&start=%s&end=%s&direction=out" %(int(self.tstart), int(self.tend))
        results_in = requests.get(url_power, params = params_in)
        results_out = requests.get(url_power, params = params_out)
        if results_in.status_code !=200:
            print "response status:%s " % results_in.status_code
            raise PLOTException(results_in.status_code)
        if results_out.status_code !=200:
            print "response status:%s " % results_in.status_code
            raise PLOTException(results_in.status_code)
        content_in = json.loads(results_in.text)
        metadata_in = content_in['meta']
        data_in = content_in['data']['Storage.datatrans']
        content_out = json.loads(results_out.text)
        metadata_out = content_out['meta']
        data_out = content_out['data']['Storage.datatrans']
        
        # fdata_in = open('diskdata_in.txt', "w", 0)
        # fdata_in.write(results_in.text)
        # fdata_in.close()
        # fdata_out = open('diskdata_out.txt', "w", 0)
        # fdata_out.write(results_in.text)
        # fdata_out.close()
        return self.createlist_state(data_in, data_out) 

    def getDiskiodata(self, diskname='sda'): 
        url_power = self.address + '/topology/nodes/%s/data/%s?' %(self.nodename, diskname)
        params_power = "metrics=Storage.iotime&start=%s&end=%s" %(int(self.tstart), int(self.tend))      
        results = requests.get(url_power, params = params_power)
        if results.status_code !=200:
                print "response status:%s " % results.status_code
                raise PLOTException(results.status_code)
        content = json.loads(results.text)
        metadata = content['meta']
        data = content['data']['Storage.iotime']
        
        fdata = open('diskiodata.txt', "w", 0)
        fdata.write(results.text)
        fdata.close()
        return self.createlist(data)


    def getNetdata_state(self, ifname='eth0'): 
        url_power = self.address + '/topology/nodes/%s/data/%s?' %(self.nodename, ifname)
        params_in = "metrics=Net.datatrans&start=%s&end=%s&direction=in" %(int(self.tstart), int(self.tend))     
        params_out = "metrics=Net.datatrans&start=%s&end=%s&direction=out" %(int(self.tstart), int(self.tend))      

        results_in = requests.get(url_power, params = params_in)
        results_out = requests.get(url_power, params = params_out)
        if results_in.status_code !=200:
            print "response status:%s " % results_in.status_code
            raise PLOTException(results_in.status_code)
        if results_out.status_code !=200:
            print "response status:%s " % results_in.status_code
            raise PLOTException(results_in.status_code)
        content_in = json.loads(results_in.text)
        metadata_in = content_in['meta']
        data_in = content_in['data']['Net.datatrans']
        content_out = json.loads(results_out.text)
        metadata_out = content_out['meta']
        data_out = content_out['data']['Net.datatrans']
        
        fdata_in = open('netdata_in.txt', "w", 0)
        fdata_in.write(results_in.text)
        fdata_in.close()
        fdata_out = open('netdata_out.txt', "w", 0)
        fdata_out.write(results_out.text)
        fdata_out.close()
        return self.createlist_state(data_in, data_out) 


if __name__ == '__main__':
    sys.exit(main(sys.argv))