import sys
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import dates
from xml.dom import minidom
import xml.etree.ElementTree as et
import time, os, datetime
from pylab import plot, show
from datasource import EKBInterface

#--------------------------------------------------


class PLOTException(Exception):
    pass


def main(argv):

    
    t1 = (2013, 8, 1 , 0, 0, 0, 0, 0, 0)
    t2 = (2013, 10, 1 , 0, 0, 0, 0, 0, 0)

    pdu = 'vu-rpdu6'
    pduport = '1'
    nodename= 'node081'


    tsdata = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
        %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=1)
    timestampe = tsdata[:,0]
    X = tsdata[:,1:-1]
    y = tsdata[:,-1]
    m = len(y)

   

    figure = plt.figure(1,(8,12))

    
    powerts = timestampe/1000
    powerdts= map(datetime.datetime.fromtimestamp, powerts)
    powerfds = dates.date2num(powerdts)
    hfmt = dates.DateFormatter('%m/%d %H:%M')
    
    powerfig = figure.add_subplot(511)
    powerfig.plot(powerfds, y, 'b.', )
    powerfig.xaxis.set_major_locator(dates.DayLocator())
    powerfig.xaxis.set_minor_locator(dates.DayLocator())
    powerfig.xaxis.set_major_formatter(hfmt)
    powerfig.set_title("(a) Power consumption", fontsize=10)
    powerfig.axes.get_xaxis().set_visible(False)
    plt.setp(powerfig.get_xticklabels(), visible=False)
    plt.ylabel('Power consumption (watt)', fontsize=8)


    
    cpufig=figure.add_subplot(512)  
    cpufig.plot(powerfds, X[:,0], color= 'r',label='CPUSystem')
    cpufig.plot(powerfds, X[:,1], color= 'b', label='CPUUser') 
    plt.legend(prop={'size':8}, loc='upper center')
    plt.ylabel('CPU utilization (%)', fontsize=8)
    cpufig.xaxis.set_major_locator(dates.DayLocator())
    cpufig.xaxis.set_minor_locator(dates.HourLocator())
    cpufig.xaxis.set_major_formatter(hfmt)
    plt.setp(cpufig.get_xticklabels(), visible=False)
    cpufig.set_title("(b) CPU load", fontsize=10)


    memfig=figure.add_subplot(513)
    plt.plot(powerfds, X[:,2], color= 'm') 
    memfig.xaxis.set_major_locator(dates.DayLocator())
    memfig.xaxis.set_minor_locator(dates.HourLocator())
    memfig.xaxis.set_major_formatter(hfmt) 
    plt.setp(memfig.get_xticklabels(), visible=False)  
    plt.ylabel('Memory utilization (%)', fontsize=8)
    #x1,x2,y1,y2 = plt.axis()
    #plt.axis((x1,x2,0,0.15))  
    memfig.set_title("(c) Memory load", fontsize=10)
    plt.setp(cpufig.get_xticklabels(), visible=False)


 

    diskiofig= figure.add_subplot(514)
    plt.plot(powerfds, X[:,3], color= 'y', label='sda')  
    plt.plot(powerfds, X[:,4], color= 'r', label = 'sdb')  
    plt.setp(diskiofig.get_xticklabels(), visible=False)
    diskiofig.xaxis.set_major_locator(dates.DayLocator())
    diskiofig.xaxis.set_minor_locator(dates.HourLocator())
    diskiofig.xaxis.set_major_formatter(hfmt)     
    plt.legend(prop={'size':8}, loc='upper center')     
    plt.ylabel(' IO time in sampled time(3min)', fontsize=8)
    diskiofig.set_title("(d) Disk load", fontsize=10)
    #plt.savefig('ppvspower.png')


    

    netfig = figure.add_subplot(515)
    netfig.plot(powerfds, X[:,5], color='r', label='eth0_Received')
    netfig.plot(powerfds, X[:,6], color='b', label='eth0_Sent')
    netfig.plot(powerfds, X[:,7], 'y.', label='ib0_Received')
    netfig.plot(powerfds, X[:,8], 'k.', label='ib0_Sent')
    netfig.xaxis.set_major_locator(dates.DayLocator())
    netfig.xaxis.set_minor_locator(dates.HourLocator())
    netfig.xaxis.set_major_formatter(hfmt)
    plt.xticks(rotation='vertical')
    plt.subplots_adjust(bottom=.3)
    netfig.legend(prop={'size':8}, loc='upper center')           
    plt.ylabel('Bytes/s on NICs', fontsize=8)
    plt.xlabel('Date time (UTC)')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    netfig.set_title("(e) Interfaces load", fontsize=10)
    plt.savefig('allvspower-1015-1030-%s.png' %nodename, bbox_inches='tight')
    #plt.show()

     


    
    


    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
