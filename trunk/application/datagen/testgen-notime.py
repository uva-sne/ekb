import datetime
import csv
import json
import os
import sys
import time
import numpy as np
from matplotlib import dates
from datasource import EKBInterface


#--------------------------------------------------


class PLOTException(Exception):
    pass


def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist    
    return normdatalist

def load_conf(path):
    return json.loads( open(path).read() )  


def main(argv):
    
    t1 = (2013, 8, 1 , 00, 00, 0, 0, 0, 0)
    t2 = (2013, 9, 1 , 00, 00, 0, 0, 0, 0)
    #t1 = (2013, 10, 1 , 00, 00, 0, 0, 0, 0)
    #t2 = (2013, 10, 15 , 00, 00, 0, 0, 0, 0)
    start_epoch = time.mktime(t1) 
    end_epoch = time.mktime(t2)


    pdu = 'vu-rpdu6'
    pduport = '0'
    nodename= 'node079'
    ipaddress = 'http://145.100.132.30:8888'

    absdir, _ = os.path.split(os.path.abspath(__file__))
    confpath  = os.path.join(absdir, 'pdu.conf')
    conf      = load_conf(confpath)
    for pdustruct in conf['pdus']: 
        pdu = pdustruct['name']
        for  nodename, pduport in pdustruct['maps']:   
            fs0 = EKBInterface(ipaddress, nodename, start_epoch, end_epoch)
            print pdu, pduport, nodename
            nodepath = os.path.join(absdir, nodename)
            if not os.path.exists(nodepath):
                os.mkdir(nodepath, 0755)

            time.sleep(10)

            powerarray= fs0.getpowerdata(pdu, pduport, aggr=180) 
            pbool = powerarray[:,1] != 0 
            plist = powerarray[pbool]
            ptslist = powerarray[:,0] + 180*1000
            plist = powerarray[:,1]

            #norm_plist = norm(plist)

            CPUarray_sys, CPUarray_user = fs0.getCPUdata_state()
            #cpulist_total= cpulist_user + cpulist_sys
            #norm_cpulist_total = norm(cpulist_total)
            #norm_cpulist_user = norm(cpulist_user)
            #norm_cpulist_sys = norm(cpulist_sys)


            memarray = fs0.getMemdata()
            #norm_memlist = norm(memlist)
            # disktslist1, disklist_in, disktslist2, disklist_out = getDiskdata_state()
            # nettslist1, netlist_in, nettslist2, netlist_out = getNetdata_state()
            # diskiotslist, diskiolist = getDiskiodata()
            
            #diskioarray_md =  fs0.getDiskiodata('md0')
            diskioarray_sdb = fs0.getDiskiodata('sdb')
            diskioarray_sda = fs0.getDiskiodata('sda')
            #dbool = diskioarraysdb[:,0] != 1378825680000 
            #diskioarraysdb  = diskioarraysdb[dbool]
            #diskiolist_total = diskiolistsda + diskiolistsdb 
            #norm_diskiolist_total = norm(diskiolist_total)
            # norm_diskiolist_sda = norm(diskiolistsda)
            # norm_diskiolist_sdb = norm(diskiolistsdb)
            # norm_diskiolist_md = norm(diskiolistmd)

            netarray_eth0_in, netarray_eth0_out = fs0.getNetdata_state('eth0')
            netarray_ib0_in, netarray_ib0_out = fs0.getNetdata_state('ib0')
            #netlist_total= netlist_in + netlist_out
            #norm_netlist_total = norm(netlist_total)
            # norm_netlist_in = norm(netlist_in)
            # norm_netlist_out = norm(netlist_out)


            cpuinterp_sys = np.interp(ptslist, CPUarray_sys[:,0], CPUarray_sys[:,1])
            cpuinterp_user = np.interp(ptslist, CPUarray_user[:,0], CPUarray_user[:,1])
            meminterp = np.interp(ptslist, memarray[:,0], memarray[:,1])
            diskiointerp_sda = np.interp(ptslist, diskioarray_sda[:,0], diskioarray_sda[:,1])
            diskiointerp_sdb = np.interp(ptslist, diskioarray_sdb[:,0], diskioarray_sdb[:,1])
            #diskiointerp_md = np.interp(ptslist, diskioarray_md[:,0], diskioarray_md[:,1])
            netinterp_eth0_in = np.interp(ptslist, netarray_eth0_in[:,0], netarray_eth0_in[:,1])
            netinterp_eth0_out = np.interp(ptslist, netarray_eth0_out[:,0], netarray_eth0_out[:,1])
            netinterp_ib0_in = np.interp(ptslist, netarray_ib0_in[:,0], netarray_ib0_in[:,1])
            netinterp_ib0_out = np.interp(ptslist, netarray_ib0_out[:,0], netarray_ib0_out[:,1])



            head = ['cpu-sys', 'cpu-user', 'mem', 'sda', 'sdb','eth0:net_in', 'eth0:net_out',\
                    'ib0:net_in', 'ib0:net_out','power']

          
            xy = np.vstack([ cpuinterp_sys, cpuinterp_user, meminterp, \
                diskiointerp_sda, diskiointerp_sdb, netinterp_eth0_in,\
                 netinterp_eth0_out, netinterp_ib0_in, netinterp_ib0_out, plist]).T
            #xy = np.vstack([ cputslist1, xsys, xuser]).T
            with open('./%s/powerdata801-901-shift180-aggr180-nomd-notime-%s.csv' %(nodename, nodename), "w") as fp:
                fdata = csv.writer(fp, delimiter=',')
                fdata.writerow(head)
                fdata.writerows(xy)



    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
