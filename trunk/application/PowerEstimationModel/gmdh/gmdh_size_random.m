%  nodenamelist={'node082' 'node083' 'node084' 'node085' 'node090' 'node091' 'node092' 'node078' ...
%      'node093' 'node079'  'node080' 'node081' 'node086' 'node087' 'node088' 'node089'};
nodenamelist={ 'node083' 'node085' };
%nodenamelist={ 'node079' 'node086' 'node087' 'node088' 'node089' 'node090' 'node091' 'node092' 'node093'};
nodesize = length(nodenamelist);
gmdh = zeros(10, nodesize);
for i =1:nodesize
    node = nodenamelist(i);
    fname = ['./randomsize/2input/mse-gmdh-' char(node) '.txt'];
    randomnum=2;
    Javgmin=10^10;
    critNum=0;

%         for maxNumNeurons=2:1:2
%             for decNumNeurons = 0:1:1
%                 Jtrsum=0;
%                 Jvalsum=0;
%                 Jtestsum=0;
%                 for rand=1:1:randomnum
%                     [trdata, valdata, testdata] = shuffledata(node);
%                     [sizex, sizey] = size(trdata);
%                     
%                     X = trdata(:, 2:sizey-1); 
%                     Y = trdata(:, sizey);
%                     Xval = valdata(:, 2:sizey-1);
%                     Yval = valdata(:, sizey);           
%                     Xtest = testdata(:, 2:sizey-1);
%                     Ytest = testdata(:, sizey);
% 
%                     m = size(X,1)
%                     mval = size(Xval,1)
%                     mtest = size(Xtest,1)
% 
%                     xmax = max(X,[],1);
%                     ymax = max(Y,[],1);
%                     X_norm = X./repmat(xmax,m,1);
%                     Y_norm = Y./ymax;
%                     Xval_norm = Xval./repmat(xmax,mval,1);
%                     Yval_norm = Yval./ymax;
%                     Xtest_norm = Xtest./repmat(xmax,mtest,1);
%                     Ytest_norm = Ytest./ymax;
% 
%                     [model, time] = gmdhbuild(X_norm, Y_norm, 2, 0, maxNumNeurons,...
%                        decNumNeurons, 2, critNum, 0, Xval_norm, Yval_norm, 0); 
%                    
%                     Ypretr_norm = gmdhpredict(model, X_norm);
%                     Ypreval_norm = gmdhpredict(model, Xval_norm);
%                     Ypretest_norm = gmdhpredict(model, Xtest_norm);
%                     bool = isnan(Ypretest_norm)==0;
%                     Ypretest_normbool = Ypretest_norm(bool);
%                     Ytestbool = Ytest(bool);
%             
%                     RMSEtr = sqrt(mean((Ypretr_norm*ymax - Y).^2));
%                     RMSEval = sqrt(mean((Ypreval_norm*ymax - Yval).^2));
%                     RMSEtest = sqrt(mean((Ypretest_normbool*ymax - Ytestbool).^2))                                       
%                     Jtrsum=Jtrsum+ RMSEtr;
%                     Jvalsum=Jvalsum+ RMSEval;
%                     Jtestsum=Jtestsum+ RMSEtest;
%                 end
%             fid = fopen(fname,'a');     
%             fprintf(fid,'%g ',[ Jtrsum/randomnum, Jvalsum/randomnum, Jtestsum/randomnum ...
%                 critNum, maxNumNeurons]);
%             fprintf(fid,'%g\n',decNumNeurons);
%             fclose('all');
%             
%             if ~(isinf(Jtestsum))&( Jtestsum/randomnum< Javgmin) 
%                 Javgmin = Jtestsum/randomnum;
%                 critNummin = critNum;
%                 maxNumNeuronsmin = maxNumNeurons;
%                 decNumNeuronsmin = decNumNeurons;
%             end    
%             end
%         end

    critNummin = 0;
    maxNumNeuronsmin =10;
    decNumNeuronsmin = 1;
    fid = fopen(fname,'a');  
    fprintf(fid, '%g ',  [critNummin, maxNumNeuronsmin]);
    fprintf(fid,'%g\n',decNumNeuronsmin);
    fclose('all');
    
    randomnum=2;
    
    for bita = 0:0.1:0.9
        Jtrsum=0;
        Jvalsum=0;
        Jtestsum=0;
        sr1sum =0;
        sr5sum =0;
        sr10sum = 0;
        for rand=1:1:randomnum
            [trdata, valdata, testdata] = shuffledata(node);
            [sizex, sizey] = size(trdata);
            sizex_train = fix(sizex*bita)+1;
            Xtr = trdata(:, 2:sizey-1); 
            Ytr = trdata(:, sizey);
            X = Xtr(sizex_train:end,:);
            Y = Ytr(sizex_train:end,:);  
            Xval = valdata(:, 2:sizey-1);
            Yval = valdata(:, sizey);           
            Xtest = testdata(:, 2:sizey-1);
            Ytest = testdata(:, sizey);

            m = size(X,1)
            mval = size(Xval,1)
            mtest = size(Xtest,1)

            xmax = max(X,[],1);
            ymax = max(Y,[],1);
            X_norm = X./repmat(xmax,m,1);
            Y_norm = Y./ymax;
            Xval_norm = Xval./repmat(xmax,mval,1);
            Yval_norm = Yval./ymax;
            Xtest_norm = Xtest./repmat(xmax,mtest,1);
            Ytest_norm = Ytest./ymax;

            [model, time] = gmdhbuild(X_norm, Y_norm, 2, 0, maxNumNeuronsmin,...
               decNumNeuronsmin, 2, 0, 0, Xval_norm, Yval_norm, 0); 

            Ypretr_norm = gmdhpredict(model, X_norm);
            Ypreval_norm = gmdhpredict(model, Xval_norm);
            Ypretest_norm = gmdhpredict(model, Xtest_norm);
            bool = isnan(Ypretest_norm)==0;
            Ypretest_normbool = Ypretest_norm(bool);
            Ytestbool = Ytest(bool);
            
            RMSEtr = sqrt(mean((Ypretr_norm*ymax - Y).^2));
            RMSEval = sqrt(mean((Ypreval_norm*ymax - Yval).^2));
            RMSEtest = sqrt(mean((Ypretest_normbool*ymax - Ytestbool).^2));                                      
            Jtrsum=Jtrsum+ RMSEtr;
            Jvalsum=Jvalsum+ RMSEval;
            Jtestsum=Jtestsum+ RMSEtest;
            
            sr1 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.01);
            sr5 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.05);
            sr10 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.1);
            sr1sum = sr1sum + sr1;
            sr5sum = sr5sum +sr5;
            sr10sum = sr10sum + sr10;
        end
        fid = fopen(fname,'a');  
        
        fprintf(fid,'%g ',[ Jtrsum/randomnum, Jvalsum/randomnum, Jtestsum/randomnum ...
            sr1sum/randomnum, sr5sum/randomnum, sr10sum/randomnum]);
        fprintf(fid,'%g\n',bita);
        fclose('all');
    end 
                     
end

