function [trdata, valdata, testdata]=shuffledata(node)    

    data1 = load(['./data/' char(node) '/powerdata801-1001-shift180-aggr180-nomd-' char(node) '.csv']);
    data2 = load(['./data/' char(node) '/powerdata1015-1030-shift180-aggr180-nomd-' char(node) '.csv']);
    data3 = load(['./data/' char(node) '/powerdata1001-1015-shift180-aggr180-nomd-' char(node) '.csv']);
    data4 = load(['./data/' char(node) '/test-' char(node) '.csv']);
    data = [data1; data2; data3; data4];
    [msize, sizey] = size(data);
    ra=randperm(msize);
    datash = data(ra,:);
    
    mtr = fix(msize*0.6);
    mval = fix(msize*0.8);
    
    trdata = datash(1:mtr,:);
    valdata = datash(mtr+1:mval,:);
    testdata = datash(mval+1:end,:);
    
    
    return