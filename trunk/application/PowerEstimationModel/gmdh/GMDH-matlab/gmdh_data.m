%nodenamelist={'node082' 'node083' 'node084' 'node085' 'node090' 'node091' 'node092' 'node078' ...
 %    'node093' 'node079'  'node080' 'node081' 'node086' 'node087' 'node088' 'node089'};
nodenamelist={'node089'};
%nodenamelist={'node078' 'node079' 'node080' 'node081' 'node082' 'node083' 'node084' 'node085'};
nodesize = length(nodenamelist);
gmdh = zeros(10, nodesize);

for i =1:nodesize
    node = nodenamelist(i);
    data = load(['./data/' char(node) '/powerdata801-1001-shift180-aggr180-nomd-' char(node) '.csv']);
    valdata = load(['./data/' char(node) '/powerdata1015-1030-shift180-aggr180-nomd-' char(node) '.csv']);
    testdata = load(['./data/' char(node) '/powerdata1001-1015-shift180-aggr180-nomd-' char(node) '.csv']);
    [sizex, sizey] = size(data);
    con = fix(sizex*0.8);        
    X = data(:, 2:sizey-1); 
    Y = data(:, sizey);
    %X_val = X(con+1:sizex,:); 
    %Y_val = Y(con+1:sizex,:);
    X_val = valdata(:, 2:sizey-1);
    Y_val = valdata(:, sizey);           
    X_test = testdata(:, 2:sizey-1);
    Y_test = testdata(:, sizey);
    fid = fopen(['./size/mse-gmdh-' char(node) '.txt'],'a'); 
    %for critNum=0:1:1
    for bita = 0.1:0.1:1.0

        sizex_train = fix(sizex*bita);
        X_train = X(1:sizex_train,:);
        Y_train = Y(1:sizex_train,:);    

    %   [model, time] = gmdhbuild(Xtr, Ytr, maxNumInputs, inputsMore, maxNumNeurons,
    %                   decNumNeurons, p, critNum, delta, Xv, Yv, verbose)
        [model, time] = gmdhbuild(X_train, Y_train, 2, 0, 18,...
                           0, 2, 0, 0, X_val, Y_val, 0);            
        %gmdheq(model, 10)
        %Yq_train = gmdhpredict(model, X_train);
        %boolt = (isnan(Yq_train)+isinf(Yq_train)==0);
        %Yq_trainbool = Yq_train(boolt);
        %Y_trainbool = Y_train(boolt);
        %MSE_train = 0.5*mean((Yq_trainbool - Y_trainbool).^2)

        Yq = gmdhpredict(model, X_test);
        bool = (isnan(Yq)+isinf(Yq)==0);
        Yqbool = Yq(bool);
        Y_testbool = Y_test(bool);
        m=size(Y_testbool)
        MSE = 0.5*mean((Yqbool - Y_testbool).^2)
        %[MSE, RMSE, RRMSE, R2] = gmdhtest(model, X_test, Y_test);
        %fprintf(fid,'%g ',MSE_train);
        fprintf(fid,'%g ',MSE);
        fprintf(fid,'%g ',time);
        fprintf(fid,'%g\n',bita);
        
        %end

    end
    fprintf(fid,'2 0 18 0 2 0\n');
    fclose('all');
    %csvwrite('mse_gmdh.txt', Yq);

end

