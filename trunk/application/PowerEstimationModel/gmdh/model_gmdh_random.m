% nodenamelist={'node082' 'node083' 'node084' 'node085' 'node090' 'node091' 'node092'  ...
%      'node093' 'node079'  'node080' 'node081' 'node086' 'node087' 'node088' 'node089'};
%nodenamelist={ 'node079'  'node080' 'node081' 'node082' };
%nodenamelist={  'node086' 'node088'  'node090'  'node092' };
%nodenamelist = {'node086'};
%nodenamelist={  'node086'  'node088'  'node090'  'node092' };
 %nodenamelist={ 'node081' 'node082' };
% basenode = {'node080'};
nodenamelist={ 'node083'  'node080' 'node081' 'node082' };
nodesize = length(nodenamelist);
NumNeuron = [2 3 2 2];
boardline = [0 73 133];
for i =1:nodesize
    for j=1:length(boardline)
        fname = ['./2input/randombenchmark/mse-gmdh-linpackcpu.txt'];
        node = nodenamelist(i);
        randomnum=10;
        %testdata = load(['./data/' char(node) '/powerdata115-116-aggr180-nomd-linpack-' char(node) '.csv']);
        %testdata = load(['./data/' char(node) '/powerdata118-aggr180-nomd-linpack-gpu-' char(node) '.csv']);
        %testdata2 = load(['./data/' char(node) '/powerdata225-aggr180-hadoop-' char(node) '.csv']);
        testdata2 = load(['./data/' char(node) '/powerdata325-aggr180-linpack-cpu-' char(node) '.csv']);
        if j ==length(boardline)
            testdata = testdata2(boardline(j)+1:end,:);
        else 
            testdata = testdata2(boardline(j)+1:boardline(j+1),:);
        end
        Jtrsum=0;
        Jvalsum=0;
        Jtestsum=0;
        sr1sum =0;
        sr5sum =0;
        sr10sum = 0;
        for rand=1:1:randomnum
            [trdata, valdata, testdata1] = shuffledata(node);
            [sizex, sizey] = size(trdata);

            X = trdata(:, 2:sizey-1); 
            Y = trdata(:, sizey);
            Xval = valdata(:, 2:sizey-1);
            Yval = valdata(:, sizey);           
            Xtest = testdata(:, 2:sizey-1);

            if strcmp(node, 'node079')
               Ytest = testdata(:, sizey)+61; 
            else   
                Ytest = testdata(:, sizey);
            end   

            m = size(X,1)
            mval = size(Xval,1)
            mtest = size(Xtest,1)

            xmax = max(X,[],1);
            ymax = max(Y,[],1);
            X_norm = X./repmat(xmax,m,1);
            Y_norm = Y./ymax;
            Xval_norm = Xval./repmat(xmax,mval,1);
            Yval_norm = Yval./ymax;
            Xtest_norm = Xtest./repmat(xmax,mtest,1);
            Ytest_norm = Ytest./ymax;

            [model, time] = gmdhbuild(X_norm, Y_norm, 2, 0, NumNeuron(i),...
               1, 2, 0, 0, Xval_norm, Yval_norm, 0); 

            Ypretr_norm = gmdhpredict(model, X_norm);
            Ypreval_norm = gmdhpredict(model, Xval_norm);
            Ypretest_norm = gmdhpredict(model, Xtest_norm);
            bool = isnan(Ypretest_norm)==0;
            Ypretest_normbool = Ypretest_norm(bool);
            Ytestbool = Ytest(bool);

            RMSEtr = sqrt(mean((Ypretr_norm*ymax - Y).^2));
            RMSEval = sqrt(mean((Ypreval_norm*ymax - Yval).^2));
            RMSEtest = sqrt(mean((Ypretest_normbool*ymax - Ytestbool).^2))                                      
            Jtrsum=Jtrsum+ RMSEtr;
            Jvalsum=Jvalsum+ RMSEval;
            Jtestsum=Jtestsum+ RMSEtest;

            sr1 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.01);
            sr5 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.05);
            sr10 = successrate(Ypretest_normbool*ymax, Ytestbool, 0.1);
            sr1sum = sr1sum + sr1;
            sr5sum = sr5sum +sr5;
            sr10sum = sr10sum + sr10;
        end
        fid = fopen(fname,'a');  

        fprintf(fid,'%g ',[ Jtrsum/randomnum, Jvalsum/randomnum, Jtestsum/randomnum ...
            sr1sum/randomnum, sr5sum/randomnum, sr10sum/randomnum]);
        %fprintf(fid,'%g\n',bita);
        fclose('all');
        dlmwrite(fname, node, '-append', 'delimiter','','newline','unix' )
    end
                     
end

