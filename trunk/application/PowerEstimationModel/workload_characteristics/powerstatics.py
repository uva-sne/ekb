import time, os, datetime
import json
import csv
import requests
import sys
from matplotlib import dates
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import scipy.stats as st
import pylab as p
#--------------------------------------------------


class PLOTException(Exception):
    pass

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))

        return data0123


def getnoise(m, step, cpu_norm_sh):
    cpu_filter = np.zeros((m-step+1,1))
    for i in range(m-step+1):

        cpu_filter[i,:] = np.mean(cpu_norm_sh[i:i+step,:])
        cpumax = np.amax(cpu_filter, axis=0)
        cpumin = np.amin(cpu_filter, axis=0)
        cpuavg = np.mean(cpu_filter, axis=0)


        with file('./filter.txt', 'a') as outfile:
            np.savetxt(outfile, np.array([cpumin, cpuavg, cpumax]).reshape(1,3))
            outfile.write(' '+str(nodename)+'\n' )
    return  cpumin, cpuavg. cpumax   











def getdas4data():   
    #nodenamelist = ['node078', 'node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']
    nodenamelist = [ 'node090', 'node091', 'node092',\
     'node093', 'node086', 'node087', 'node088', 'node089']
    totalX = np.zeros((1,9))
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)
        timestampe = data[:,0]
        X = data[:,1:-1]
        y = data[:,-1]
        m = data.shape[0]
        totalX = np.vstack((totalX,X))


        # normsX =  np.amax(X, axis=0)
        # normsy = np.amax(y.reshape(-1,1), axis=0)
        # X_norm = X/normsX.reshape(1,-1)
        # y_norm = y/normsy
    cpudata = map(add, totalX[:,0], totalX[:,1])
    memorydata = totalX[:,2]
    diskiodata = map(add, totalX[:,3], totalX[:,4])


    cpu_norm = np.array(cpudata/np.amax(cpudata))
    memory_norm = np.array(memorydata/np.amax(memorydata))
    diskio_norm = np.array(diskiodata/np.amax(diskiodata))
    print np.amax(cpudata), np.amax(memorydata), np.amax(diskiodata)
    return cpu_norm, memory_norm, diskio_norm



    #for nodename in nodenamelist:
        
    #     print nodename
    #     data = getShuffledData(nodename)


    #     timestampe = data[:,0]
    #     X = data[:,1:-1]
    #     y = data[:,-1]
    #     m = data.shape[0]


    #     # normsX =  np.amax(X, axis=0)
    #     # normsy = np.amax(y.reshape(-1,1), axis=0)
    #     # X_norm = X/normsX.reshape(1,-1)
    #     # y_norm = y/normsy
    #     cpudata = map(add, X[:,0], X[:,1])
    #     cpu_norm = np.array(cpudata/np.amax(cpudata))

    #     memorydata = X[:,2]
    #     memory_norm = np.array(memorydata/np.amax(memorydata))

    #     diskiodata = map(add, X[:,3], X[:,4])
    #     diskio_norm = np.array(diskiodata/np.amax(diskiodata))

    #     if init == 0:
    #         Acpu = cpu_norm
    #         Amem = memory_norm
    #         Adiskio = diskio_norm
    #         init +=1
    #     else:
    #         # Acpu = np.vstack((cpu_norm, Acpu))
    #         # Amem = np.vstack((memory_norm, Amem))
    #         # Adiskio = np.vstack((diskio_norm, Adiskio))
    #         Acpu = np.array(list(Acpu) + list(cpu_norm))
    #         Amem = np.array(list(Amem) + list(memory_norm))
    #         Adiskio = np.array(list(Adiskio) + list(diskio_norm))

    #     # featurenum = X.shape[1]
    #     # step=5
    #     # mfilter = int(m/5)
    #     # m = mfilter*5
    #     # cpu_norm_sh = cpu_norm[0:m,:].reshape(-1,1)
    #     #cpumin, cpuavg, cpumax = getnoise(m, step. cpu_norm_sh)
    #     # sumcpumax = sumcpumax+cpumax
    #     # sumcpumax = sumcpumin + cpumin
    #     # sumcpumax = sumcpumean + cpuavg
def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def main(argv):
    


    nodenamelist = [ 'node090', 'node091', 'node092',\
             'node093', 'node086', 'node087', 'node088', 'node089']
    totaldata= np.zeros((1,11))
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)
        totaldata = np.vstack((totaldata,data))

    totaldata = totaldata[1:,:]
    power = totaldata[:,-1]


    f, (ax1, ax2) = plt.subplots( 2, sharex=True)


    # ecdf1 = sm.distributions.ECDF(power)
    #x1 = np.linspace(100, 400, 1000)
    # y1 = ecdf1(x1) 
    #  ax1.plt(power, y1,'b.')
    bins = range(100,220)

    n, bins, pathches = ax1.hist(power, bins, normed=1, histtype='bar', orientation='horizontal')
    ax1.set_ylabel('Power Consumption (Watt)', fontsize=10)
    ax1.set_title('(a) Hadoop nodes', fontsize=12)


    nodenamelist = ['node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']


    totaldata = np.zeros((1,11))
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)
        totaldata = np.vstack((totaldata,data))

    totaldata = totaldata[1:,:]
    # timestampe = totaldata[:,0]
    # powerts = timestampe/1000
    # powerdts= map(datetime.datetime.fromtimestamp, powerts)
    # powerfds = dates.date2num(powerdts)
    # hfmt = dates.DateFormatter('%m/%d %H:%M')
    power = totaldata[:,-1]
    bins = range(100,350)


    n, bins, pathches = ax2.hist(power, bins, normed=1, histtype='bar', orientation='horizontal')
    ax2.set_xlabel('PDF', fontsize=10)
    ax2.set_ylabel('Power Consumption (Watt)', fontsize=10)
    ax2.set_title('(b) Nodes with GPU', fontsize=12)
    # ecdf2 = sm.distributions.ECDF(power)
    # y2 = ecdf2(x1)
    # ax2.plot(x1, y2, 'k.')
    # ax2.plot(powerfds, power, 'r.')
    # ax1.set_xlabel('Date time')
    # ax1.set_ylabel('Prediction Error', color='r')
    # ax1.xaxis.set_major_locator(dates.DayLocator())
    # ax1.xaxis.set_minor_locator(dates.DayLocator())
    # ax1.xaxis.set_major_formatter(hfmt)
    # plt.xticks(rotation='vertical')




    plt.savefig('./nodes-power.eps')



    # with file('./filter.txt', 'a') as outfile:
    #     np.savetxt(outfile, [sumcpumin/8, sumcpumean/8, sumcpumax/8])
        




if __name__ == '__main__':
    sys.exit(main(sys.argv))   


