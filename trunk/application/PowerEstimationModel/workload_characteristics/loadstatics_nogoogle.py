import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
from operator import add
import matplotlib.pyplot as plt
import pylab as p
import statsmodels.api as sm
from collections import OrderedDict
#--------------------------------------------------


class PLOTException(Exception):
    pass

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))

        return data0123


def getnoise(m, step, cpu_norm_sh):
    cpu_filter = np.zeros((m-step+1,1))
    for i in range(m-step+1):

        cpu_filter[i,:] = np.mean(cpu_norm_sh[i:i+step,:])
        cpumax = np.amax(cpu_filter, axis=0)
        cpumin = np.amin(cpu_filter, axis=0)
        cpuavg = np.mean(cpu_filter, axis=0)


        with file('./filter.txt', 'a') as outfile:
            np.savetxt(outfile, np.array([cpumin, cpuavg, cpumax]).reshape(1,3))
            outfile.write(' '+str(nodename)+'\n' )
    return  cpumin, cpuavg. cpumax   





def getdas4datagpu():   
    nodenamelist = ['node078', 'node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']
    totalX = np.zeros((1,9))
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)
        timestampe = data[:,0]
        X = data[:,1:-1]
        y = data[:,-1]
        m = data.shape[0]
        totalX = np.vstack((totalX,X))


    cpudata = map(add, totalX[:,0], totalX[:,1])
    memorydata = totalX[:,2]
    diskiodata = map(add, totalX[:,3], totalX[:,4])


    cpu_norm = np.array(cpudata/np.amax(cpudata))
    memory_norm = np.array(memorydata/np.amax(memorydata))
    diskio_norm = np.array(diskiodata/np.amax(diskiodata))
    print np.amax(cpudata), np.amax(memorydata), np.amax(diskiodata)
    return cpu_norm, memory_norm, diskio_norm



def getdas4data():   
    #nodenamelist = ['node078', 'node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']
    nodenamelist = [ 'node090', 'node091', 'node092',\
     'node093', 'node086', 'node087', 'node088', 'node089']
    totalX = np.zeros((1,9))
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)
        timestampe = data[:,0]
        X = data[:,1:-1]
        y = data[:,-1]
        m = data.shape[0]
        totalX = np.vstack((totalX,X))


        # normsX =  np.amax(X, axis=0)
        # normsy = np.amax(y.reshape(-1,1), axis=0)
        # X_norm = X/normsX.reshape(1,-1)
        # y_norm = y/normsy
    cpudata = map(add, totalX[:,0], totalX[:,1])
    memorydata = totalX[:,2]
    diskiodata = map(add, totalX[:,3], totalX[:,4])


    cpu_norm = np.array(cpudata/np.amax(cpudata))
    memory_norm = np.array(memorydata/np.amax(memorydata))
    diskio_norm = np.array(diskiodata/np.amax(diskiodata))
    print np.amax(cpudata), np.amax(memorydata), np.amax(diskiodata)
    return cpu_norm, memory_norm, diskio_norm



    #for nodename in nodenamelist:
        
    #     print nodename
    #     data = getShuffledData(nodename)


    #     timestampe = data[:,0]
    #     X = data[:,1:-1]
    #     y = data[:,-1]
    #     m = data.shape[0]


    #     # normsX =  np.amax(X, axis=0)
    #     # normsy = np.amax(y.reshape(-1,1), axis=0)
    #     # X_norm = X/normsX.reshape(1,-1)
    #     # y_norm = y/normsy
    #     cpudata = map(add, X[:,0], X[:,1])
    #     cpu_norm = np.array(cpudata/np.amax(cpudata))

    #     memorydata = X[:,2]
    #     memory_norm = np.array(memorydata/np.amax(memorydata))

    #     diskiodata = map(add, X[:,3], X[:,4])
    #     diskio_norm = np.array(diskiodata/np.amax(diskiodata))

    #     if init == 0:
    #         Acpu = cpu_norm
    #         Amem = memory_norm
    #         Adiskio = diskio_norm
    #         init +=1
    #     else:
    #         # Acpu = np.vstack((cpu_norm, Acpu))
    #         # Amem = np.vstack((memory_norm, Amem))
    #         # Adiskio = np.vstack((diskio_norm, Adiskio))
    #         Acpu = np.array(list(Acpu) + list(cpu_norm))
    #         Amem = np.array(list(Amem) + list(memory_norm))
    #         Adiskio = np.array(list(Adiskio) + list(diskio_norm))

    #     # featurenum = X.shape[1]
    #     # step=5
    #     # mfilter = int(m/5)
    #     # m = mfilter*5
    #     # cpu_norm_sh = cpu_norm[0:m,:].reshape(-1,1)
    #     #cpumin, cpuavg, cpumax = getnoise(m, step. cpu_norm_sh)
    #     # sumcpumax = sumcpumax+cpumax
    #     # sumcpumax = sumcpumin + cpumin
    #     # sumcpumax = sumcpumean + cpuavg

def setAxLinesBW(ax):
    """
    Take each Line2D in the axes, ax, and convert the line style to be 
    suitable for black and white viewing.
    """
    MARKERSIZE = 3

    COLORMAP = {
        'b': {'marker': None, 'dash': (None,None)},
        'y': {'marker': None, 'dash': (None,None)}, #[1,2,1,10]}
        'g': {'marker': None, 'dash': [5,5]},
        'r': {'marker': None, 'dash': [5,3,1,3]},
        'c': {'marker': None, 'dash': [1,3]},
        'k': {'marker': None, 'dash': [5,2,5,2,5,10]},
        'm': {'marker': None, 'dash': [5,3,1,2,1,10]},
        #'k': {'marker': 'o', 'dash': (None,None)} #[1,2,1,10]}
        }

    for line in ax.get_lines() + ax.get_legend().get_lines():
        origColor = line.get_color()
        #line.set_color('black')
        line.set_dashes(COLORMAP[origColor]['dash'])
        line.set_marker(COLORMAP[origColor]['marker'])
        line.set_markersize(MARKERSIZE)

def setFigLinesBW(fig):
    """
    Take each axes in the figure, and for each line in the axes, make the
    line viewable in black and white.
    """
    for ax in fig.get_axes():
        setAxLinesBW(ax)

def main(argv):
    


    f, (ax1, ax2) = plt.subplots( 2, sharex=True)

    Acpu, Amem, Adiskio = getdas4data()
    print Acpu.shape
    ecdf1 = sm.distributions.ECDF(Acpu)
    x1 = np.linspace(0, 1, 1000)
    y1 = ecdf1(x1)
    ax1.step(x1, y1, 'b:', label='CPU load', lw=2)


    ecdf2 = sm.distributions.ECDF(Amem)
    y2 = ecdf2(x1)
    ax1.step(x1, y2, 'y:', label='Memory load', lw=2)


    ecdf3 = sm.distributions.ECDF(Adiskio)
    y3 = ecdf3(x1)
    ax1.step(x1, y3, 'g:', label='Disk load' , lw=2)


    ax1.legend(prop={'size':10},loc='lower right')
    ax1.set_ylabel('CDF')
    ax1.set_title('(a) Hadoop nodes')




    Acpu, Amem, Adiskio = getdas4datagpu()
    print Acpu.shape
    ecdf1 = sm.distributions.ECDF(Acpu)
    x1 = np.linspace(0, 1, 1000)
    y1 = ecdf1(x1)
    ax2.step(x1, y1, 'b:', label='CPU load', lw=2)

    ecdf2 = sm.distributions.ECDF(Amem)
    y2 = ecdf2(x1)
    ax2.step(x1, y2, 'y:', label='Memory load', lw=2 )

    ecdf3 = sm.distributions.ECDF(Adiskio)
    y3 = ecdf3(x1)
    ax2.step(x1, y3, 'g:', label='Disk load' , lw=2)


    
    ax2.legend(prop={'size':10},loc='lower right')
    ax2.set_ylabel('CDF')
    ax2.set_title('(b) Nodes with GPUs')
    ax2.set_xlabel('Resource Load')

    setFigLinesBW(f)
    plt.savefig('./nodes-loaddistribution-googleno.eps')



    # with file('./filter.txt', 'a') as outfile:
    #     np.savetxt(outfile, [sumcpumin/8, sumcpumean/8, sumcpumax/8])
        

#1199.83669782 0.550463353583 1807.98435866 367.1 gpu
#2391.56979205 0.286155987762 2005.2145619 231.448275862 hadoop


if __name__ == '__main__':
    sys.exit(main(sys.argv))   


