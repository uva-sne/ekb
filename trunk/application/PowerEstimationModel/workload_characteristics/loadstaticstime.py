import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
from operator import add
import matplotlib.pyplot as plt
import pylab as p
import statsmodels.api as sm
#--------------------------------------------------


class PLOTException(Exception):
    pass

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))

        return data0123


def getnoise(m, step, cpu_norm_sh):
    cpu_filter = np.zeros((m-step+1,1))
    for i in range(m-step+1):

        cpu_filter[i,:] = np.mean(cpu_norm_sh[i:i+step,:])
        cpumax = np.amax(cpu_filter, axis=0)
        cpumin = np.amin(cpu_filter, axis=0)
        cpuavg = np.mean(cpu_filter, axis=0)


        with file('./filter.txt', 'a') as outfile:
            np.savetxt(outfile, np.array([cpumin, cpuavg, cpumax]).reshape(1,3))
            outfile.write(' '+str(nodename)+'\n' )
    return  cpumin, cpuavg. cpumax      

def main(argv):
    


    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']

    # nodenamelist = ['node078', 'node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']
    # nodenamelist = [ 'node090', 'node091', 'node092',\
    #  'node093', 'node086', 'node087', 'node088', 'node089']
    nodenamelist = [ 'node090']

    init = 0
    for nodename in nodenamelist:
        
        print nodename
        data = getShuffledData(nodename)


        timestampe = data[:,0]
        X = data[:,1:-1]
        y = data[:,-1]
        m = data.shape[0]


        # normsX =  np.amax(X, axis=0)
        # normsy = np.amax(y.reshape(-1,1), axis=0)
        # X_norm = X/normsX.reshape(1,-1)
        # y_norm = y/normsy

        cpudata = map(add, X[:,0], X[:,1])
        cpu_norm = np.array(cpudata/np.amax(cpudata))

        memorydata = X[:,2]
        memory_norm = np.array(memorydata/np.amax(memorydata))

        diskiodata = map(add, X[:,3], X[:,4])
        diskio_norm = np.array(diskiodata/np.amax(diskiodata))

        y_norm = np.array(y/np.amax(y))

        cpu_norm_changelist = []
        memory_norm_changelist = []
        y_norm_changelist = []
        diskio_norm_changelist = []
        for i in range(m-1):
            print cpu_norm[i]
            for j in range(i, m-i):
                cpu_norm_change = np.absolute(cpu_norm[i+j] - cpu_norm[i])
                
                if cpu_norm_change >=1.0/16:
                    stephour = j/20
                    cpu_norm_changelist.append(stephour)
                    break    
            # memory_norm_change = memory_norm[i+step] - memory_norm[i]
            # memory_norm_changelist.append(memory_norm_change)
            # diskio_norm_change = diskio_norm[i+step] - diskio_norm[i]
            # diskio_norm_changelist.append(diskio_norm_change)
            # y_norm_change = y_norm[i+step] - y_norm[i]
            # y_norm_changelist.append(y_norm_change)


        if init == 0:
            Acpu = cpu_norm_changelist
            # Amem = memory_norm_changelist
            # Adiskio = diskio_norm_changelist
            # Ay = y_norm_changelist
            init +=1
        else:
            Acpu = Acpu + cpu_norm_changelist
            # Amem = Amem + memory_norm_changelist
            # Adiskio = Adiskio + diskio_norm_changelist
            # Ay  = Ay + y_norm_changelist

    Acpu = np.array(Acpu)
    # Amem = np.array(Amem)
    # Adiskio = np.array(Adiskio)
    # Ay = np.array(Ay)
    print Acpu.shape

    ecdf1 = sm.distributions.ECDF(Acpu)
    x1 = np.linspace(0, 48, 100)
    y1 = ecdf1(x1)
    plt.step(x1, y1, 'b--', label='CPU load')

    # ecdf2 = sm.distributions.ECDF(Amem)
    # y2 = ecdf2(x1)
    # plt.step(x1, y2, 'y:', label='Memory load')

    # ecdf3 = sm.distributions.ECDF(Adiskio)
    # y3 = ecdf3(x1)
    # plt.step(x1, y3, 'r-.', label='Disk load')

    # ecdf4 = sm.distributions.ECDF(Ay)
    # y4 = ecdf4(x1)
    # plt.step(x1, y4, 'g-', label='Power load')


    plt.legend(prop={'size':10},loc='lower right')
    plt.ylabel('CDF')
    plt.xlabel('1-day Change in Load Utilization')
    plt.title('CDF of Load Utilization of All Hadoop Hosts')





    plt.savefig('./hadoopnodes-1daychangtime.png')



        




if __name__ == '__main__':
    sys.exit(main(sys.argv))   


