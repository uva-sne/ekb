import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
from operator import add
from operator import sub
import matplotlib.pyplot as plt
import pylab as p
import statsmodels.api as sm
from collections import OrderedDict
#--------------------------------------------------


class PLOTException(Exception):
    pass

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))

        return data0123


def getnoise(m, step, cpu_norm_sh):
    cpu_filter = np.zeros((m-step+1,1))
    for i in range(m-step+1):

        cpu_filter[i,:] = np.mean(cpu_norm_sh[i:i+step,:])
        cpumax = np.amax(cpu_filter, axis=0)
        cpumin = np.amin(cpu_filter, axis=0)
        cpuavg = np.mean(cpu_filter, axis=0)


        with file('./filter.txt', 'a') as outfile:
            np.savetxt(outfile, np.array([cpumin, cpuavg, cpumax]).reshape(1,3))
            outfile.write(' '+str(nodename)+'\n' )
    return  cpumin, cpuavg. cpumax 


def getdas4datagpu(step):
    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']

    nodenamelist = ['node078', 'node079', 'node080', 'node081', 'node082', 'node083', 'node084', 'node085']
    cpumax = 2391.56979205 
    memorymax = 0.286155987762 
    diskiomax = 2005.2145619
    ymax = 367.1

    init = 0
    totalX = np.zeros((1,10))
    m=0
    for nodename in nodenamelist:
            
        print nodename
        data = getShuffledData(nodename)
        timestampe = data[:,0]
        X = data[:,1:]
        m = data.shape[0]+m
        totalX = np.vstack((totalX,X))
    y=totalX[:,-1]


    cpudata = map(add, totalX[:,0], totalX[:,1])
    memorydata = totalX[:,2]
    diskiodata = map(add, totalX[:,3], totalX[:,4])
    # cpu_norm = np.array(cpudata/np.amax(cpudata))
    # memory_norm = np.array(memorydata/np.amax(memorydata))
    # diskio_norm = np.array(diskiodata/np.amax(diskiodata))
    # y_norm = np.array(y/np.amax(y))
    cpu_norm = np.array(cpudata)/cpumax
    memory_norm = np.array(memorydata)/memorymax
    diskio_norm = np.array(diskiodata)/diskiomax
    y_norm = np.array(y)/ymax

    print np.amax(cpudata), np.amax(memorydata), np.amax(diskiodata), np.amax(y)


    cpu_norm_changelist = []
    memory_norm_changelist = []
    y_norm_changelist = []
    diskio_norm_changelist = []
    for i in range(int(m/step)-2):
        stepnum = i*step
        cpu_norm_change = np.mean(cpu_norm[stepnum+step:stepnum+2*step]) - np.mean(cpu_norm[stepnum:stepnum+step])
        cpu_norm_changelist.append(cpu_norm_change)
        memory_norm_change = np.mean(memory_norm[stepnum+step:stepnum+2*step]) - np.mean(memory_norm[stepnum:stepnum+step])
        memory_norm_changelist.append(memory_norm_change)
        diskio_norm_change = np.mean(diskio_norm[stepnum+step:stepnum+2*step]) - np.mean(diskio_norm[stepnum:stepnum+step])
        diskio_norm_changelist.append(diskio_norm_change)
        y_norm_change = np.mean(y_norm[stepnum+step:stepnum+2*step]) - np.mean(y_norm[stepnum:stepnum+step])
        y_norm_changelist.append(y_norm_change)


    if init == 0:
        Acpu = cpu_norm_changelist
        Amem = memory_norm_changelist
        Adiskio = diskio_norm_changelist
        Ay = y_norm_changelist
        init +=1
    else:
        Acpu = Acpu + cpu_norm_changelist
        Amem = Amem + memory_norm_changelist
        Adiskio = Adiskio + diskio_norm_changelist
        Ay  = Ay + y_norm_changelist

    Acpu = np.array(Acpu)
    Amem = np.array(Amem)
    Adiskio = np.array(Adiskio)
    Ay = np.array(Ay)   

    return Acpu, Amem, Adiskio, Ay      

def getdas4data(step):
    init = 0
    nodenamelist = [ 'node090', 'node091', 'node092',\
          'node093', 'node086', 'node087', 'node088', 'node089']
    totalX = np.zeros((1,10))
    m=0
    cpumax = 2391.56979205 
    memorymax = 0.286155987762 
    diskiomax = 2005.2145619
    ymax = 367.1

    for nodename in nodenamelist:
            
        print nodename
        data = getShuffledData(nodename)
        timestampe = data[:,0]
        X = data[:,1:]
        m = m+data.shape[0]
        totalX = np.vstack((totalX,X))

    y=totalX[:,-1]
    cpudata = map(add, totalX[:,0], totalX[:,1])
    memorydata = totalX[:,2]
    diskiodata = map(add, totalX[:,3], totalX[:,4])
    # cpu_norm = np.array(cpudata/np.amax(cpudata))
    # memory_norm = np.array(memorydata/np.amax(memorydata))
    # diskio_norm = np.array(diskiodata/np.amax(diskiodata))
    # y_norm = np.array(y/np.amax(y))

    cpu_norm = np.array(cpudata)/cpumax
    memory_norm = np.array(memorydata)/memorymax
    diskio_norm = np.array(diskiodata)/diskiomax
    y_norm = np.array(y)/ymax

    print np.amax(cpudata), np.amax(memorydata), np.amax(diskiodata), np.amax(y)


    cpu_norm_changelist = []
    memory_norm_changelist = []
    y_norm_changelist = []
    diskio_norm_changelist = []
    for i in range(int(m/step)-2):
        stepnum = i*step
        cpu_norm_change = np.mean(cpu_norm[stepnum+step:stepnum+2*step]) - np.mean(cpu_norm[stepnum:stepnum+step])
        cpu_norm_changelist.append(cpu_norm_change)
        memory_norm_change = np.mean(memory_norm[stepnum+step:stepnum+2*step]) - np.mean(memory_norm[stepnum:stepnum+step])
        memory_norm_changelist.append(memory_norm_change)
        diskio_norm_change = np.mean(diskio_norm[stepnum+step:stepnum+2*step]) - np.mean(diskio_norm[stepnum:stepnum+step])
        diskio_norm_changelist.append(diskio_norm_change)
        y_norm_change = np.mean(y_norm[stepnum+step:stepnum+2*step]) - np.mean(y_norm[stepnum:stepnum+step])
        y_norm_changelist.append(y_norm_change)


    if init == 0:
        Acpu = cpu_norm_changelist
        Amem = memory_norm_changelist
        Adiskio = diskio_norm_changelist
        Ay = y_norm_changelist
        init +=1
    else:
        Acpu = Acpu + cpu_norm_changelist
        Amem = Amem + memory_norm_changelist
        Adiskio = Adiskio + diskio_norm_changelist
        Ay  = Ay + y_norm_changelist

        # print nodename
        # data = getShuffledData(nodename)


        # timestampe = data[:,0]
        # X = data[:,1:-1]
        # y = data[:,-1]
        # m = data.shape[0]

        # cpudata = map(add, X[:,0], X[:,1])
        # cpu_norm = np.array(cpudata/np.amax(cpudata))

        # memorydata = X[:,2]
        # memory_norm = np.array(memorydata/np.amax(memorydata))

        # diskiodata = map(add, X[:,3], X[:,4])
        # diskio_norm = np.array(diskiodata/np.amax(diskiodata))

        # y_norm = np.array(y/np.amax(y))

        # cpu_norm_changelist = []
        # memory_norm_changelist = []
        # y_norm_changelist = []
        # diskio_norm_changelist = []
        # for i in range(int(m/step)-2):
        #     stepnum = i*step
        #     cpu_norm_change = np.mean(cpu_norm[stepnum+step:stepnum+2*step]) - np.mean(cpu_norm[stepnum:stepnum+step])
        #     cpu_norm_changelist.append(cpu_norm_change)
        #     memory_norm_change = np.mean(memory_norm[stepnum+step:stepnum+2*step]) - np.mean(memory_norm[stepnum:stepnum+step])
        #     memory_norm_changelist.append(memory_norm_change)
        #     diskio_norm_change = np.mean(diskio_norm[stepnum+step:stepnum+2*step]) - np.mean(diskio_norm[stepnum:stepnum+step])
        #     diskio_norm_changelist.append(diskio_norm_change)
        #     y_norm_change = np.mean(y_norm[stepnum+step:stepnum+2*step]) - np.mean(y_norm[stepnum:stepnum+step])
        #     y_norm_changelist.append(y_norm_change)


        # if init == 0:
        #     Acpu = cpu_norm_changelist
        #     Amem = memory_norm_changelist
        #     Adiskio = diskio_norm_changelist
        #     Ay = y_norm_changelist
        #     init +=1
        # else:
        #     Acpu = Acpu + cpu_norm_changelist
        #     Amem = Amem + memory_norm_changelist
        #     Adiskio = Adiskio + diskio_norm_changelist
        #     Ay  = Ay + y_norm_changelist


    Acpu = np.array(Acpu)
    Amem = np.array(Amem)
    Adiskio = np.array(Adiskio)
    Ay = np.array(Ay)  
    return Acpu, Amem, Adiskio, Ay


def getgoogledata(step):
    #googlefilepath = '/Users/haozhu/Documents/GreenCloud/googleclusterdata/task_usage'
    googlefilepath = '/vmstore/googleclusterevl/googleclusterdata/task_usage'
    
    # googledata1 = np.genfromtxt('%s/part-0000%s.csv' \
    #         %(googlefilepath,0), delimiter=',' ,dtype='float', skip_header=0)
    googledata1 = np.genfromtxt('%s/part-0000%s-of-00500.csv' \
            %(googlefilepath,0), delimiter=',' ,dtype='float', skip_header=0)

    totalgoogledata=np.zeros((1,6))
    machineIDlist = list(OrderedDict.fromkeys(googledata1[:,4]))
    mfile = 10
    for i in range(mfile):
        # googledata = np.genfromtxt('%s/part-0000%s.csv' \
        #     %(googlefilepath,i), delimiter=',' ,dtype='float', skip_header=0)
        googledata = np.genfromtxt('%s/part-0000%s-of-00500.csv' \
                %(googlefilepath,i), delimiter=',' ,dtype='float', skip_header=0)
     
        googledatacon = googledata[:,[0,1,4,5,6,11]]


        indexs = (googledatacon[:,3]>0)
        googledatabool = googledatacon[indexs]
        print googledatabool.shape
        totalgoogledata = np.vstack((totalgoogledata, googledatabool))
    #timelist = list(OrderedDict.fromkeys(totalgoogledata[:,0]))
    timelist =  [(600+j*300)*10**6 for j in range(168)]
    for machine in machineIDlist:
        stepcon = step
        print'machine: %s' %str(machine)
        googledata_machinetimesumlist=[]
        googledata_machinetimemean_pre = [0,0,0,0,0,0]
        machineindexs = (totalgoogledata[:,2]==machine)
        googledata_machinebool = totalgoogledata[machineindexs]
        if len(googledata_machinebool)==0:
            continue
        for stime in timelist:
            timeindexs = (googledata_machinebool[:,0]==float(stime))
            googledata_machinetimebool = googledata_machinebool[timeindexs]
            if len(googledata_machinetimebool)==0:
                continue
            googledata_machinetimesum = np.sum(googledata_machinetimebool,axis=0)
            if stepcon>0:
                googledata_machinetimesumlist.append(googledata_machinetimesum) 
                stepcon-=1 
            else:
                googledata_machinetimemean = np.mean(googledata_machinetimesumlist,axis=0)
                print googledata_machinetimemean
                stepcon = step
                googledata_machinetimesumlist=[]
                if googledata_machinetimemean_pre[2]!=0:
                    googledata_machinetimemean_change = \
                        map(sub, googledata_machinetimemean, googledata_machinetimemean_pre)
                    yield googledata_machinetimemean_change

                googledata_machinetimemean_pre =  googledata_machinetimemean   

                
def setAxLinesBW(ax):
    """
    Take each Line2D in the axes, ax, and convert the line style to be 
    suitable for black and white viewing.
    """
    MARKERSIZE = 3

    COLORMAP = {
        'b': {'marker': None, 'dash': (None,None)},
        'y': {'marker': None, 'dash': (None,None)}, #[1,2,1,10]}
        'g': {'marker': None, 'dash': [5,5]},
        'r': {'marker': None, 'dash': [5,3,1,3]},
        'c': {'marker': None, 'dash': [1,3]},
        'k': {'marker': None, 'dash': [5,2,5,2,5,10]},
        'm': {'marker': None, 'dash': [5,3,1,2,1,10]},
        #'k': {'marker': 'o', 'dash': (None,None)} #[1,2,1,10]}
        }

    for line in ax.get_lines() + ax.get_legend().get_lines():
        origColor = line.get_color()
        #line.set_color('black')
        line.set_dashes(COLORMAP[origColor]['dash'])
        line.set_marker(COLORMAP[origColor]['marker'])
        line.set_markersize(MARKERSIZE)

def setFigLinesBW(fig):
    """
    Take each axes in the figure, and for each line in the axes, make the
    line viewable in black and white.
    """
    for ax in fig.get_axes():
        setAxLinesBW(ax)



def main(argv):
    


    model = 'Hadoop'
    timespan = '2hour'
    steptime = 2*20
    Acpu, Amem, Adiskio, Ay = getdas4data(steptime)




    f, (ax1, ax2) = plt.subplots( 2, sharex=True)

    ecdf1 = sm.distributions.ECDF(Acpu)
    x1 = np.linspace(-0.1, 0.1, 1000)
    y1 = ecdf1(x1)
    ax1.step(x1, y1, 'b:', label='Cluster CPU', lw=2)

    ecdf2 = sm.distributions.ECDF(Amem)
    y2 = ecdf2(x1)
    ax1.step(x1, y2, 'y:', label='Cluster Memory', lw=2)

    ecdf3 = sm.distributions.ECDF(Adiskio)
    y3 = ecdf3(x1)
    ax1.step(x1, y3, 'g:', label='Cluster Disk' , lw=2)

    # ecdf4 = sm.distributions.ECDF(Ay)
    # y4 = ecdf4(x1)
    # ax1.step(x1, y4, 'g', label='Power load', lw=2)



    gstep = 24
    gdata = np.array(list(getgoogledata(gstep)))

    ecdf7 = sm.distributions.ECDF(gdata[:,3])
    x1 = np.linspace(-0.1, 0.1, 1000)
    y7 = ecdf7(x1)
    ax1.step(x1, y7, 'r:', label='Google CPU', lw=2)


    ecdf5 = sm.distributions.ECDF(gdata[:,4])
    y5 = ecdf5(x1)
    ax1.step(x1, y5, 'c:',label='Google Memory', lw=2)

    ecdf6 = sm.distributions.ECDF(gdata[:,5])
    y6 = ecdf6(x1)
    ax1.step(x1, y6, 'k:' ,label='Google Disk', lw=2)
    
    ax1.legend(prop={'size':10},loc='lower right')
    ax1.set_ylabel('CDF')
    # plt.xlabel('%s Change in Load Utilization' %timespan)
    ax1.set_title('(a) Hadoop nodes vs. Google')










    model = 'GPU'
    Acpu, Amem, Adiskio, Ay = getdas4datagpu(steptime)
    
    Acpu = np.array(Acpu)
    Amem = np.array(Amem)
    Adiskio = np.array(Adiskio)
    Ay = np.array(Ay)
    print Acpu.shape

    # plt.subplot(212)
    ecdf1 = sm.distributions.ECDF(Acpu)
    x1 = np.linspace(-0.1, 0.1, 100)
    y1 = ecdf1(x1)
    ax2.step(x1, y1, 'b:', label='Cluster CPU',lw=2)

    ecdf2 = sm.distributions.ECDF(Amem)
    y2 = ecdf2(x1)
    ax2.step(x1, y2, 'y:', label='Cluster Memory', lw=2)

    ecdf3 = sm.distributions.ECDF(Adiskio)
    y3 = ecdf3(x1)
    ax2.step(x1, y3, 'g:', label='Cluster Disk', lw=2)

    # ecdf4 = sm.distributions.ECDF(Ay)
    # y4 = ecdf4(x1)
    # ax2.step(x1, y4, 'g', label='Cluster Power', lw=2)


    ecdf7 = sm.distributions.ECDF(gdata[:,3])
    x1 = np.linspace(-0.1, 0.1, 1000)
    y7 = ecdf7(x1)
    ax2.step(x1, y7, 'r:',  label='Google CPU', lw=2)


    ecdf5 = sm.distributions.ECDF(gdata[:,4])
    y5 = ecdf5(x1)
    ax2.step(x1, y5, 'c:', label='Google Memory', lw=2)

    ecdf6 = sm.distributions.ECDF(gdata[:,5])
    y6 = ecdf6(x1)
    ax2.step(x1, y6, 'k:', label='Google Disk', lw=2)

    ax2.legend(prop={'size':10},loc='lower right')
    ax2.set_ylabel('CDF')
    ax2.set_xlabel('%s Change in Load Utilization' %timespan)
    ax2.set_title('(b) Nodes with GPU vs. Google')
    ax2.set_xlim([-0.1,0.1])
    setFigLinesBW(f)



    plt.savefig('./nodes-%sloadchange-google.eps' %timespan)

#1199.83669782 0.550463353583 1807.98435866 367.1 gpu
#2391.56979205 0.286155987762 2005.2145619 231.448275862 hadoop
        




if __name__ == '__main__':
    sys.exit(main(sys.argv))   


