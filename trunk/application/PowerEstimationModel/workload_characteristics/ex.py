#!/usr/bin/env python
import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
from operator import add
from operator import sub
import matplotlib.pyplot as plt
import pylab as p
import statsmodels.api as sm
from collections import OrderedDict


class PLOTException(Exception):
    pass

def main(argv):
	step=2
	stepcon = 2
    googlefilepath = '/Users/haozhu/Documents/GreenCloud/googleclusterdata/task_usage'
    #googlefilepath = '/vmstore/googleclusterevl/googleclusterdata/task_usage'
    googledata1 = np.genfromtxt('%s/part-0000%s.csv' \
            %(googlefilepath,0), delimiter=',' ,dtype='float', skip_header=0)
    # googledata1 = np.genfromtxt('%s/part-0000%s-of-00500.csv' \
    #         %(googlefilepath,0), delimiter=',' ,dtype='float', skip_header=0)

    totalgoogledata=np.zeros((1,6))
    machineIDlist = list(OrderedDict.fromkeys(googledata1[:,4]))
    mfile = 1
    for i in range(mfile):
        print i
        googledata = np.genfromtxt('%s/part-0000%s.csv' \
            %(googlefilepath,i), delimiter=',' ,dtype='float', skip_header=0)
        # googledata = np.genfromtxt('%s/part-0000%s-of-00500.csv' \
        #      %(googlefilepath,i), delimiter=',' ,dtype='float', skip_header=0)
        googledatacon = googledata[:,[0,1,4,5,6,11]]


        indexs = (googledatacon[:,3]>0)
        googledatabool = googledatacon[indexs]
        totalgoogledata = np.vstack((totalgoogledata, googledatabool.reshape(-1,1)))
    #timelist = list(OrderedDict.fromkeys(totalgoogledata[:,0]))
    print machineIDlist, totalgoogledata.shape
    timelist =  [(600+j*300)*10^6 for j in range(168)]
    for machine in machineIDlist:
        googledata_machinetimesumlist=[]
        googledata_machinetimemean_pre = np.zeros((1,6))
        machineindexs = (totalgoogledata[:,2]==machine)
        googledata_machinebool = totalgoogledata[machineindexs]
        if len(googledata_machinebool)==0:
            continue
        for stime in timelist:
            print stime
            timeindexs = (googledata_machinebool[:,0]==stime)
            googledata_machinetimebool = googledata_machinebool[timeindexs]
            print googledata_machinetimebool
            if len(googledata_machinetimebool)==0:
                continue
            googledata_machinetimesum = np.sum(googledata_machinetimebool,axis=0)
            if stepcon>0:
                googledata_machinetimesumlist.append(googledata_machinetimesum) 
                stepcon-=1 
            else:
                googledata_machinetimemean = np.mean(googledata_machinetimesumlist)
                print googledata_machinetimemean
                stepcon = step
                googledata_machinetimesumlist=[]
                if googledata_machinetimemean_pre[1]!=0:
                    googledata_machinetimemean_change = \
                        map(sub, googledata_machinetimemean, googledata_machinetimemean_pre)
                    print googledata_machinetimemean_change
            googledata_machinetimemean_pre =  googledata_machinetimemean   
            with file('test.txt', 'a') as ftest:
                np.savetxt(ftest, googledata_machinetimemean_pre) 




if __name__ == '__main__':
    sys.exit(main(sys.argv))   