import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np


#--------------------------------------------------


class PLOTException(Exception):
    pass



def main(argv):
    


    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']

    nodenamelist = ['node080', 'node088']

    for nodename in nodenamelist:
        
        print nodename

        # data1 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
        #     %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        # data2 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
        #     %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        # data=np.vstack((data1, data2))

        

        # valdata1 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
        #         %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        # testdata1 = np.genfromtxt('../datagen/%s/test-%s.csv' \
        #         %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        data = np.genfromtxt('../datagen/%s/powerdata1109-1230-aggr180-all-nofan-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        totalen = data.shape[0]

        tsdata = data
        timestampe = tsdata[:,0]
        X = tsdata[:,1:-1]
        y = tsdata[:,-1]
        m = int(totalen)




        featurenum = X.shape[1]
        ccovlist = np.zeros((featurenum,1))
        ex = np.vstack((np.ones(X[:,0].shape),y))
        print np.corrcoef(ex)

        for i in range(featurenum):
            mat = np.vstack((X[:,i], y))
            print i
            print np.corrcoef(mat)
     
        # for i in range(featurenum):
        #     for j in range(featurenum):
        #         if i!=j:
        #             mat = np.vstack((X[:,i], X[:,j]))
        #             print i,j
        #             print np.corrcoef(mat)
        #print ccovlist    


if __name__ == '__main__':
    sys.exit(main(sys.argv))   


