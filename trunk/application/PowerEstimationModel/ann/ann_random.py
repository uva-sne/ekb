import time, os, datetime
import sys
import numpy as np
import pypr.ann as ann
from pypr.helpers.modelwithdata import *
import pypr.optimization as opt
import pypr.ann.activation_functions as af
import random



class PLOTException(Exception):
    pass



def compower(datalist, power):
    powlist = datalist**power
    return powlist
     
def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count     


def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))
        totalnum = data0123.shape[0]
        arr = np.arange(totalnum)
        random.shuffle(arr)
        trainnum = int(totalnum*0.6)
        valnum = int(totalnum*0.8)
        testnum = totalnum - (trainnum+valnum)
        datatrain = data0123[arr[0:trainnum]]
        dataval = data0123[arr[trainnum:valnum]]
        datatest = data0123[arr[valnum:]]
        return datatrain, dataval, datatest





def main(argv):
    

    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']
    # nodenamelist = ['node079', 'node080', 'node081', 'node082']
    # nodenamelist = ['node086', 'node088', 'node090', 'node092']
    # nodenamelist = ['node088', 'node090', 'node092']
    # nodenamebase='node086'
    # nodenamelist = ['node081', 'node082']
    # nodenamebase='node080'
    nodenamelist = ['node083', 'node080', 'node081', 'node082']

    boardline = [0, 73, 133]
    for nodename in nodenamelist:
        for board in range(len(boardline)):
            # testdata = np.genfromtxt('../datagen/%s/powerdata115-116-aggr180-nomd-linpack-%s.csv' \
            #      %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            # testdata = np.genfromtxt('../datagen/%s/powerdata118-aggr180-nomd-linpack-gpu-%s.csv' \
            #             %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            # testdata = np.genfromtxt('../datagen/%s/powerdata225-aggr180-hadoop-%s.csv' \
            #             %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            testdata = np.genfromtxt('../datagen/%s/powerdata325-aggr180-linpack-cpu-%s.csv' \
                            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            if board+1 == len(boardline):
                testdata = testdata[boardline[board]:,:]
            else:
                testdata = testdata[boardline[board]:boardline[board+1],:]         
            timestampetest = testdata[:,0]
            Xtest = testdata[:,1:-1]
            if nodename == 'node079':
                ytest = testdata[:,-1]+ 61
            else:
                ytest = testdata[:,-1]
            mtest = len(ytest)

            outfile = './randombenchmark/mse-ann-linpackcpu-%s.txt' %board
            
            randomnum = 10.0

            Jtestsum=0
            Jtrainsum =0 
            Jvalsum = 0
            sr1sum =0
            sr5sum =0
            sr10sum = 0
            for random_iter in my_range(1, randomnum, 1):

                trdata, valdata, testdata1 = getShuffledData(nodename) 
                X = trdata[:,1:-1]
                y = trdata[:,-1]
                m = len(y)

                Xval = valdata[:,1:-1]
                yval = valdata[:,-1]
                mval = len(yval)



                normsX =  np.amax(X, axis=0)
                normsy = np.amax(y.reshape(-1,1), axis=0)
                X_norm = X/normsX.reshape(1,-1)
                Xtest_norm = Xtest/normsX.reshape(1,-1)
                Xval_norm = Xval/normsX.reshape(1,-1)
                
                yval_norm = yval/normsy
                y_norm = y/normsy
                ytest_norm = ytest/normsy

                
                featurenum = X.shape[1]
                print featurenum, m, mval, mtest

                
                A =  X_norm
                Aval = Xval_norm
                Atest = Xtest_norm
                
                pownum = 1
                while(pownum < 2):    
                    pow_A = compower(X, pownum)
                    pow_Aval = compower(Xval, pownum)
                    pow_Atest = compower(Xtest, pownum)

                    norms_totalA =  np.amax(pow_A, axis=0)
                    pow_A = pow_A/norms_totalA.reshape(1,-1)
                    pow_Aval = pow_Aval/norms_totalA.reshape(1,-1)
                    pow_Atest = pow_Atest/norms_totalA.reshape(1,-1)
                    A = np.hstack((A, pow_A))
                    Aval = np.hstack((Aval, pow_Aval))
                    Atest = np.hstack((Atest, pow_Atest))


                    pownum += 1 
                    featurenum = featurenum*pownum
       

                nn = ann.ANN([featurenum, 2, 1],[af.lin, af.lin])
                #initW = nn.get_flat_weights()
                
                #nn.set_flat_weights(initW)
                #nn.v = 0.0 # Weight decay, just a guess, should actually be found
                dm = ModelWithData(nn, A, y_norm.reshape(-1,1))
                #print nn.weights[0],nn.weights[1]
                err = opt.minimize(dm.get_parameters(), dm.err_func, dm.err_func_d, 500)


                norm_ppower_test = nn.forward(Atest)
                ppower_test = np.array(norm_ppower_test*normsy)
                Jtest_nonorm = np.sqrt(np.mean((ppower_test - ytest.reshape(-1,1))**2))

                norm_ppower_train = nn.forward(A)
                ppower_train = np.array(norm_ppower_train*normsy)
                Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


                norm_ppower_val = nn.forward(Aval)
                ppower_val = np.array(norm_ppower_val*normsy)
                Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                
                Jtestsum = Jtestsum + Jtest_nonorm
                Jvalsum = Jvalsum + Jval_nonorm
                Jtrainsum = Jtrainsum + Jtrain_nonorm



                success_num1 = countsuccess(ppower_test, ytest, 0.01)
                success_num5 = countsuccess(ppower_test, ytest, 0.05)
                success_num10 = countsuccess(ppower_test, ytest, 0.1)
                success_rate10 = success_num10/float(mtest)
                success_rate1 = success_num1/float(mtest)
                success_rate5 = success_num5/float(mtest)
                sr1sum = sr1sum + success_rate1
                sr5sum = sr5sum +success_rate5
                sr10sum = sr10sum + success_rate10

            Jtrainavg = Jtrainsum/randomnum
            Jvalavg = Jvalsum/randomnum
            Jtestavg  =  Jtestsum/randomnum
            success_rate1avg = sr1sum/randomnum
            success_rate5avg = sr5sum/randomnum
            success_rate10avg = sr10sum/randomnum

            fdata = open(outfile, "a", 0)
            fdata.write(str(Jtrainavg))
            fdata.write(' ')
            fdata.write(str(Jvalavg))
            fdata.write(' ')
            fdata.write(str(Jtestavg))
            fdata.write(' ')
            fdata.write(' '+ str(success_rate1avg))
            fdata.write(' '+ str(success_rate5avg))
            fdata.write(' '+ str(success_rate10avg))
            fdata.write(' '+ nodename + '\n')
            fdata.close()





if __name__ == '__main__':
    sys.exit(main(sys.argv))      