import time, os, datetime
import sys
import matplotlib.pyplot as plt
import numpy as np
from pylab import plot, show
from matplotlib import dates
import pypr.ann as ann
from pypr.helpers.modelwithdata import *
import pypr.optimization as opt
import pypr.ann.activation_functions as af



class PLOTException(Exception):
	pass



def compower(datalist, power):
    powlist = datalist**power
    return powlist
     
def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count     


def main(argv):
    fignum=1
    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    # 'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']

    nodenamelist = ['node079', 'node080', 'node081', 'node082']
    for nodename in nodenamelist:
        
        data = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        # data2 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
        #     %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        # data=np.vstack((data1, data2))
        totalen = data.shape[0]
        

        tsdata = data
        timestampe = tsdata[:,0]
        X = tsdata[:,1:-1]
        y = tsdata[:,-1]
        m = int(totalen)

        valdata = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        # testdata = np.genfromtxt('../datagen/%s/powerdata115-116-aggr180-nomd-linpack-%s.csv' \
        #     %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        testdata = np.genfromtxt('../datagen/%s/powerdata118-aggr180-nomd-linpack-gpu-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)


        # testdata1 = np.genfromtxt('../datagen/%s/test-%s.csv' \
        #     %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        # # testdata = np.genfromtxt('../datagen/%s/powerdata922-1002-shift180-aggr180-nomd-%s-del.csv' \
        # #     %(nodenametest,nodenametest), delimiter=',' ,dtype=None, skip_header=1)
        # #testdata = np.vstack((testdata1, testdata2))
                       
        # mval = valdata1.shape[0]
        # mtest = testdata1.shape[0]
        # exmval = int(0.4*mval)
        # exmtest = int(0.4*mtest)
        # valdata = np.vstack((testdata1[:exmtest,:], valdata1[exmval:,:]))
        # testdata = np.vstack((valdata1[:exmval,:], testdata1[exmtest:,:]))

        mval = valdata.shape[0]
        mtest = testdata.shape[0]
        timestampeval = valdata[:,0]
        Xval = valdata[:,1:-1]
        yval = valdata[:,-1] 

        timestampetest = testdata[:,0]
        Xtest = testdata[:,1:-1]
        if nodename == 'node079':
            ytest = testdata[:,-1]+ 61
        else:
            ytest = testdata[:,-1]
        mtest = len(ytest)

      
        normsX =  np.amax(X, axis=0)
        normsy = np.amax(y.reshape(-1,1), axis=0)
        print normsX, normsy
        X_norm = X/normsX.reshape(1,-1)
        Xtest_norm = Xtest/normsX.reshape(1,-1)
        y_norm = y/normsy
        ytest_norm = ytest/normsy




        A =  X_norm
        
        Atest = Xtest_norm
        
        featurenum = X.shape[1]

        print featurenum, totalen, m, mval, mtest


        pownum = 1
        while(pownum < 2):    
            pow_A = compower(X, pownum)
            pow_Atest = compower(Xtest, pownum)


            norms_totalA =  np.amax(np.vstack((pow_A, pow_Atest)), axis=0)
            pow_A = pow_A/norms_totalA.reshape(1,-1)
            pow_Atest = pow_Atest/norms_totalA.reshape(1,-1)


            A = np.hstack((A, pow_A))
            Atest = np.hstack((Atest, pow_Atest))

            size = A.shape
            theta = np.zeros((size[1],1))
            pownum += 1 
            featurenum = featurenum*pownum

        nn = ann.ANN([featurenum, 20, 1],[af.lin, af.lin])
        #nn.v = 0.0 # Weight decay, just a guess, should actually be found
        dm = ModelWithData(nn, A, y_norm.reshape(-1,1))
        #print nn.weights[0],nn.weights[1]


        err = opt.minimize(dm.get_parameters(), dm.err_func, dm.err_func_d, 300)
        norm_ppower = nn.forward(A)
        norm_ppower_test = nn.forward(Atest)
        fig=plt.figure(fignum,(8,10))
        fignum +=1

        powerts = timestampetest/1000
        powerdts= map(datetime.datetime.fromtimestamp, powerts)
        powerfds = dates.date2num(powerdts)
        hfmt = dates.DateFormatter('%m/%d %H:%M')
        Jtest = 0.5*np.mean(err)
        

        # ppower = np.array(norm_ppower*normsy)
        # J_nonorm = 0.5/m*(((ppower - y.reshape(-1,1))**2).sum())       
        
        ppower_error = norm_ppower_test - ytest_norm
        ppower_test = np.array(norm_ppower_test*normsy)
        Jtest_nonorm = 0.5/mtest*(((ppower_test - ytest.reshape(-1,1))**2).sum())
        # with file('./lmntest.txt', 'w') as outfile:
        #      np.savetxt(outfile, max(ppower_test)) 
        #      pos=np.argmax(ppower_test, axis=0)
        #      print pos
        #      np.savetxt(outfile, testdata[1261:1263,:])


        estfig=plt.subplot(211)
        cpufig_total, = plt.plot(powerfds, ytest, 'b', label='Real Power')
        plt.plot(powerfds, ppower_test, 'r', label='Estimated Power')
        estfig.xaxis.set_major_locator(dates.DayLocator())
        estfig.xaxis.set_minor_locator(dates.DayLocator())
        estfig.xaxis.set_major_formatter(hfmt)
        plt.setp(estfig.get_xticklabels(), visible=False)
        #plt.xticks(rotation='vertical')
        plt.legend(prop={'size':6},loc='upper right')
        plt.ylabel('Power consumption(watt)')
        plt.title('Mean square error of test set = %s (%s)' %(Jtest, Jtest_nonorm), fontsize=10)

        #plt.xlabel('Date time')

        success_num1 = countsuccess(ppower_test, ytest, 0.01)
        success_num5 = countsuccess(ppower_test, ytest, 0.05)
        success_num10 = countsuccess(ppower_test, ytest, 0.1)
        success_rate10 = success_num10/float(mtest)
        success_rate1 = success_num1/float(mtest)
        success_rate5 = success_num5/float(mtest)
        fdata = open('./mse-ann-linpack.txt', "a", 0)
        # fdata.write(str(J_nonorm))
        # fdata.write( ' ')
        fdata.write(str(Jtest_nonorm))
        fdata.write(' '+ str(success_rate1))
        fdata.write(' '+ str(success_rate5))
        fdata.write(' '+ str(success_rate10))
        fdata.write(' '+ nodename + '\n')
        fdata.close()

        ax1=plt.subplot(212)
        plt.xticks(rotation='vertical')
        #avg_error=np.average(ppower_error)
        #std_error=np.std(ppower_error)
        cpuinterp_totalval = Xtest[:,0] + Xtest[:,1]

        print  Jtest
        
        ax1.plot(powerfds, ppower_error, 'r-')
        ax1.set_xlabel('Date time')
        ax1.set_ylabel('Prediction Error', color='r')
        ax2=ax1.twinx()
        ax2.plot(powerfds, cpuinterp_totalval, 'b.')
        ax2.set_ylabel('CPU load', color='b')
        ax1.xaxis.set_major_locator(dates.DayLocator())
        ax1.xaxis.set_minor_locator(dates.DayLocator())
        ax1.xaxis.set_major_formatter(hfmt)
        plt.xticks(rotation='vertical')

        plt.subplots_adjust(bottom=.3)

        plt.savefig('./error-115-116-linpack-%s.png' %nodename, bbox_inches='tight')
        #plt.savefig('./error-0801-0901-0922-1002--%s-pred-%s-del.png' %(nodename,nodenametest), bbox_inches='tight')


if __name__ == '__main__':
    sys.exit(main(sys.argv))      