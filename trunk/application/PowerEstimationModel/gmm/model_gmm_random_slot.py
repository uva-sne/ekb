import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
import pypr.clustering.gmm as gmm
import random


#--------------------------------------------------


class PLOTException(Exception):
    pass

def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist    
    return normdatalist

def computecost(yhat, y):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    mid = yhat-y
    Jsq = mid**2
    J = 0.5/m*Jsq.sum()
    return J


def prediction(msize, X, centroids, ccov,p_k ):
    A_pred = np.hstack((X, np.nan*np.ones(msize).reshape(-1,1)))
    y_pred = np.zeros((msize,1))
    for i in range(msize):
        cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(A_pred[i,:]),\
            centroids, ccov, p_k)
        for j in range(len(cen_cond)):
            if any(new_mc_i ==True for new_mc_i in np.isnan(mc_cond)):
                mc_cond = 0.1*np.ones(np.array(cen_cond).shape)
                mc_cond = mc_cond/np.sum(mc_cond)
            y_pred[i] = y_pred[i] + (cen_cond[j]*mc_cond[j])

            # nanbool =  np.isnan(ytest_pred) == False
            # ytest_pred = ytest_pred[nanbool]
            # ytest_norm = ytest_norm[nanbool]
            # ytest = ytest[nanbool]
            # timestampetest = timestampetest[nanbool]
    return y_pred        

def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count


def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))
        totalnum = data0123.shape[0]
        arr = np.arange(totalnum)
        random.shuffle(arr)
        trainnum = int(totalnum*0.6)
        valnum = int(totalnum*0.8)
        testnum = totalnum - (trainnum+valnum)
        datatrain = data0123[arr[0:trainnum]]
        dataval = data0123[arr[trainnum:valnum]]
        datatest = data0123[arr[valnum:]]
        return datatrain, dataval, datatest



def main(argv):
    



    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']
    #nodenamelist = ['node079', 'node080', 'node081', 'node082']
    #nodenamelist = ['node086', 'node088', 'node090', 'node092']
    #nodenamelist = ['node088', 'node090', 'node092']
    # nodenamelist = ['node081', 'node082']
    #nodenamebase='node086'
    nodenamelist = ['node083', 'node080', 'node081', 'node082']

    boardline = [0, 73, 133]

    for nodename in nodenamelist:

            outfile = './randombenchmark/mse-gmm-linpackcpuslot.txt' 
            # testdata = np.genfromtxt('../datagen/%s/powerdata115-116-aggr180-nomd-linpack-%s.csv' \
            #      %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            # testdata = np.genfromtxt('../datagen/%s/powerdata118-aggr180-nomd-linpack-gpu-%s.csv' \
            #             %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            # testdata = np.genfromtxt('../datagen/%s/powerdata225-aggr180-hadoop-%s.csv' \
            #             %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
            #testdata = testdata[:120,:]
            testdata = np.genfromtxt('../datagen/%s/powerdata325-aggr180-linpack-cpu-%s.csv' \
                            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)


            
            Xtest = testdata[:,1:-1]
            if nodename == 'node079':
                ytest = testdata[:,-1]+ 61
            else:
                ytest = testdata[:,-1]
            mtest = len(ytest)



            randomnum = 10.0
            Jtestsum=0
            Jtestsum0=0
            Jtestsum1=0
            Jtestsum2=0                                    
            Jtrainsum =0 
            Jvalsum = 0 
            sr1sum =0
            sr5sum =0
            sr10sum = 0
            for random_iter in my_range(1, randomnum, 1):

                trdata, valdata, testdatatmp = getShuffledData(nodename) 
                X = trdata[:,1:-1]
                y = trdata[:,-1]
                m = len(y)

                Xval = valdata[:,1:-1]
                yval = valdata[:,-1]
                mval = len(yval)



                normsX =  np.amax(X, axis=0)
                normsy = np.amax(y.reshape(-1,1), axis=0)
                X_norm = X/normsX.reshape(1,-1)
                Xtest_norm = Xtest/normsX.reshape(1,-1)
                Xval_norm = Xval/normsX.reshape(1,-1)
                
                yval_norm = yval/normsy
                y_norm = y/normsy
                ytest_norm = ytest/normsy

                cpu_norm=X_norm[:,1]*normsX[1]+X_norm[:,0]*normsX[0]
                print cpu_norm
                bool1 = np.where(cpu_norm<=100)
                X_norm1 = X_norm[bool1]
                X1shape = X_norm1.shape
                y_norm1 = y_norm[bool1]

                bool2 = np.where(np.logical_and(cpu_norm>100, cpu_norm<=600))
                X_norm2 = X_norm[bool2]
                X2shape = X_norm2.shape
                y_norm2 = y_norm[bool2]


                bool3 = np.where(cpu_norm>600)
                X_norm3 = X_norm[bool3]
                X3shape = X_norm3.shape
                y_norm3 = y_norm[bool3]
                
                featurenum = X.shape[1]
                print featurenum, m, mval, mtest

                
                A1 = np.hstack((X_norm1, y_norm1.reshape(-1,1)))
                A2 = np.hstack((X_norm2, y_norm2.reshape(-1,1)))
                A3 = np.hstack((X_norm3, y_norm3.reshape(-1,1)))

                Aval = np.hstack((Xval_norm, yval_norm.reshape(-1,1)))
                Atest = np.hstack((Xtest_norm, ytest_norm.reshape(-1,1)))
       

                cluster_init_kw = {'cluster_init':'kmeans', 'max_init_iter':5, \
                    'cov_init':'var', 'verbose':False}
                centroids1, ccov1, p_k1, logL1 = gmm.em_gm(A1, K = 10, max_iter = 500, verbose = False, \
                    init_kw= cluster_init_kw, max_tries=10)
                centroids2, ccov2, p_k2, logL2 = gmm.em_gm(A2, K = 5, max_iter = 500, verbose = False, \
                    init_kw= cluster_init_kw, max_tries=10)
                centroids3, ccov3, p_k3, logL3 = gmm.em_gm(A3, K = 5, max_iter = 500, verbose = False, \
                    init_kw= cluster_init_kw, max_tries=10)

                Xtest_norm0 = Xtest_norm[0:73]
                Xtest_norm1 = Xtest_norm[73:132]
                Xtest_norm2 = Xtest_norm[132:]

                ytest0 = ytest.reshape(-1,1)[0:73]
                ytest1 = ytest.reshape(-1,1)[73:132]
                ytest2 = ytest.reshape(-1,1)[132:]

                mtest0 = len(Xtest_norm0)
                mtest1 = len(Xtest_norm1)
                mtest2 = len(Xtest_norm2)

                print mtest0, mtest1, mtest2

                norm_ppower_test0 = prediction(mtest0, Xtest_norm0, centroids2, ccov2, p_k2)
                ppower_test0 = np.array(norm_ppower_test0*normsy)
                Jtest_nonorm0 = np.sqrt(np.mean((ppower_test0 - ytest0.reshape(-1,1))**2))

                norm_ppower_test1 = prediction(mtest1, Xtest_norm1, centroids3, ccov3, p_k3)
                ppower_test1 = np.array(norm_ppower_test1*normsy)
                Jtest_nonorm1 = np.sqrt(np.mean((ppower_test1 - ytest1.reshape(-1,1))**2))

                norm_ppower_test2 = prediction(mtest2, Xtest_norm2, centroids3, ccov3, p_k3)
                ppower_test2 = np.array(norm_ppower_test2*normsy)
                Jtest_nonorm2 = np.sqrt(np.mean((ppower_test2 - ytest2.reshape(-1,1))**2))

                # norm_ppower_train = prediction(m, X_norm, centroids1, ccov1, p_k1)
                # ppower_train = np.array(norm_ppower_train*normsy)
                # Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


                # norm_ppower_val = prediction(mval, Xval_norm, centroids1, ccov1, p_k1)
                # ppower_val = np.array(norm_ppower_val*normsy)
                # Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                
                Jtestsum0 = Jtestsum0 + Jtest_nonorm0
                Jtestsum1 = Jtestsum1 + Jtest_nonorm1
                Jtestsum2 = Jtestsum2 + Jtest_nonorm2
                # Jvalsum = Jvalsum + Jval_nonorm
                # Jtrainsum = Jtrainsum + Jtrain_nonorm



                # success_num1 = countsuccess(ppower_test, ytest, 0.01)
                # success_num5 = countsuccess(ppower_test, ytest, 0.05)
                # success_num10 = countsuccess(ppower_test, ytest, 0.1)
                # success_rate10 = success_num10/float(mtest)
                # success_rate1 = success_num1/float(mtest)
                # success_rate5 = success_num5/float(mtest)
                # sr1sum = sr1sum + success_rate1
                # sr5sum = sr5sum +success_rate5
                # sr10sum = sr10sum + success_rate10

            Jtrainavg = Jtrainsum/randomnum
            # Jvalavg = Jvalsum/randomnum
            Jtestavg0  =  Jtestsum0/randomnum
            Jtestavg1  =  Jtestsum1/randomnum
            Jtestavg2  =  Jtestsum2/randomnum            
            # success_rate1avg = sr1sum/randomnum
            # success_rate5avg = sr5sum/randomnum
            # success_rate10avg = sr10sum/randomnum

            # fdata = open(outfile, "a", 0)
            # fdata.write(str(Jtrainavg))
            # fdata.write(' ')
            # fdata.write(str(Jvalavg))
            # fdata.write(' ')
            # fdata.write(str(Jtestavg0))
            # fdata.write(' ')
            # fdata.write(' '+ str(success_rate1avg))
            # fdata.write(' '+ str(success_rate5avg))
            # fdata.write(' '+ str(success_rate10avg))
            # fdata.write(' '+ nodename + '\n')
            # fdata.close()

            fdata = open(outfile, "a", 0)
            fdata.write(str(Jtestavg0))
            fdata.write(' ')
            fdata.write(str(Jtestavg1))
            fdata.write(' ')
            fdata.write(str(Jtestavg2))
            fdata.write(' ')
            # fdata.write(' '+ str(success_rate1avg))
            # fdata.write(' '+ str(success_rate5avg))
            # fdata.write(' '+ str(success_rate10avg))
            fdata.write(' '+ nodename + '\n')
            fdata.close()
 




    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
