import time, os, datetime
import json
import requests
import sys
import matplotlib.pyplot as plt
import numpy as np
from pylab import plot, show
import pypr.clustering.gmm as gmm
from matplotlib import dates

#--------------------------------------------------


class PLOTException(Exception):
    pass

def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist    
    return normdatalist

def computecost(yhat, y):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    mid = yhat-y
    Jsq = mid**2
    J = 0.5/m*Jsq.sum()
    return J

def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count

def main(argv):
    
    t1 = (2013, 8, 1 , 0, 0, 0, 0, 0, 0)
    t2 = (2013, 10, 1 , 0, 0, 0, 0, 0, 0)
    start_epoch = time.mktime(t1) 
    end_epoch = time.mktime(t2)

    nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']


    ttest1 = (2013, 10, 1 , 0, 0, 0, 0, 0, 0)
    ttest2 = (2013, 10, 15 , 0, 0, 0, 0, 0, 0)

    fignum = 1
    for nodename in nodenamelist:
        data = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
                %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=1)
        totalen = data.shape[0]
        
        m = int(totalen*0.8)
        tsdata = data[:m,:]
        timestampe = tsdata[:,0]
        X = tsdata[:,1:-1]
        y = tsdata[:,-1]
        

        mval = totalen - m
        valdata = data[m:,:]
        timestampeval = valdata[:,0]
        Xval = valdata[:,1:-1]
        yval = valdata[:,-1]

        testdata = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=1)
        # testdata = np.genfromtxt('../datagen/%s/powerdata922-1002-shift180-aggr180-nomd-%s-del.csv' \
        #     %(nodenametest,nodenametest), delimiter=',' ,dtype=None, skip_header=1)
        timestampetest = testdata[:,0]
        Xtest = testdata[:,1:-1]
        ytest = testdata[:,-1]
        mtest = len(ytest)



        X1 = X[np.where(X[:,1]<=1)]
        X1shape = X1.shape
        y1 = y[:X1shape[0]]

        masker2 = X[X1shape[0]:,:]
        X2 = masker2[np.where(masker2[:,1]<=10)]
        X2shape = X2.shape
        y2 = y[X1shape[0]:X1shape[0]+X2shape[0]]  

        masker3 = masker2[X2shape[0]:,:]
        #masker3 = X3[np.where(X3[:,1]<=10)]
        X3 = masker3
        #X3shape = X3.shape
        y3 = y[X1shape[0]+X2shape[0]:]

        print y1.shape, y2.shape,  y3.shape, X3.shape


        # with file('./GMMslot.txt', 'w') as outfile:
        #     np.savetxt(outfile, X3)




        normsX =  np.amax(np.vstack((X, Xval)), axis=0)
        normsy = np.amax(np.vstack((y.reshape(-1,1), yval.reshape(-1,1))), axis=0)
        print normsX, normsy
        X_norm = X/normsX.reshape(1,-1)
        Xtest_norm = Xtest/normsX.reshape(1,-1)
        y_norm = y/normsy
        ytest_norm = ytest/normsy

        X1_norm = X1/normsX.reshape(1,-1)
        y1_norm = y1/normsy
        X2_norm = X2/normsX.reshape(1,-1)
        y2_norm = y2/normsy
        X3_norm = X3/normsX.reshape(1,-1)
        y3_norm = y3/normsy




        featurenum = X.shape[1]
        colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        colorlines = ['b-.', 'g-.', 'r-.', 'c-.', 'm-.', 'y-.', 'k-.']
        print featurenum, m, mval


        A = np.hstack((X_norm, y_norm.reshape(-1,1)))
        A1 = np.hstack((X1_norm, y1_norm.reshape(-1,1)))
        A2 = np.hstack((X2_norm, y2_norm.reshape(-1,1)))
        A3 = np.hstack((X3_norm, y3_norm.reshape(-1,1)))
        Atest = np.hstack((Xtest_norm, ytest_norm.reshape(-1,1)))
       
        kpara = 10
        #centroids, ccov, p_k, logL = gmm.em_gm(A, K = kpara, max_iter = 800, verbose = True)
        centroids1, ccov1, p_k1, logL1 = gmm.em_gm(A1, K = 5, max_iter = 800, verbose = True)
        centroids2, ccov2, p_k2, logL2 = gmm.em_gm(A2, K = 5, max_iter = 800, verbose = True)
        centroids3, ccov3, p_k3, logL3 = gmm.em_gm(A3, K = 5, max_iter = 800, verbose = True)
        Atest_pred = np.hstack((Xtest_norm, np.nan*np.ones(mtest).reshape(mtest,1)))
        ytest_pred = np.zeros(ytest.shape)
        for i in range(mtest):
            if Atest_pred[i,1]*normsX[1] <=1:
                cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(Atest_pred[i,:]),\
                    centroids1, ccov1, p_k1)
            elif Atest_pred[i,1]*normsX[1] >1 and Atest_pred[i,1]*normsX[1] <=10:  
                cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(Atest_pred[i,:]),\
                        centroids2, ccov2, p_k2) 
            else:
                cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(Atest_pred[i,:]),\
                        centroids3, ccov3, p_k3)             
            for j in range(len(cen_cond)):
                ytest_pred[i] = ytest_pred[i] + (cen_cond[j]*mc_cond[j])
           

        nanbool =  np.isnan(ytest_pred) == False
        ytest_pred = ytest_pred[nanbool]
        ytest_norm = ytest_norm[nanbool]
        ytest = ytest[nanbool]
        timestampetest = timestampetest[nanbool]
        mtest_bool = len(ytest)
      
        fig=plt.figure(fignum,(8,10))
        fignum+=1

        powerts = timestampetest/1000
        powerdts= map(datetime.datetime.fromtimestamp, powerts)
        powerfds = dates.date2num(powerdts)
        hfmt = dates.DateFormatter('%m/%d %H:%M')





        Jtest = computecost(ytest_pred, ytest_norm)
        print 'Mean square error of validation set: %s' %Jtest
        ppower_error = ytest_pred - ytest_norm
        ppower_test = ytest_pred*normsy
        Jtest_nonorm = 0.5/mtest_bool*((ppower_test - ytest)**2).sum()

        estfig=plt.subplot(211)
        cpufig_total, = plt.plot(powerfds, ytest, 'b', label='Real Power')
        plt.plot(powerfds, ppower_test, 'r', label='Estimated Power')
        estfig.xaxis.set_major_locator(dates.DayLocator())
        estfig.xaxis.set_minor_locator(dates.DayLocator())
        estfig.xaxis.set_major_formatter(hfmt)
        plt.setp(estfig.get_xticklabels(), visible=False)
        #plt.xticks(rotation='vertical')
        plt.legend(prop={'size':6},loc='upper right')
        plt.ylabel('Power consumption(watt)')
        plt.title('Mean square error of test set = %s (%s)' %(Jtest, Jtest_nonorm))

        #plt.xlabel('Date time')

        success_num1 = countsuccess(ppower_test, ytest, 0.01)
        success_num5 = countsuccess(ppower_test, ytest, 0.05)
        success_num10 = countsuccess(ppower_test, ytest, 0.1)
        success_rate1 = success_num1/float(mtest_bool)
        success_rate5 = success_num5/float(mtest_bool)
        success_rate10 = success_num10/float(mtest_bool)

        fdata = open('./mse-gmmslot-15.txt', "a", 0)
        fdata.write(str(Jtest_nonorm))
        fdata.write(' '+ str(success_rate1))
        fdata.write(' '+ str(success_rate5))
        fdata.write(' '+ str(success_rate10))
        fdata.write(' '+ nodename + '\n')
        fdata.close()

        ax1=plt.subplot(212)
        plt.xticks(rotation='vertical')
        avg_error=np.average(ppower_error)
        std_error=np.std(ppower_error)
        cpuinterp_totalval = Xtest[:,0] + Xtest[:,1]
        print  Jtest, avg_error, std_error
        
        # fdata = open('./linear-m/error-l3-0825-0902-0911-0912.txt', "w", 0)
        # fdata.write(error)
        # fdata.close()
        ax1.plot(powerfds, ppower_error, 'r-')
        ax1.set_xlabel('Date time')
        ax1.set_ylabel('Prediction Error', color='r')
        ax2=ax1.twinx()
        ax2.plot(powerfds, cpuinterp_totalval[nanbool], 'b.')
        ax2.set_ylabel('CPU load', color='b')
        ax1.xaxis.set_major_locator(dates.DayLocator())
        ax1.xaxis.set_minor_locator(dates.DayLocator())
        ax1.xaxis.set_major_formatter(hfmt)
        plt.xticks(rotation='vertical')

        plt.subplots_adjust(bottom=.3)

        plt.savefig('./error-0801-1001-1001-1015-k=15-slot-%s.png' %nodename, bbox_inches='tight')



#0.00452849159419


    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
