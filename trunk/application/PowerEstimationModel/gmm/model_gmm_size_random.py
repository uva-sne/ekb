import time, os, datetime
import json
import csv
import requests
import sys
import numpy as np
import pypr.clustering.gmm as gmm
import random


#--------------------------------------------------


class PLOTException(Exception):
    pass

def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist    
    return normdatalist

def computecost(yhat, y):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    mid = yhat-y
    Jsq = mid**2
    J = 0.5/m*Jsq.sum()
    return J


def prediction(msize, X, centroids, ccov,p_k ):
    A_pred = np.hstack((X, np.nan*np.ones(msize).reshape(-1,1)))
    y_pred = np.zeros((msize,1))
    for i in range(msize):
        cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(A_pred[i,:]),\
            centroids, ccov, p_k)
        for j in range(len(cen_cond)):
            if any(new_mc_i ==True for new_mc_i in np.isnan(mc_cond)):
                mc_cond = 0.1*np.ones(np.array(cen_cond).shape)
                mc_cond = mc_cond/np.sum(mc_cond)
            y_pred[i] = y_pred[i] + (cen_cond[j]*mc_cond[j])

            # nanbool =  np.isnan(ytest_pred) == False
            # ytest_pred = ytest_pred[nanbool]
            # ytest_norm = ytest_norm[nanbool]
            # ytest = ytest[nanbool]
            # timestampetest = timestampetest[nanbool]
    return y_pred        

def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count


def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))
        totalnum = data0123.shape[0]
        arr = np.arange(totalnum)
        random.shuffle(arr)
        trainnum = int(totalnum*0.6)
        valnum = int(totalnum*0.8)
        testnum = totalnum - (trainnum+valnum)
        datatrain = data0123[arr[0:trainnum]]
        dataval = data0123[arr[trainnum:valnum]]
        datatest = data0123[arr[valnum:]]
        return datatrain, dataval, datatest



def main(argv):
    



    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    #  'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']
    #nodenamelist = [ 'node082', 'node083', 'node084']
    nodenamelist = ['node079']

    for nodename in nodenamelist:

        outfile = './randomsize/mse-gmm-%s.txt' %nodename
        
        Jvalavgmin = 10**100
        randomnum = 2.0
        randomnum1 = 10.0
        # for kpara in my_range(5, 20, 5):

        #     Jtestsum=0
        #     Jtrainsum =0 
        #     Jvalsum = 0

        #     for random_iter in my_range(1, randomnum1, 1):
        #         trdata, valdata, testdata  = getShuffledData(nodename)

        #         X = trdata[:,1:-1]
        #         y = trdata[:,-1]
        #         m = len(y)

        #         Xval = valdata[:,1:-1]
        #         yval = valdata[:,-1]
        #         mval = len(yval)

        #         Xtest = testdata[:,1:-1]
        #         ytest = testdata[:,-1]
        #         mtest = len(ytest)

        #         normsX =  np.amax(X, axis=0)
        #         normsy = np.amax(y.reshape(-1,1), axis=0)
        #         X_norm = X/normsX.reshape(1,-1)
        #         Xtest_norm = Xtest/normsX.reshape(1,-1)
        #         Xval_norm = Xval/normsX.reshape(1,-1)
                
        #         yval_norm = yval/normsy
        #         y_norm = y/normsy
        #         ytest_norm = ytest/normsy

                
        #         featurenum = X.shape[1]
        #         print featurenum, m, mval, mtest

                
        #         A = np.hstack((X_norm, y_norm.reshape(-1,1)))
        #         Aval = np.hstack((Xval_norm, yval_norm.reshape(-1,1)))
        #         Atest = np.hstack((Xtest_norm, ytest_norm.reshape(-1,1)))
       

        #         cluster_init_kw = {'cluster_init':'kmeans', 'max_init_iter':5, \
        #             'cov_init':'var', 'verbose':False}
        #         centroids, ccov, p_k, logL = gmm.em_gm(A, K = kpara, max_iter = 800, verbose = False, \
        #             init_kw= cluster_init_kw, max_tries=10)

        #         norm_ppower_test = prediction(mtest, Xtest_norm, centroids, ccov, p_k)
        #         ppower_test = np.array(norm_ppower_test*normsy)
        #         Jtest_nonorm = np.sqrt(np.mean((ppower_test - ytest.reshape(-1,1))**2))

        #         norm_ppower_train = prediction(m, X_norm, centroids, ccov, p_k)
        #         ppower_train = np.array(norm_ppower_train*normsy)
        #         Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


        #         norm_ppower_val = prediction(mval, Xval_norm, centroids, ccov, p_k)
        #         ppower_val = np.array(norm_ppower_val*normsy)
        #         Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                
        #         Jtestsum = Jtestsum + Jtest_nonorm
        #         Jvalsum = Jvalsum + Jval_nonorm
        #         Jtrainsum = Jtrainsum + Jtrain_nonorm



        #     Jtrainavg = Jtrainsum/randomnum1
        #     Jvalavg = Jvalsum/randomnum1
        #     Jtestavg  =  Jtestsum/randomnum1

        #     if Jvalavg < Jvalavgmin:
        #         Jvalavgmin = Jvalavg
        #         kparamin= kpara


        #     fdata = open(outfile, "a", 0)
        #     fdata.write(str(Jtrainavg))
        #     fdata.write(' ')
        #     fdata.write(str(Jvalavg))
        #     fdata.write(' ')
        #     fdata.write(str(Jtestavg))
        #     fdata.write(' ')
        #     fdata.write(str(kpara)+'\n')
        #     fdata.close()
        kparamin = 20
        fdata = open(outfile, "a", 0)
        fdata.write(str(kparamin))
        fdata.write('\n')
        fdata.close()

                
        for bita in my_range(0, 0.9, 0.1):
            Jtestsum=0
            Jtrainsum =0 
            Jvalsum = 0
            sr1sum =0
            sr5sum =0
            sr10sum = 0
            for random_iter in my_range(1, randomnum, 1):

                trdata, valdata, testdata = getShuffledData(nodename) 
                totalen = trdata.shape[0]
                mcut = int(totalen*bita)
                tsdata = trdata[mcut:,:]
                X = tsdata[:,1:-1]
                y = tsdata[:,-1]
                m = len(y)

                Xval = valdata[:,1:-1]
                yval = valdata[:,-1]
                mval = len(yval)

                Xtest = testdata[:,1:-1]
                ytest = testdata[:,-1]
                mtest = len(ytest)

                normsX =  np.amax(X, axis=0)
                normsy = np.amax(y.reshape(-1,1), axis=0)
                X_norm = X/normsX.reshape(1,-1)
                Xtest_norm = Xtest/normsX.reshape(1,-1)
                Xval_norm = Xval/normsX.reshape(1,-1)
                
                yval_norm = yval/normsy
                y_norm = y/normsy
                ytest_norm = ytest/normsy

                
                featurenum = X.shape[1]
                print featurenum, m, mval, mtest

                
                A = np.hstack((X_norm, y_norm.reshape(-1,1)))
                Aval = np.hstack((Xval_norm, yval_norm.reshape(-1,1)))
                Atest = np.hstack((Xtest_norm, ytest_norm.reshape(-1,1)))
       

                cluster_init_kw = {'cluster_init':'kmeans', 'max_init_iter':5, \
                    'cov_init':'var', 'verbose':False}
                centroids, ccov, p_k, logL = gmm.em_gm(A, K = kparamin, max_iter = 800, verbose = False, \
                    init_kw= cluster_init_kw, max_tries=10)

                norm_ppower_test = prediction(mtest, Xtest_norm, centroids, ccov, p_k)
                ppower_test = np.array(norm_ppower_test*normsy)
                Jtest_nonorm = np.sqrt(np.mean((ppower_test - ytest.reshape(-1,1))**2))

                # norm_ppower_train = prediction(m, X_norm, centroids, ccov, p_k)
                # ppower_train = np.array(norm_ppower_train*normsy)
                # Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


                norm_ppower_val = prediction(mval, Xval_norm, centroids, ccov, p_k)
                ppower_val = np.array(norm_ppower_val*normsy)
                Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                
                Jtestsum = Jtestsum + Jtest_nonorm
                Jvalsum = Jvalsum + Jval_nonorm
                # Jtrainsum = Jtrainsum + Jtrain_nonorm



                success_num1 = countsuccess(ppower_test, ytest, 0.01)
                success_num5 = countsuccess(ppower_test, ytest, 0.05)
                success_num10 = countsuccess(ppower_test, ytest, 0.1)
                success_rate10 = success_num10/float(mtest)
                success_rate1 = success_num1/float(mtest)
                success_rate5 = success_num5/float(mtest)
                sr1sum = sr1sum + success_rate1
                sr5sum = sr5sum +success_rate5
                sr10sum = sr10sum + success_rate10

            Jtrainavg = Jtrainsum/randomnum
            Jvalavg = Jvalsum/randomnum
            Jtestavg  =  Jtestsum/randomnum
            success_rate1avg = sr1sum/randomnum
            success_rate5avg = sr5sum/randomnum
            success_rate10avg = sr10sum/randomnum

            fdata = open(outfile, "a", 0)
            fdata.write(str(Jtrainavg))
            fdata.write(' ')
            fdata.write(str(Jvalavg))
            fdata.write(' ')
            fdata.write(str(Jtestavg))
            fdata.write(' ')
            fdata.write(' '+ str(success_rate1avg))
            fdata.write(' '+ str(success_rate5avg))
            fdata.write(' '+ str(success_rate10avg))
            fdata.write(' '+ str(bita) + '\n')
            fdata.close()






    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
