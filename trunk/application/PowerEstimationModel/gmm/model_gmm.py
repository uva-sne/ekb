import time, os, datetime
import json
import csv
import requests
import sys
import matplotlib.pyplot as plt
import numpy as np
from pylab import plot, show
import pypr.clustering.gmm as gmm
from matplotlib import dates

#--------------------------------------------------


class PLOTException(Exception):
    pass

def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist    
    return normdatalist

def computecost(yhat, y):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    mid = yhat-y
    Jsq = mid**2
    J = 0.5/m*Jsq.sum()
    return J

def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count

def main(argv):
    
    t1 = (2013, 8, 1 , 0, 0, 0, 0, 0, 0)
    t2 = (2013, 10, 1 , 0, 0, 0, 0, 0, 0)
    start_epoch = time.mktime(t1) 
    end_epoch = time.mktime(t2)

    # nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
    # 'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']

    nodenamelist = ['node079', 'node080', 'node081', 'node082']

    ttest1 = (2013, 10, 1 , 0, 0, 0, 0, 0, 0)
    ttest2 = (2013, 10, 15 , 0, 0, 0, 0, 0, 0)

    fignum = 1
    for nodename in nodenamelist:
        


        data = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        totalen = data.shape[0]
        

        tsdata = data
        timestampe = tsdata[:,0]
        X = tsdata[:,1:-1]
        y = tsdata[:,-1]
        m = int(totalen)

        valdata = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        # testdata = np.genfromtxt('../datagen/%s/powerdata115-116-aggr180-nomd-linpack-%s.csv' \
        #     %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        testdata = np.genfromtxt('../datagen/%s/powerdata118-aggr180-nomd-linpack-gpu-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)


        mval = valdata.shape[0]
        mtest = testdata.shape[0]
        timestampeval = valdata[:,0]
        Xval = valdata[:,1:-1]
        yval = valdata[:,-1] 

        timestampetest = testdata[:,0]
        Xtest = testdata[:,1:-1]
        if nodename == 'node079':
            ytest = testdata[:,-1]+ 61
        else:
            ytest = testdata[:,-1]
        mtest = len(ytest)


        normsX =  np.amax(X, axis=0)
        normsy = np.amax(y.reshape(-1,1), axis=0)
        print normsX, normsy
        X_norm = X/normsX.reshape(1,-1)
        Xtest_norm = Xtest/normsX.reshape(1,-1)
        y_norm = y/normsy
        ytest_norm = ytest/normsy


        featurenum = X.shape[1]
        colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        colorlines = ['b-.', 'g-.', 'r-.', 'c-.', 'm-.', 'y-.', 'k-.']
        print featurenum, m, mval


        A = np.hstack((X_norm, y_norm.reshape(-1,1)))
        Atest = np.hstack((Xtest_norm, ytest_norm.reshape(-1,1)))
       
        kpara = 15
        centroids, ccov, p_k, logL = gmm.em_gm(A, K = kpara, max_iter = 800, verbose = True)
        Atest_pred = np.hstack((Xtest_norm, np.nan*np.ones(mtest).reshape(-1,1)))
        Atrain_pred = np.hstack((X_norm, np.nan*np.ones(m).reshape(-1,1)))
        ytest_pred = np.zeros(ytest.shape)
        y_pred = np.zeros(y.shape)
        for i in range(mtest):
            cen_cond, cov_cond, mc_cond = gmm.cond_dist(np.array(Atest_pred[i,:]),\
                centroids, ccov, p_k)
            for j in range(len(cen_cond)):
                ytest_pred[i] = ytest_pred[i] + (cen_cond[j]*mc_cond[j])
        # for k in range(m):
        #     cen_cond_tr, cov_cond_tr, mc_cond_tr = gmm.cond_dist(np.array(Atrain_pred[k,:]),\
        #         centroids, ccov, p_k)
        #     for kj in range(len(cen_cond_tr)):
        #         y_pred[k] = y_pred[k] + (cen_cond_tr[kj]*mc_cond_tr[kj])
        # with file('./centroid/centroid-gmm-%s-%s.txt' %(kpara,nodename), "w") as fcen:
        #     np.savetxt(fcen, centroids)
        # with file('./ccov/ccov-gmm-%s-%s.txt' %(kpara,nodename), "w") as fccov:
        #     print ccov
        #     [np.savetxt(fccov, ccov_ele) for ccov_ele in ccov]
        # with file('./p_k/pk-gmm-%s-%s.txt' %(kpara,nodename), "w") as fpk:
        #     np.savetxt(fpk, p_k)

        nanbool =  np.isnan(ytest_pred) == False
        ytest_pred = ytest_pred[nanbool]
        ytest_norm = ytest_norm[nanbool]
        ytest = ytest[nanbool]
        timestampetest = timestampetest[nanbool]
        mtest_bool = len(ytest)

        # nanbooltr =  np.isnan(y_pred) == False
        # y_pred = y_pred[nanbooltr]
        # y_norm = y_norm[nanbooltr]
        # y = y[nanbooltr]
        # m_bool = len(y)
        # ppower_train = y_pred*normsy
        # Jtrain_nonorm = 0.5/m_bool*((ppower_train - y)**2).sum()


        fig=plt.figure(fignum,(8,10))
        fignum +=1

        powerts = timestampetest/1000
        powerdts= map(datetime.datetime.fromtimestamp, powerts)
        powerfds = dates.date2num(powerdts)
        hfmt = dates.DateFormatter('%m/%d %H:%M')



        # with file('./GMMbool.txt', 'w') as outfile:
        #     np.savetxt(outfile, nanbool)
        #     np.savetxt(outfile, ytest_pred)

        Jtest = computecost(ytest_pred, ytest_norm)
        print 'Mean square error of validation set: %s' %Jtest
        ppower_error = ytest_pred - ytest_norm
        ppower_test = ytest_pred*normsy
        Jtest_nonorm = 0.5/mtest_bool*((ppower_test - ytest)**2).sum()
        
        estfig=plt.subplot(211)
        cpufig_total, = plt.plot(powerfds, ytest, 'b', label='Real Power')
        plt.plot(powerfds, ppower_test, 'r', label='Estimated Power')
        estfig.xaxis.set_major_locator(dates.DayLocator())
        estfig.xaxis.set_minor_locator(dates.DayLocator())
        estfig.xaxis.set_major_formatter(hfmt)
        plt.setp(estfig.get_xticklabels(), visible=False)
        #plt.xticks(rotation='vertical')
        plt.legend(prop={'size':6},loc='upper right')
        plt.ylabel('Power consumption(watt)')
        plt.title('Mean square error of test set = %s (%s)' %(Jtest, Jtest_nonorm), fontsize=10)

        #plt.xlabel('Date time')


        ax1=plt.subplot(212)
        plt.xticks(rotation='vertical')
        #avg_error=np.average(ppower_error)
        #std_error=np.std(ppower_error)
        cpuinterp_totalval = Xtest[:,0] + Xtest[:,1]
        print  Jtest

        success_num1 = countsuccess(ppower_test, ytest, 0.01)
        success_num5 = countsuccess(ppower_test, ytest, 0.05)
        success_num10 = countsuccess(ppower_test, ytest, 0.1)
        success_rate1 = success_num1/float(mtest_bool)
        success_rate5 = success_num5/float(mtest_bool)
        success_rate10 = success_num10/float(mtest_bool)

        fdata = open('./mse-gmm-linpack-%s.txt' %kpara, "a", 0)
        # fdata.write(str(Jtrain_nonorm))
        fdata.write(' '+str(Jtest_nonorm))
        fdata.write(' '+ str(success_rate1))
        fdata.write(' '+ str(success_rate5))
        fdata.write(' '+ str(success_rate10))
        fdata.write(' '+ nodename + '\n')
        fdata.close()



        ax1.plot(powerfds, ppower_error, 'r-')
        ax1.set_xlabel('Date time')
        ax1.set_ylabel('Prediction Error', color='r')
        ax2=ax1.twinx()
        ax2.plot(powerfds, cpuinterp_totalval, 'b.')
        ax2.set_ylabel('CPU load', color='b')
        ax1.xaxis.set_major_locator(dates.DayLocator())
        ax1.xaxis.set_minor_locator(dates.DayLocator())
        ax1.xaxis.set_major_formatter(hfmt)
        plt.xticks(rotation='vertical')

        plt.subplots_adjust(bottom=.3)

        #plt.savefig('./error-0801-0901-0922-1002-k=15-%s-pred-%s.png' %(nodename,nodenametest), bbox_inches='tight')
        plt.savefig('./error-115-116-linpack--k=%s-%s.png' %(kpara, nodename), bbox_inches='tight')



#0.00452849159419


    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
