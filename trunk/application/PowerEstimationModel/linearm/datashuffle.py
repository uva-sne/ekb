import numpy as np        


class ShuffleException(Exception):
    """Shuffle Exception"""
    pass



class datashuffle():

    def getShuffledData():
        data0 = np.genfromtxt('./%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)
        totalen = data.shape[0]

        data1 = np.genfromtxt('./%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('./%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('./%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)


        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))
        totalnum = data0123.shape[0]
        arr = np.arange(totalnum)
        arr_shuffle = np.random.shuffle(arr)
        trainnum = int(totalnum*0.6)
        valnum = int(totalnum*0.2)
        testnum = totalnum - (trainnum+valnum)
        datatrain = data0123[arr_shuffle[0:trainnum]]
        dataval = data0123[arr_shuffle[trainnum:valnum]]
        datatest = data0123[valnum:]
        return datatrain, dataval, datatest
