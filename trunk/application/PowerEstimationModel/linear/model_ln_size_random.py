import time, os, datetime
import sys
import threading
import numpy as np
import random





#--------------------------------------------------


class PLOTException(Exception):
    pass

def norm(datalist):
    maxdata = np.amax(datalist, axis=0)
    mindata = np.amin(datalist,axis=0)
    #print maxdata, mindata
    avgdata = np.average(datalist)
    if maxdata != mindata:
        normdatalist = (datalist - avgdata)/(maxdata-mindata)
    else: 
        normdatalist = datalist/1    
    return normdatalist


def prediction(X, y, theta):
    m = len(y)
    y = y.reshape(m,1)
    yval = np.dot(X, theta) 
    error = yval - y
    return yval, error

def computecost(X, y, theta):
    m = len(y)
    y = y.reshape(m,1)
    mid = np.dot(X, theta)-y
    Jsq = mid**2
    J = 0.5/m*Jsq.sum() 
    return J

def computecost_gen(X, y, theta, featurenum, lamd):
    m = len(y)
    y = y.reshape(m,1)
    mid = np.dot(X, theta)-y
    Jsq = mid**2
    Gsq = theta[1:featurenum, :]**2
    J = 0.5/m*Jsq.sum() + 0.5/m*lamd*Gsq.sum()
    return J

def gradientDescent(X, y, theta, alpha, num_iters, featurenum, lamd):
    m = len(y)
    print 'm=%f' %m
    J_history = []
    thetavector = theta
    y = y.reshape(m,1)
    for iter in range(num_iters):
        theta_tmp = theta 
        mid = np.dot(X,theta_tmp)-y
        Xtrans = X.T
        theta[0,:] = theta_tmp[0,:] - alpha/m*np.dot(Xtrans[0,:], mid)
        theta[1:featurenum+1,:] = theta_tmp[1:featurenum+1,:]*(1-alpha/m*lamd) - \
        alpha/m*np.dot(Xtrans[1:featurenum+1,:], mid) 
        if iter >=1:
            thetavector = np.hstack((thetavector, theta))
        J = computecost(X, y, theta)
        J_history.append(J) 
    return theta, J_history, thetavector

def validation(Xval, yval, thetavector):
    Jval_history = [] 
    xit, yit = thetavector.shape
    for it in range(yit):
        thetaval = thetavector[:,it]
        m = len(thetaval)
        thetaval = thetaval.reshape(m,1)
        Jval = computecost(Xval, yval, thetaval)
        Jval_history.append(Jval)
    return Jval_history  
        


def compower(datalist, power):
     powlist = datalist**power
     return powlist
     
def countsuccess(yhat, y, delta):
    m = len(y)
    y = y.reshape(m,1)
    yhat = yhat.reshape(m,1)
    boolarray = np.absolute((yhat - y)/y) <= delta
    count = np.sum(boolarray)
    return count     

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

def getShuffledData(nodename):
        data0 = np.genfromtxt('../datagen/%s/powerdata801-1001-shift180-aggr180-nomd-%s.csv' \
            %(nodename, nodename), delimiter=','  ,dtype=None, skip_header=0)

        data1 = np.genfromtxt('../datagen/%s/powerdata1001-1015-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)
        
        data2 = np.genfromtxt('../datagen/%s/powerdata1015-1030-shift180-aggr180-nomd-%s.csv' \
                %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data3 = np.genfromtxt('../datagen/%s/test-%s.csv' \
            %(nodename,nodename), delimiter=',' ,dtype=None, skip_header=0)

        data01 = np.vstack((data0, data1))
        data23 = np.vstack((data2,data3))
        data0123 = np.vstack((data01,data23))
        totalnum = data0123.shape[0]
        arr = np.arange(totalnum)
        random.shuffle(arr)
        trainnum = int(totalnum*0.6)
        valnum = int(totalnum*0.8)
        testnum = totalnum - (trainnum+valnum)
        datatrain = data0123[arr[0:trainnum]]
        dataval = data0123[arr[trainnum:valnum]]
        datatest = data0123[arr[valnum:]]
        return datatrain, dataval, datatest


def main(argv):
    


    lamdlist = [0.01, 0.1, 1, 10, 100]

    nodenamelist = ['node082', 'node083', 'node084', 'node085', 'node090', 'node091', 'node092',\
     'node093', 'node079', 'node078', 'node080', 'node081', 'node086', 'node087', 'node088', 'node089']



    for nodename in nodenamelist:

        outfile = './randomsize/mse-linearcpu-%s.txt' %nodename
        
        iterations = 1500
        Jvalavgmin = 10**100
        randomnum = 100.0
        for alpha in my_range(0.1, 0.9, 0.2):

            for lamd in lamdlist:
                Jtestsum=0
                Jtrainsum =0 
                Jvalsum = 0

                for random_iter in my_range(1, randomnum, 1):
                    trdata, valdata, testdata  = getShuffledData(nodename)

                    X = trdata[:,1:3]
                    y = trdata[:,-1]
                    m = len(y)

                    Xval = valdata[:,1:3]
                    yval = valdata[:,-1]
                    mval = len(yval)

                    Xtest = testdata[:,1:3]
                    ytest = testdata[:,-1]
                    mtest = len(ytest)

                    normsX =  np.amax(X, axis=0)
                    normsy = np.amax(y.reshape(-1,1), axis=0)
                    X_norm = X/normsX.reshape(1,-1)
                    Xtest_norm = Xtest/normsX.reshape(1,-1)
                    Xval_norm = Xval/normsX.reshape(1,-1)
                    yval_norm = yval/normsy
                    y_norm = y/normsy
                    ytest_norm = ytest/normsy

                    onem = np.ones(m).reshape(-1,1)
                    A = np.hstack((onem, X_norm))
                    onemval = np.ones(mval).reshape(-1,1)
                    Aval = np.hstack((onemval, Xval_norm))
                    onemtest = np.ones(mtest).reshape(-1,1)
                    Atest = np.hstack((onemtest, Xtest_norm))
                    
                    featurenum = X.shape[1]
                    theta = np.zeros((featurenum+1,1))
                    print featurenum, m, mval, mtest

                    
                    theta, history, thetavector = gradientDescent(A, y_norm, theta, alpha, iterations, featurenum, lamd)


                    norm_ppower_test, ppower_error = prediction(Atest, ytest_norm, theta)
                    ppower_test = np.array(norm_ppower_test*normsy)
                    Jtest_nonorm = np.sqrt(np.mean((ppower_test - ytest.reshape(-1,1))**2))

                    norm_ppower_train, ppower_error_train = prediction(A, y_norm, theta)
                    ppower_train = np.array(norm_ppower_train*normsy)
                    Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


                    norm_ppower_val, ppower_error_val = prediction(Aval, yval_norm, theta)
                    ppower_val = np.array(norm_ppower_val*normsy)
                    Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                    
                    Jtestsum = Jtestsum + Jtest_nonorm
                    Jvalsum = Jvalsum + Jval_nonorm
                    Jtrainsum = Jtrainsum + Jtrain_nonorm



                Jtrainavg = Jtrainsum/randomnum
                Jvalavg = Jvalsum/randomnum
                Jtestavg  =  Jtestsum/randomnum

                if Jvalavg < Jvalavgmin:
                    Jvalavgmin = Jvalavg
                    alphamin = alpha
                    lamdmin = lamd 

                fdata = open(outfile, "a", 0)
                fdata.write(str(Jtrainavg))
                fdata.write(' ')
                fdata.write(str(Jvalavg))
                fdata.write(' ')
                fdata.write(str(Jtestavg))
                fdata.write(' ')
                fdata.write(str(alpha))
                fdata.write(' ')
                fdata.write(str(lamd)+'\n')
                fdata.close()

        fdata = open(outfile, "a", 0)
        fdata.write(str(alphamin))
        fdata.write(' ')
        fdata.write(str(lamdmin))
        fdata.write('\n')
        fdata.close()

                
        for bita in my_range(0, 0.9, 0.1):
            Jtestsum=0
            Jtrainsum =0 
            Jvalsum = 0
            sr1sum =0
            sr5sum =0
            sr10sum = 0
            for random_iter in my_range(1, randomnum, 1):

                trdata, valdata, testdata = getShuffledData(nodename) 
                totalen = trdata.shape[0]
                mcut = int(totalen*bita)
                tsdata = trdata[mcut:,:]
                X = tsdata[:,1:3]
                y = tsdata[:,-1]
                m = len(y)

                Xval = valdata[:,1:3]
                yval = valdata[:,-1]
                mval = len(yval)

                Xtest = testdata[:,1:3]
                ytest = testdata[:,-1]
                mtest = len(ytest)

                normsX =  np.amax(X, axis=0)
                normsy = np.amax(y.reshape(-1,1), axis=0)
                X_norm = X/normsX.reshape(1,-1)
                Xtest_norm = Xtest/normsX.reshape(1,-1)
                Xval_norm = Xval/normsX.reshape(1,-1)
                yval_norm = yval/normsy
                y_norm = y/normsy
                ytest_norm = ytest/normsy

                onem = np.ones(m).reshape(-1,1)
                A = np.hstack((onem, X_norm))
                onemval = np.ones(mval).reshape(-1,1)
                Aval = np.hstack((onemval, Xval_norm))
                onemtest = np.ones(mtest).reshape(-1,1)
                Atest = np.hstack((onemtest, Xtest_norm))
                
                featurenum = X.shape[1]
                theta = np.zeros((featurenum+1,1))
                print featurenum, m, mval, mtest

                
                theta, history, thetavector = gradientDescent(A, y_norm, theta, alphamin, iterations, featurenum, lamdmin)


                norm_ppower_test, ppower_error = prediction(Atest, ytest_norm, theta)
                ppower_test = np.array(norm_ppower_test*normsy)
                Jtest_nonorm = np.sqrt(np.mean((ppower_test - ytest.reshape(-1,1))**2))

                norm_ppower_train, ppower_error_train = prediction(A, y_norm, theta)
                ppower_train = np.array(norm_ppower_train*normsy)
                Jtrain_nonorm = np.sqrt(np.mean((ppower_train - y.reshape(-1,1))**2))


                norm_ppower_val, ppower_error_val = prediction(Aval, yval_norm, theta)
                ppower_val = np.array(norm_ppower_val*normsy)
                Jval_nonorm =np.sqrt(np.mean((ppower_val - yval.reshape(-1,1))**2))
                
                Jtestsum = Jtestsum + Jtest_nonorm
                Jvalsum = Jvalsum + Jval_nonorm
                Jtrainsum = Jtrainsum + Jtrain_nonorm



                success_num1 = countsuccess(ppower_test, ytest, 0.01)
                success_num5 = countsuccess(ppower_test, ytest, 0.05)
                success_num10 = countsuccess(ppower_test, ytest, 0.1)
                success_rate10 = success_num10/float(mtest)
                success_rate1 = success_num1/float(mtest)
                success_rate5 = success_num5/float(mtest)
                sr1sum = sr1sum + success_rate1
                sr5sum = sr5sum +success_rate5
                sr10sum = sr10sum + success_rate10

            Jtrainavg = Jtrainsum/randomnum
            Jvalavg = Jvalsum/randomnum
            Jtestavg  =  Jtestsum/randomnum
            success_rate1avg = sr1sum/randomnum
            success_rate5avg = sr5sum/randomnum
            success_rate10avg = sr10sum/randomnum

            fdata = open(outfile, "a", 0)
            fdata.write(str(Jtrainavg))
            fdata.write(' ')
            fdata.write(str(Jvalavg))
            fdata.write(' ')
            fdata.write(str(Jtestavg))
            fdata.write(' ')
            fdata.write(' '+ str(success_rate1avg))
            fdata.write(' '+ str(success_rate5avg))
            fdata.write(' '+ str(success_rate10avg))
            fdata.write(' '+ str(bita) + '\n')
            fdata.close()
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))   
        
